#lang scribble/manual
@(require scriblib/autobib)
@(provide (all-defined-out))

@(define RoShi-AsahiBunko-1
(make-bib
#:title "老子 (上) 中国古典選10"
#:author "福永光司"
#:date "1978"
#:is-book? #t
#:location (book-location #:publisher "東京: 朝日新聞社")
))
@(define RoShi-AsahiBunko-2
(make-bib
#:title "老子 (下) 中国古典選10"
#:author "福永光司"
#:date "1978"
#:is-book? #t
#:location (book-location #:publisher "東京: 朝日新聞社")
))
@(define LT-NA
(make-bib
#:title "Non-Activity"
#:author "Leo Tolstoy"
#:date "1893"
#:is-book? #f
#:location (techrpt-location #:institution "The Anarchist Library on-line Site"
#:number "2021")		     
#:url "https://theanarchistlibrary.org/library/leo-tolstoy-non-activity"
#:note "Accessed 2021/10/24, Also available though revoltlib on-line site: http://www.revoltlib.com/?id=10493"
))
@(define LTolstoy-DavisFumiko-1
(make-bib
#:title "What today's youth can learn from the Great Russian Writer Leo Tolstoy"
#:author "Fumiko Davis"
#:date "2018"
#:is-book? #f
#:location (techrpt-location #:institution "japan-forward on-line site"
#:number "March 14, 2018")
 #:url "https://japan-forward.com/what-todays-youth-can-learn-from-the-great-russian-writer-leo-tolstoy/"
#:note "Accessed 2021/10/24"
))
@(define TNR-LT-old
(make-bib
#:title "The Old Magician: A defense of the late, scolding Tolstoy"
#:author "Irving Howe"
#:date "2014"
#:is-book? #f
#:location (techrpt-location #:institution "The New Republic"
				#:number "April 27, 1992")
 #:url "https://newrepublic.com/article/116158/defense-leo-tolstoy"
 #:note "Accessed 2021/10/24. On-line page dated January 10, 2014. Date under text title April 27, 1992"
))
