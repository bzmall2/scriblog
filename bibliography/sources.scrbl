#lang scribble/manual
@(require scriblib/autobib)
@(provide (all-defined-out))

@; So Much Nearer Future of Poetry
@(define AR-MoUH
(make-bib
#:title "Ministry of Utmost Happiness"
#:author "Arundhati Roy"
#:date "2017"
#:is-book? #t
#:location (book-location #:publisher "New York: Alfred A. Knopf")
))
@(define JL-LMTTM
(make-bib
#:title "Lies My Teacher Told Me: Everything Your American History Textbook Got Wrong"
#:author "James W. Loewen"
#:date "1995"
#:is-book? #t
#:location (book-location #:publisher "New York: Touchstone")
))
@(define TR-CoI
(make-bib
#:title "The Cult of Information: A Neo-Luddite Treatise on High-Tech, Artificial Intelligence, and the True Art of Thinking"
#:author "Theodore Roszak"
#:date "1994"
#:is-book? #t
#:location (book-location #:publisher "Berkley: University of California Press"  #:edition "Second")
#:note "First edition copyrighted in 1986"
))
@(define OA-LaBi5th
(make-bib
#:title "LaTeX2e 美文書作成入門改正第５版"
#:author "奥村晴彦"
#:date "2012"
#:is-book? #t
#:location (book-location #:publisher "技術評論社")
#:note "最初の版は1991年"
))
@(define HA-THC
(make-bib
#:title "The Human Condition"
#:author "Hannah Arendt"
#:date "1958"
#:is-book? #t
#:location (book-location #:publisher "Chicago: The University of Chicago Press")
@;#:url 
@;#:note 
))
@(define SW-TNfR
(make-bib
#:title "The Need for Roots"
#:author "Simone Weil"
#:date "1952"
#:is-book? #t
#:location (book-location #:publisher "New York: G.P. Putnam's Sons")
@;#:url 
@;#:note 
))
@(define MB-TNR
(make-bib
#:title "The Next Revolution: Popular Assemblies & The Promise of Direct Democracy"
#:author "Murray Bookchin"
#:date "2015"
#:is-book? #t
#:location (book-location #:publisher "London: Verso")
@;#:url 
@;#:note 
))
@(define MB-TNR-ULK-FW
(make-bib
#:title "Foreword by Ursula K. Le Guin"
#:author "Ursula K. Le Guin"
#:date "2015"	 
#:is-book? #f	 
#:location (book-chapter-location ""
                         #:volume "The Next Revolution"
			 #:pages (list "ix" "xi"))
))
@(define ULK-NG-rvw
(make-bib
#:title "Norse Mythology by Neil Gaiman review---nice dramatic narratives, but where's the nihilism"
#:author "Ursula K. Le Guin"
#:date "2017"
#:is-book? #f
#:location "The Guardian's on-line site"	
#:url "https://www.theguardian.com/books/2017/mar/29/norse-myths-by-neil-gaiman-review"
#:note "Accessed 2021/10/31"
))
@(define BR-FMW-rjcom
(make-bib
#:title "A Free Man's Worship"
#:author "Bertrand Russel"
#:date "1903"
#:is-book? #f	
#:location "Japanese Russelian site russel-j"
#:url "https://www.russell-j.com/0053-FMW.HTM"
#:note "Accessed 2021/10/31"
))

(define TM-RoU
  (make-bib
   #:title "Raids on the Unspeakable"
   #:author "Thomas Merton"
   #:date "1964"
   #:is-book? #t
   #:location (book-location #:publisher "New York: New Directions")
   #:note "Earlier copyright 1960 by The Carleton Miscellany. Also in 1961, 1964, 1965, 1966 by The Abbey of Gethsemani."
   ))
@(define Mdavis-Aflu
  (make-bib
   #:title "The Monster at Our Door: The Global Threat of Avian Flu"
   #:author "Mike Davis"
   #:date "2005"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Henry Holt and Company")
   ))
@(define BR-GO-1984
  (make-bib
   #:title "Symptoms of George Orwell's 1984"
   #:author "Bertrand Russel"
   #:date "1953" ; four years after Orwell's death
   #:is-book? #f
   #:location "Bertrand Russell : Portal site for Russellian in Japan"
   #:url "https://www.russell-j.com/1070_SoO.HTM"
   #:note "Accessed 2021/10/20"
   ))
@(define BR-OE-en
  (make-bib
   #:title "On Education, especially in early childhood, 1926"
   #:author "Bertrand Russel"
   #:date "1926"
   #:is-book? #f
   #:location "Bertrand Russell : Portal site for Russellian in Japan"
   #:url "https://www.russell-j.com/beginner/ON_EDU-TEXT.HTM"
   #:note "Accessed 2021/10/20"
   ))
@(define BR-GO-1984-ja
  (make-bib
   #:title "バートランド・ラッセル オーウェルの『1984年』の徴候"
   #:author "バートランド、 ラッセル"
   #:date "2016"
   #:is-book? #f
   #:location "バートランド・ラッセル の ポータルサイト"
   #:url "https://www.russell-j.com/beginner/1070_SoO-010.HTM"
   #:note "Accessed 2021/10/20"
   ))
@(define BR-OE-TF-ja
  (make-bib
   #:title "「ラッセル教育論」(松下彰良・訳)"
   #:author "バートランド、 ラッセル"
   #:date "2015"
   #:is-book? #f
   #:location "バートランド・ラッセル の ポータルサイト"
   #:url "https://russell-j.com/beginner/OE08-090.HTM"
   #:note "Accessed 2021/10/20"
   ))
@(define NC-WOE-intrvw
  (make-bib
   #:title "The World On Edge: Interview with Noam Chomsky"
   #:author "Shaun Randol"
   #:date "2014"
   #:is-book? #f
   #:location "chomksy.info on-line site"
   #:url "https://chomsky.info/20140428/"
   #:note "Accessed 2021/10/19"
   ))
@(define NC-GOz-AF-intro-reddit
  (make-bib
   #:title "George Orwell's unpublished introduction to Animal Farm"
   #:author "Datardlyrebel"
   #:date "2015"
   #:is-book? #f
   #:location "reddit.com on-line site"
   #:url "https://www.reddit.com/r/chomsky/comments/4ewkur/george_orwells_unpublished_introduction_to_animal/"
   #:note "Accessed 2021/10/19"
   ))
@(define MDavis-PSlums
  (make-bib
   #:title "Planet of Slums"
   #:author "Mike Davis"
   #:date "2006"
   #:is-book? #t
   #:location (book-location #:publisher "London: Verso")
   ))
@(define TheRP
  (make-bib
   #:title "The Rhetorical Précis"
   #:author "unknown"
   #:date "2021"
   #:is-book? #f
   #:location "fcusd online site"
   #:url "https://www.fcusd.org/cms/lib/CA01001934/Centricity/Domain/1250/Rhetorical%20Precis%20Master.pdf"
   #:note "Accessed 2021/10/19"
   ))
@(define WTheRP
  (make-bib
   #:title "Writing the Rhetorical Précis"
   #:author "Mrs. Spear"
   #:date "2021"
   #:is-book? #f
   #:location "collegeessay online site"
   #:url "https://www.collegeessay.org/blog/how-to-write-a-precis/rhetorical-precis-ap-lang.pdf"
   #:note "Accessed 2021/10/19"
   ))
@(define GMcC-CStat-Ja
  (make-bib
   #:title "Client State: Japan in the American Embrace"
   #:author "Gavan McCormack"
   #:date "2007"
   #:is-book? #t
   #:location (book-location #:publisher "London: Verso")
   ))
@(define VP-SSeekers
  (make-bib
   #:title "The Status Seekers"
   #:author "Vance Packard"
   #:date "1959"
   #:is-book? #t
   #:location (book-location #:publisher "Harmondsworth: Penguin Books")
   ))
@(define VP-WMakers
  (make-bib
   #:title "The Waste Makers"
   #:author "Vance Packard"
   #:date "1960"
   #:is-book? #t
   #:location (book-location #:publisher "Brooklyn: Ig Publishing")
   #:note "Witn an introduction by Bill McKibben, copyright renewed 1988 by Vance Packard"
   ))
@(define CKO-MPsychology
  (make-bib
   #:title "The Meaning of Psychology"
   #:author "C.K. Ogden"
   #:date "1923"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Harper & Brothers")
   #:note "Produced by Amazon, Printed in Japan, a reproduction of an important historical work."
   ))
@(define MM-kara
  (make-bib
   #:title "水俣から寄り添って語る"
   #:author "水俣フォーラム編"
   #:date "2018"
   #:is-book? #t
   #:location (book-location #:publisher "東京: 岩波書店")
   ))
@(define MM-kara-ID
  (make-bib
   #:title "水俣の分断と重層する共同体"
   #:author "色川大吉"
   #:date "2003"
   #:is-book? #f
   #:location (book-chapter-location  ""
			   #:volume "水俣から寄り添って語る"
			   #:pages (list 143 167))
   #:note "第五回水俣病記念講演会、2003年4月10日"
   ))
@(define MR-KuraTae
  (make-bib
   #:title "暗闇に耐える思想： 松下竜一講演録"
   #:author "松下竜一"
   #:date "2012"
   #:is-book? #t
   #:location (book-location #:publisher "合同会花乱社")
   #:note "藤永伸による講演の文字化"
   ))
@(define NC-PnI
  (make-bib
   #:title "On Power and Ideology"
   #:author "Noam Chomsky"
   #:date "2015"
   #:is-book? #f
   #:location "chomsky.info"
   #:url "https://chomsky.info/on-power-and-ideology/"
   #:note "The New School for Social Research, September 19, 2015
Published at Democracy Now: http://www.democracynow.org/2015/9/22/noam_chomsky_on_the_myth_of"
   ))
@(define Rddt-GO-Af-intro
  (make-bib
   #:title "George Orwell's unpublished introduction to Animal Farm"
   #:author "Dastardlyrebel"
   #:date "2016"
   #:is-book? #f
   #:location "reddit.com"
   #:url "https://www.reddit.com/r/chomsky/comments/4ewkur/george_orwells_unpublished_introduction_to_animal/"
   #:note "post links to http://orwell.ru/library/novels/Animal_Farm/english/efp_go"
   ))
@(define AC-NVNE 
  (make-bib
   #:title "Neither Victims nor Executioners: An Ethic Superior to Murder"
   #:author "Albert Camus"
   #:date "2007"
   #:location (book-location #:publisher "Eugene: Wipf and Stock Publishers")
   #:note "With a new introdcution by Peter Klotz-Chamberlin and Scott Kennedy. Translated by Dwight Macdonald. Original (in French) Copyright  1946 Gallimard, previously published (in English) in 1986 by New Society Publishers. 'This book is a reprint of \"Ni victimes ni bourreaux\" in Actuelles: Écrtis politiques: Tome I: Chorniques 1944-1948, Gallimard, Paris 1950"))
@(define DR-GBiB
  (make-bib
   #:title "Get Back in the Box: How Being Great at What You Do is Great for Business"
   #:author "Douglas Rushkoff"
   #:date "2005"
   #:is-book? #t
   #:location (book-location #:publisher "New York: HarperCollins Publishers")
   )) 
@(define SMinako-BSDH
  (make-bib
   #:title "文章読書さん江"
   #:author "斎藤美奈子"
   #:is-book? #t
   #:date "2002"
   #:location (book-location #:publisher "東京: 筑摩書房")
   ))
@(define SMinako-BSDH-bunko
  (make-bib
   #:title "文章読書さん江"
   #:author "斎藤美奈子"
   #:is-book? #t
   #:date "2007"
   #:location (book-location #:publisher "東京: 筑摩書房")
   #:note "2002年の本の文庫版"))
@(define NC-MisEdu
  (make-bib
   #:title "Chomsky on MisEduction"
   #:author "Noam Chomsky"
   #:date "2000"
   #:is-book? #t
   #:location (book-location #:publisher "Lanham: Rowman & Littlefield")
   #:note "Edited and introduced by Donaclod Macedo. First paperback edition 2004"))
@(define NC-MisEdu-Ja
  (make-bib
   #:title "チョムスキーの教育論"
   #:author "ノーム・チョムスキー"
   #:date "2006"
   #:is-book? #t
   #:location (book-location #:publisher "東京: 明石書店")
   #:note "訳者： 寺島陸吉 寺島美紀子。 英語教育の研究会JAASET(Japan Association of Applied Semiotics for English Teaching)の翻訳プロジェクト"))
@(define NC-MinProg
  (make-bib
   #:title "The Minimalist Program"
   #:author "Noam Chomsky"
   #:date "1995"
   #:is-book? #t
   #:location (book-location #:publisher "Cambridge: The MIT Press")))
@(define KY-Imi-Iku
  (make-bib
   #:title "意味論と外国語教育"
   #:author "片桐ユズル"
   #:date "1973"
   #:is-book? #t
   #:location (book-location #:publisher "東京: くろしお出版")))
@(define HK-Roushi
  (make-bib
   #:title "老子"
   #:author "蜂屋邦夫"
   #:is-book? #t
   #:date "2008"
   #:location (book-location #:publisher "東京: 岩波書店")
   #:note "文庫本"))
@(define CKO-Oppstn
  (make-bib
   #:title "Opposition: A Linguistic and Psychological Analysis"
   #:author "C.K. Ogden"
   #:date "1967"
   #:is-book? #t
   #:location (book-location #:publisher "Bloomington: Indiana University Press")
   #:note "First book copyright 1932 by Orhological Institute, Introduction Copyright 1967 by I.A. Richards"))
@(define UKL-LaoTzu
  (make-bib
   #:title "lao tzu: tao te ching"
   #:author "Ursula K. LeGuin"
   #:date "1997"
   #:is-book? #t
   #:location (book-location #:publisher "Boston: Shambala Publications")))
@(define CKO-BW
  (make-bib
   #:title "The Basic Words"
   #:author "C.K. Ogden"
   #:date "1977"
   #:location (book-location #:publisher "Tokyo: Hokuseido Press"
			     #:edition "Twelth, revised")
   #:note "Original Copyright much older"))
@(define CKO-BD
  (make-bib
   #:title "The Basic Dictionary: ベーシック英英いい換え辞典"
   #:author "C.K. Ogden"
   #:date "1990"
   #:location (book-location #:publisher "Tokyo: Hokuseido Press")
   #:note "Original copyright much earlier. 解説: 片桐ユズル"))
@(define NP-AOtD
  (make-bib
   #:title "Amusing Ourselves to Death: Public Discourse in the Age of Show Business"
   #:author "Neil Postman"
   #:date "1985"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Viking Penguin Inc.")))
@(define SE-CoC
  (make-bib
   #:title "Captains of Consciousness: Advertising and the Social Roots of the Consumer Culture"
   #:author "Stuart Ewen"
   #:date "1976"
   #:is-book? #t
   #:location (book-location #:publisher "New York: McGraw-Hill Book Company")))
@(define IHisashi-BSDH
  (make-bib
   #:title "自家製文章読本"
   #:author "井上ひさし"
   #:is-book? #t
   #:date "1984" ; 昭和59年
   #:location (book-location #:publisher "東京: 新潮社")))
@(define NYukio-BSDH
  (make-bib
   #:title "書くことについて"
   #:author "野口悠起雄"
   #:date "2020"
   #:location (book-location #:publisher "東京: KADOKAWA")))
@(define KSR-MftF
  (make-bib
   #:title "The Ministry for the Future"
   #:author "Kim Stanley Robinson"
   #:date "2020"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Orbit")))
@(define MB-EoF
  (make-bib
   #:title "The Ecology of Freedom: The emergence and dissolution of hierarchy"
   #:author "Murray Bookchin"
   #:date "2005"
   #:is-book? #t
   #:location (book-location #:publisher "Oakland: Ak Press")))
@(define NC-APnNM
  (make-bib
   #:title "American Power and the New Mandarins"
   #:author "Noam Chomsky"
   #:date "2002"
   #:is-book? #t
   #:location (book-location #:publisher "New York: The New Press")
   #:note "First published in hardcover by Pantheon Books in 1969"))
@(define NC-APnNM-Ja
  (make-bib
   #:title "アメリカン・パワーと新官僚：知識人と責任"
   #:author "N. ョムスキー"
   #:date "1975" ;; 昭和50年
   #:is-book? #t
   #:location (book-location #:publisher "太陽選書")
   #:note "Translation of American Power and the New Mandarins by Noam Chomsky, 1969. 訳 木村雅次, 水落一朗, 吉田武士訳"))
@(define CLasch-Plain
  (make-bib
   #:title "Plain Style: A Guide to Written English"
   #:author "Christopher Lasch"
   #:date "2002"
   #:location (book-location #:publisher "Philadelphia: University of Pennsylvania Press")
   #:note "Edited and with an introduction by Stewart Weaver"))
@(define SMW-Minamata
  (make-bib
   #:title "The Minamata Story: An EcoTragedy"
   #:author (authors "Sean Michael Wilson" "Akiko Shimojima")
   #:date "2021"
   #:location (book-location #:publisher "Berkely: Stone Bridge Press")))
@(define BS-MStory-foreword
  (make-bib
   #:title "Foreword"
   #:author "Brian Small"
   #:date "2021"
   #:location (book-chapter-location ""
                                     #:volume "The Minamata Story: An EcoTragedy"
                                     #:pages (list "i" "ii"))))

@(define RoRhetoric
  (make-bib
   #:title "Reading for Rhetoric: Applications to Writing"
   #:author (authors "Caroline Shrodes" "Clifford Josephson" "James K. Wilson")
   #:date "1962"
   #:location (book-location #:publisher "New York: The Macmillan Company")))

@(define JC-RF-WoP
  (make-bib
   #:title "Robert Frost: The Way of the Poem"
   #:author "John Ciardi"
   #:date "1962"
   #:location (book-chapter-location "Analysis"
                                     #:volume "Reading for Rhetoric: Applications to Writing"
                                     #:publisher "New York: The Macmillan Company"
                                     #:pages (list 231 240))))
@(define LF-NoIT
  (make-bib
   #:title "No, In Thunder!"
   #:author "Leslie Fiedler"
   #:date "1962"
   #:location (book-chapter-location "Argument and Persuasion"
                                     #:pages (list 276 286)
                                     #:volume "Reading for Rhetoric: Applications to Writing"
                                     #:publisher "New York: The Macmillan Company")))
    
@(define SB-TCS
  (make-bib
   #:title "The Complete Stylist"
   #:author "Sheridan Baker"
   #:date "1966"
   #:location (book-location #:publisher "New York: Thomas Y. Crowell Company")))

@(define SB-TE
  (make-bib
   #:title "The Essayist"
   #:author "Sheridan Baker"
   #:date "1972"
   #:location (book-location #:publisher "New York: Thomas Y. Crowell Company"
                             #:edition "Second")))
@(define JB-5thAv
  (make-bib
   #:title "Fifth Avenue Uptown: A Letter from Harlem"
   #:author "James Baldwin"
   #:date "1960"
   #:location (book-chapter-location "The Autobiographical Essay"
                                     #:volume "The Essayist"
                                     #:publisher "New York: Thomas Y. Crowell Company"
                                     #:pages (list 233 241))
   #:note "An essay from Nobody Knows My Name, reprinted by permission of the publisher The Dial Press. Originally published in Esquire."))

@(define AR-air
  (make-bib
   #:title "The Air We Breathe: A Conversation With Arundhati Roy"
   #:author "Ratik Asokan"
   #:date "2017"
   #:url "https://www.thenation.com/article/archive/the-air-we-breathe-a-conversation-with-arundhati-roy/"
   #:is-book? #f
   #:note "The writer discusses her new novel, love, justice, and Indian politics. Accessed 2021/10/07"))
  
@(define AR-unpopular-point
  (make-bib
   #:title "Arundhati Roy: 'The point of the writer is to be unpopular'"
   #:author "Tim Lewis"
   #:is-book? #f
   #:date "2018"
   #:url  "https://www.theguardian.com/books/2018/jun/17/arundhati-roy-interview-you-ask-the-questions-the-point-of-the-writer-is-to-be-unpopular"
   #:note "Accessed 2021/10/07"))

@(define AR-EasyTarget
  (make-bib
   #:title "What Makes Arundhati Roy An Easy Target Of Hate"
   #:author "Somak Ghoshal"
   #:date "2017"
   #:is-book? #f
   #:url "https://www.huffpost.com/archive/in/entry/what-makes-arundhati-roy-an-easy-target-of-hate_a_22106863"
   #:note "Accessed 2021/10/07"))
@(define AR-LnH
  (make-bib
   #:title "Why We Love To Hate Ms Roy"
   #:author "Saba Nagvi"
   #:date "2006"
   #:url "https://www.outlookindia.com/website/story/why-we-love-to-hate-ms-roy/233522"
   #:note "Deconstructing the complex Indian responses to Arundhati Roy reveals layers of prejudice. Apart from the macho male response, more intriguing is the Indian response to her at a personal level... "))
@(define AR-DN-LitPower
  (make-bib
   #:title "Arundhati Roy on the Power of Fiction: Literature Is \"The Simplest Way of Saying a Complicated Thing\""
   #:author (authors "Amy Goodman" "Nermeen Shaikh")
   #:date "2019"
   #:is-book? #f
   #:location "Democracy Now on-line site"
   #:url "https://www.democracynow.org/2019/5/13/arundhati_roy_on_the_power_of"
   #:note "Accessed 2021/10/14"
   ))
@(define AR-DN-extnct
  (make-bib
   #:title "Arundhati Roy on Kashmir, the Danger of U.S. Attacking Iran & Her New Book “My Seditious Heart”"
   #:author (authors "Amy Goodman" "Nermeen Shaikh")
   #:date "2019"
   #:is-book? #f
   #:location "Democracy Now on-line site"   
   #:url "https://www.democracynow.org/2019/5/13/arundhati_roy_on_kashmir_the_danger"
   #:note "Accessed 2021/10/14"
   ))
@(define FF-10Myths
  (make-bib
   #:title "World Hunger: Ten Myths"
   #:author (authors "Francis More Lappé" "Joseph Collins")
   #:date "2015"
   #:is-book? #f
   #:location "FoodFirst on-line site"
   #:url "https://foodfirst.org/publication/world-hunger-ten-myths/"
   #:note "This Backgrounder was adapted and edited by KellyAnne Tang and Tanya Kerssen from the book World Hunger: 10 Myths by Food First co-founders Frances Moore Lappé and Joseph Collins (New York: Grove Press and Oakland: Food First Books, 2015) See original for notes and references."))
@(define EG-OVLA
  (make-bib
   #:title "Open Veins of Latin America: five centuries of the pillage of a continent"
   #:author "Eduardo Galeano"
   #:date "1973"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Monthly Review Press"
			     #:edition "25th Anniversary")
   #:note "Foreword by Isabel Allende. Translated by Cedric Belfrage. Originally published as Las venas abiertas de América Latina"
   ))
@(define SG-AWP
  (make-bib
   #:title "Another world is possible if..."
   #:author "Susan George"
   #:date "2004"
   #:is-book? #t
   #:location (book-location #:publisher "London: Verso")
   ;; #:note
   ))
@(define SG-AWP-Ja
  (make-bib
   #:title "オルター・グローバリゼーション宣言"
   #:author "スーザン・ジョージ"
   #:date "2004"
   #:is-book? #t
   #:location (book-location #:publisher "作品社")
   #:note "訳者: 杉村昌昭、 真田 満"
   ))
@(define AR-PenLect
  (make-bib
   #:title "Literature provides shelter. That's why we need it"
   #:author "Arundhati Roy"
   #:date "2019"
   #:is-book? #f
   #:location "The Guardian Site"
   #:url "https://www.theguardian.com/commentisfree/2019/may/13/arundhati-roy-literature-shelter-pen-america"
   #:note "Accessed 2021/10/14. Online page explanation \"This is an abridged version of Arundhati Roy’s PEN America Arthur Miller Freedom to Write Lecture, delivered on 12 May 2019\""
   ))
@(define YV-DN-ANow
  (make-bib
   #:title "Another Now”: Socialist Alternatives to Capitalism Explored in New Novel by Yanis Varoufakis"
   #:author (authors "Amy Goodman" "Yanis Varoufakis")
   #:date "2021"
   #:is-book? #f
   #:location "Democracy Now! Site"
   #:url "https://www.democracynow.org/2021/9/29/another_now_socialist_alternatives_to_capitalism"
   #:note "Accessed 2021/10/15"
   ))
@(define DG-I-idiot
  (make-bib
   #:title "\"I found myself turning into an idiot!\": David Graeber explains the life-sapping reality of bureaucratic life"
   #:date "2015"
   #:author "Elias Isquith"
   #:url "https://www.salon.com/2015/03/05/i_found_myself_turning_into_an_idiot_david_graeber_explains_the_life_sapping_reality_of_bureaucratic_life/"
   #:note "Accessed 2021/10/07. The activist-academic and Occupy Wall Street champion tells Salon about his new book on the bureaucratic state."
   ))

@(define SnW-EoS
  (make-bib
   #:title "The Elements of Style"
   #:author (authors "William Strunk" "E.B.White")
   #:date "2000"
   #:location (book-location #:publisher "Needham Heights: Allyn & Bacon"
			     #:edition "Fourth")))
;; The Complete Stylist, and THe Essayist
@(define SB-PS
  (make-bib
   #:title "The Practical Stylist: with Readings"
   #:author (authors "Sheridan Baker" "Robert E. Yarber")
   #:date "1986"
   #:location (book-location #:publisher "New York: Harper & Row"
			     #:edition "Sixth")
   #:is-book? #t))
@(define SJG-WHNM
  (make-bib
   #:title "Wide Hats and Narrow Minds"
   #:author "Stephen Jay Gould"
   #:date "1980"
   #:location (book-chapter-location "Readings"
				     #:volume "The Practical Stylist: with Readings: Sixth edition"
				     #:pages (list 234 239))
   #:note "Reprinted from The Panda's Thumb, More Reflections in Natural History by permission of W.W. Norton & Company"))
@(define JB-ef
  (make-bib
   #:title "an excerpt from Fifth Avenue Uptown: A Letter from Harlem"
   #:author "James Baldwin"
   #:date "1960"
   #:location (book-chapter-location "Middle Paragraphs"
				     #:volume "The Practical Stylist: with Readings: Sixth edition"
				     #:pages (list 99 11))
   #:note "The excerpted essay is from Nobody Knows My Name, A Dial Press book, reprinted in The Practical Stylist by permission of Doubleday & Company"))

@(define SB-LPS
  (make-bib
   #:title "The Longman Practical Stylist"
   #:author "Sheridan Baker"
   #:date "2006"
   #:location (book-location #:publisher "New York: Pearson Longman")))

@(define AC-R-V
  (make-bib
   #:title "The Rebel: An Essay on Man in Revolt"
   #:author "Albert Camus"
   #:is-book? #t
   #:date "1991"
   #:location (book-location #:publisher "New York: Vintage Books"
			     #:edition "1st Vintage International")
   #:note "First copyright 1956 by Alfred A. Knopf, renewed 1984. This translation first published in the United States by Alfred A. Knopf, Inc., New York, in 1956. This citation comes from First Vintage International Edition November 1991. With a foreword by Herbert Read; a revised and complete translation of L'Homme révolté by Anthony Bower.---1st Vintage International ed."))

@(define AC-R-V-HRead-FW
  (make-bib
   #:title "Foreword"
   #:author "Herbert Read"
   #:date "1991"
   #:location (book-chapter-location ""
				     #:volume "The Rebel: An Essay on Man in Revolt"
				     #:pages (list "vii" "xii"))
   #:note "Citation from the Vintage Books First Vintange International Edition November 1991"))

@(define AC-R-P
  (make-bib
   #:title "The Rebel: An Essay on Man in Revolt"
   #:author "Albert Camus"
   #:is-book? #t
   #:location (book-location #:publisher "London: Penguin Classics"
			     #:edition "Penguin Classics 2013")
   #:date "2000"
   #:note "First published in France as L'Homme révolté in 1951. This translation first published in Great Britain by Hamish Hamilton 1953. Translation copyright 1953 by Anthony Bower. Afterword copyright Oliver Todd, 2000"))

@(define AC-R-P-OTodd-AW
  (make-bib
   #:title "Afterword"
   #:author "Oliver Todd"
   #:date "2000"
   #:location (book-chapter-location ""
				     #:volume "The Rebel: An Essay on Man in Revolt"
				     #:pages (list 249 260))))
@(define AC-R-Ja
  (make-bib
   #:title "カミュ全集6:反抗的人間"
   #:author "Albert Camus"
   #:is-book? #t
   #:date "1973"
   #:location (book-location #:publisher "東京: 新潮社")
   #:note "翻訳者: 佐開朔, 白井浩司"))

@(define AC-RK ; Riso Kyoushitu
  (make-bib
   #:title "カミュ「よそもの」きみの友だち"
   #:author "野崎歓"
   #:is-book? #t
   #:date "2006"
   #:location (book-location #:publisher "東京： みすず書房")))


@(define OM-CW
  (make-bib
   #:title "チッソは私であった"
   #:author "緒方正人"
   #:date "2010"
   #:location (book-location #:publisher "河出書房新社")
   #:note "本書は2001年10月に葦書房より刊行されました。文庫化にあたり「常世の舟」「文庫版解説」「略年?」を増補しました。"))

@(define GS-TI
  (make-bib
   #:title "Turtle Island 亀の島"
   #:author (authors "Gary Snyder" "ナナオ サカキ")
   #:date "1974"
   #:is-book? #t
   #:location (book-location #:publisher "山口書店")))

@(define OM-RES
  (make-bib
   #:title "Rowing the Eternal Sea: The Story of a Minamata Fisherman"
   #:author (authors "Masato Ogata" "Oiwa Keibo" "Karen Colligan-Taylor")
   #:date "2001"
   #:location (book-location #:publisher "Lanham: Rowman & Littlefield")
   ))

@(define SY-LW
  (make-bib
   #:title "Life-world: Beyond Fukushima and Minamata 「いのちの世界」: フクシマとミナマタを超えて"
   #:author "Shoko Yoneyama"
   #:date "2012"
   #:location (journal-location "The Asia-Pacific Journal"
				#:volume "Vol 10"
				#:number "Issue 42")
   #:url "https://apjjf.org/2012/10/42/Shoko-YONEYAMA/3845/article.html"))

@(define OM-KH-talk
  (make-bib
   #:title "対談: 祈りの語り"
   #:author (authors "緒方正人" "栗原 彬")
   #:date  "2010" ;; when?
   #:location (book-chapter-location "" #:volume "チッソは私であった"
				     #:pages (list 177 237))))
@(define AC-NBs
  (make-bib
   #:title "Notebooks 1935-1942"
   #:author "Albert Camus"
   #:date "1963"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Harcourt Brace Jovanovich")
   #:note "Originally published in French under the title Carnets, mai 1935---février 1942 in 1962. Published in English in Great Britain under the title Carnets 1935---1942 by Hamish Hamilton Ltd. This Harvest/HBJ book is a reprint of the 1969 ed. published by Knopf, New York."))
@(define NC-PnP
  (make-bib
   #:title "Powers & Prospects: Reflections on human nature and the social order"
   #:author "Noam Chomsky"
   #:is-book? #t
   #:date "1996"
   #:location (book-location #:publisher "Boston: South End Press")
   ;; #:url "https://zcomm.org/powers-and-prospects/" ; not complete
   ))
@(define NC-PnP-DemMarkets
  (make-bib
   #:title "Democracy and Markets in the New World Order"
   #:author "Noam Chomsky"
   #:date "1996"
   #:location (book-chapter-location ""
                                     #:volume "Powers & Prospects: Reflections on human nature and the social order"
                                     #:pages (list 94 131))
   ))
@(define NC-WO-Delhi
  (make-bib
   #:title " World Orders, Old and New In: Democracy and Power: The Delhi Lectures"
   #:location (book-location #:publisher "Cambridge: Open Book Publishers")
   #:url "http://books.openedition.org/obp/2165"
   #:date "2014"
   #:note "Accessed 2021/10/06"))
@(define NC-PnP-WritersResponsibility
  (make-bib
   #:title "Writers and Intellectual Responsibility"
   #:author "Noam Chomsky"
   #:date "1996"
   #:is-book? #f
   #:location (book-chapter-location ""
                                     #:volume "Powers & Prospects: Reflections on human nature and the social order"
                                     #:pages (list 55 69))

   ))
@(define EP1-KKS ; 教科書
  (make-bib
   #:title "絵で見る英語 Book 1"
   #:author (authors "I.A. Richards" "Christine Gibson")
   #:date "2005"
   #:is-book? #t
   #:location (book-location #:publisher "Japan: IBCパブリッシング")
   #:note "First copyrights in 1945, 1946 by Language Research, Inc. then in 1973 by I.A. Richards and Christine Gibson, then in 2004 by the President and Fellows of Harvard College."))

@(define YK-BkUse
  (make-bib
   #:title "この本の使い方"
   #:author "片桐 ユズル"
   #:date "2005"
   #:location (book-chapter-location ""
				     #:volume "絵で見る英語 Book 1"
				     #:pages (list "i" "v"))))
@(define EP1-1973
  (make-bib
   #:title "English Through Pictures and A First Workbook of English"
   #:author (authors "I.A. Richards" "Christine Gibson")   
   #:date "1973"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Pocket Books of Simon & Schuster")
   #:note "This version 'combines the first half of the previous Book I with the Workbook I to form the new Book I, this volume contains a vocabulary of 250 wrods: the 500-word vocabulary is completed in Englsh Through Pictures Book II'"))

@(define EP1-Ppn
  (make-bib
   #:title "English Through Pictures Book 1: Including a First Workbook of English"
   #:author (authors "I.A. Richards" "Christine M. Gibson")
   #:is-book? #t
   #:date "2005"
   #:location (book-location #:publisher "Toronto: Pippin Publishing")
      #:note "Earlier copyrights in 1945, 1946, 1973 and 2004 for English Through Pictures, in 1959 for A First Workbook of English"))

@(define EP1-Ppn-notes
  (make-bib
   #:title "Notes on the re-issue and update of English Through Pictures"
   #:author "Archie MacKinnon"
   #:date "2005"
   #:location (book-chapter-location ""
				     #:volume "English Through Pictures Book 1: Including a First Workbook of English"
				     #:pages (list "i" "ii"))))

@(define EP1-Ppn-suggestions
  (make-bib
   #:title "Suggestions to the Beginner"
   #:author (authors "I.A. Richards" "Christine Gibson")
   #:date "2005"
   #:location (book-chapter-location ""
				     #:volume "English Through Pictures Book 1: Including a First Workbook of English"
				     #:pages (list "iii" "iv"))))

@(define EP2-Ppn
  (make-bib
   #:title "English Through Pictures Book 2: Including a Second Workbook of English"
   #:author (authors "I.A. Richards" "Christine M. Gibson")
   #:is-book? #t
   #:date "2005"
   #:location (book-location #:publisher "Toronto: Pippin Publishing")
   #:note "Earlier copyrights in 1945, 1946, 1973 and 2004 for English Through Pictures, in 1959 for A First Workbook of English"))

@(define EP2-Ppn-preface
  (make-bib
   #:title "Preface"
   #:author (authors "I.A. Richards" "Christine Gibson")
   #:date "2005"
   #:location (book-chapter-location ""
				     #:volume "English Through Pictures Book 2: Including a Second Workbook of English"
				     #:pages (list "iii" "iii"))))

@(define EP3-Ppn
  (make-bib
   #:title "English Through Pictures Book 3"
   #:author (authors "I.A. Richards" "Christine M. Gibson")
   #:is-book? #t
   #:date "2005"
   #:location (book-location #:publisher "Toronto: Pippin Publishing")
   #:note "Earlier copyrights in 1973 and 2004 by I.A. Richards and the President and Fellows of Harvard Collegs respectively. This book mistakenly included itself, the 1957 Book II that became book III, with the 1945 Book I which were separated to become Books I and II and include workbook sections "))

@(define EP3-Ppn-preface
  (make-bib
   #:title "Preface"
   #:author (authors "Christine M. Gibson" "I.A. Richards")
   #:date "2005"
   #:location (book-chapter-location ""
				     #:volume "English Through Pictures Book 3"
				     #:pages (list "iii" "iii"))))
@(define EP2-KKS
  (make-bib
   #:title "絵で見る英語 Book 2"
   #:author (authors "I.A. Richards" "Christine Gibson")
   #:date "2005"
   #:is-book? #t
   #:location (book-location #:publisher "Japan: IBCパブリッシング")
   #:note "First copyrights in 1945, 1946 by Language Research, Inc. then in 1973 by I.A. Richards and Christine Gibson, then in 2004 by the President and Fellows of Harvard College."))

@(define EP3-KKS
  (make-bib
   #:title "絵で見る英語 Book 3"
   #:author (authors "I.A. Richards" "Christine Gibson")
   #:date "2005"
   #:is-book? #t
   #:location (book-location #:publisher "Japan: IBCパブリッシング")
   #:note "First copyrights in 1957 by Language Research, Inc. as English Through Pictures Book 2 (EP 1 and 2 were one book with the workbooks separate) then in 1973 by I.A. Richards and Christine Gibson, then in 2004 by the President and Fellows of Harvard College."))

@(define GM-TPM
(make-bib
#:title "The Population Myth"
#:author "George Monbiot"
#:date "2009"
#:is-book? #f
#:location "George Monbiot's on-line site"
#:url "https://www.monbiot.com/2009/09/29/the-population-myth/"
#:note "Accessed 2021/11/11. Published in the Guardian, 29th September 2009."
))
@(define SG-HtoHD-p
(make-bib
#:title "How the Other Half Dies: The real reasons for world hunger"
#:author "TNI"
#:date "2015"
#:is-book? #f
#:location "Transnational Institute's on-line site"
#:url "https://www.tni.org/en/publication/how-the-other-half-dies"
#:note "Accessed 2021/10/29."
))
@(define SG-HtoHD-b
(make-bib
#:title "How the Other Half Dies: The Real Reasons for World Hunger"
#:author "Susan George"
#:date "1976"
#:is-book? #f
#:location (book-location #:publisher "New York: Viking Penguin")
#:url "https://www.tni.org/files/download/howtheotherhalfdies.pdf"
#:note "Susan George's classic study of world hunger. Re-released in 2009 as free online download."
))
@(define SG-HtoHD-b-ja
(make-bib
#:title "なぜ世界の半分が飢えるのか:食料危機の構造"
#:author "スーザン・ジョージ"
#:date "1984"
#:is-book? #t
#:location (book-location #:publisher "東京: 朝日新聞社")
#:note "Japanese translation of Susan George's How the Other Half Dies"
))
@(define DG-Debt
  (make-bib
   #:title "Debt: The First 5,000 Years"
   #:author "David Graeber"
   #:date "2011"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Melville House Publishing")))

@(define AB-RoR
  (make-bib
   #:title "Richards on Rhetoric: I.A. Richards Selected Essays 1929-1974"
   #:author "Ann E. Berthoff"
   #:date "1991"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Oxford University Press")))
   
@(define UnderPoetry
  (make-bib
   #:title "Understanding Poety"
   #:author (authors "Cleanth Brooks" "Robert Penn Warren")
   #:date "1960"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Holt, Rinehart and Winston"
                             #:edition "Third")
   #:note "Earlier copyrights are from 1938 and 1950. Marchal McLuhan's Understanding Media may have been started with this book has a model, or goal."))

@(define UP-Yeats-2songs
  (make-bib
   #:title "Two Songs from a Play"
   #:author (authors "Cleanth Brooks" "Robert Penn Warren")
   #:date "1960"
   #:is-book? #f
   ;;#:url
   #:location (book-chapter-location ""
                                     #:volume "Understanding Poetry: Third Edition")))

@(define Simply-Scheme
  (make-bib
   #:title "Simply Scheme: Introducing Computer Science"
   #:author (authors "Brian Harvey" "Matthew Wright")
   #:date "1999"
   #:is-book? #t
   #:url "https://people.eecs.berkeley.edu/~bh/ss-toc2.html"
   #:location (book-location #:publisher "Cambridge: The MIT Press"
                             #:edition "Second")))

@(define NC-CL
  (make-bib
   #:title "Cartesian Linguistics: A Chapter in the History of Rationalist Thought."
   #:author "Noam Chomsky"
   #:date "2009"
   #:is-book? #t
   #:location (book-location #:publisher "Cambridge: Cambridge University Press"
			     #:edition "Third")
   ))
@(define CL-Intro-JM
  (make-bib
   #:title "Introduction to the third edition"
   #:author "James McGilvray"
   #:date "2009"
   #:location (book-chapter-location ""
				     #:volume "Cartesian Linguistics: A Chapter in the History of Rationalist Thought."
				     #:publisher "Cambridge: Cambridge University Press"
				     #:pages (list 1 52))))
@(define GDM-about
  (make-bib
   #:title "GDMとは"
   #:author "GDM website"
   #:url "https://www.gdm-japan.net/introduction/about-gdm/"
   #:date "2021"
   #:note "Accessed 2021/10/07"))
@(define KSR-FSoR
  (make-bib
   #:title "Forty Signs of Rain"
   #:author "Kim Stanley Robinson"
   #:date "2005"
   #:location (book-location #:publisher "New York: Bantam Dell"
			     #:edition "mass market")
   #:note "Bantan hardcover edition published June 2004"))
@(define Iar-PoLC
  (make-bib
   #:title "Principles of Literary Criticism"
   #:author "I.A. Richards"
   #:date "1930"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Harcourt, Brace and Company")
   #:note "First Edition was in 1924, Second edition (with two new Appendices) was in 1926, my copy is the 'Fourth Impression'"))
@(define WE-7A
  (make-bib
   #:title "7 Types of Ambiguity"
   #:author "William Empson"
   #:date "1966"
   #:is-book? #t
   #:location (book-location #:publisher "New York: New Directions")
   #:note "First U.S> edition, New Directions 1947 (revised from British 1930 edition)"))
@(define Iar-Clrdg
  (make-bib
   #:title "The Portable Coleridge"
   #:author "Samuel Taylor Coleridge"
   #:date "1950"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Viking Press")
   #:note "Edited, and with and Introduction by I.A. Richards"))
@(define TJ-BSDH
  (make-bib
   #:title "文章読本"
   #:author "谷崎潤一郎"
   #:date "1996"
   #:is-book? #t
   #:location (book-location #:publisher "東京: 中央公論新社")
   #:note "初版昭和九年、1934. 1975初版、1966改版"))
@(define RS-TiS
  (make-bib
   #:title "The Tao is Silent"
   #:author "Raymond Smullyan"
   #:date "1977"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Harper & Row")))
@(define IaR-SSWTE
  (make-bib
   #:title "A Semantically Sequenced Way of Teaching English: Selected and Uncollected Writings by I.A. Richards"
   #:author "I.A. Richards"
   #:date "1993"
   #:is-book? #t
   #:location (book-location #:publisher "Kyoto: Yamaguchi Publishing House")
   #:note "Edited, with and introduction Yuzuru Katagiri, John Constable"))
@(define IaR-LBE
  (make-bib
   #:title "Learning Basic English"
   #:author (authors "I.A. Richards" "Christine Gibson")
   #:date "1945"
   #:location (book-location #:publisher "New York: W.W. Norton & Company")
   ))
@(define IaR-PsASs
  (make-bib
   #:title "Poetries and Sciences: A reissue with a commentary of Science and Poetry (1926, 1935)"
   #:author "I.A. Richards"
   #:date "1970"
   #:is-book? #f
   #:location  (book-location #:publisher "1970: W.W. Norton & Company")
   #:note "Science and Poetry was first published in 1926 and revised in 1935"
   ))
@(define CC-RTI
  (make-bib
   #:title "The Rising Tide of Insignificancy (The Big Sleep) [RTI]"
   #:author "Cornelius Castoriadis"
   #:date "2003"
   #:is-book? #t ;; online .pdf book but no book-location #:publisher etc
   #:location (book-location #:publisher "2003:notbored.org")
   #:url "http://www.notbored.org/RTI.pdf"
   #:note "Translated from the French and edited anonymously as a public service. Electronic publication date: December 2003."))

@(define CC-IoR
  (make-bib
   #:title "The Idea of Revolution"
   #:author "Cornelius Castoriadis"
   #:date "1990"
   #:url "http://www.notbored.org/RTI.pdf"
   #:location (book-chapter-location ""
				     #:volume "The Rising Tide of Insignificancy (The Big Sleep) [RTI]"
				     #:publisher "2003:notbored.org"
				     #:pages (list 288 310))
   #:note "The translation originally appeared \"Does the Idea of Revolution Still Make Sense?\" in Thesis Eleven, 26 (1990), 123-38"))

@(define IaR-PoemWhenFinished
  (make-bib
   #:title "How Does a Poem Know When It Is Finished?"
   #:author "I.A. Richards"
   #:date "1963"
   #:is-book? #f ;; part of a book so #t??
   #:location (book-chapter-location ""
				     #:volume "Poetries and Sciences"
				     #:publisher "1970: W.W. Norton & Company"
				     #:pages (list 105 118))
   #:note "Reprinted with permission of The Macmillan Company from Parts and Wholes: The Hayden Colloquium on Scientific Method and Concept, edited by Daniel Lerner, MIT, 1963"))

@(define IaR-PC
  (make-bib
   #:title "Practical Criticism: A Study of Literary Judgment"
   #:author "I.A. Richards"
   #:date "1929"
   #:location (book-location #:publisher "New York: Harcourt, Brace & World")
   #:is-book? #t))
@(define BB-Edu-Age-of-Science
  (make-bib
   #:title "Introduction to Education in the Age of Science"
   #:author "Brand Blanshard"
   #:url "http://www.anthonyflood.com/blanshardeducationageofscience.htm"
   #:date "1959"
   #:note "Accessed 2021/09/27. From Education in the Age of Science, Brand Blanshard, ed. New York: Basic Books, 1959, vii-xviii."))
@(define MLW-DH
  (make-bib
   #:title "How to Use Scribble to Write your Academic Papers: A Reference Tutorial"
   #:date "2021"
   #:author "Morgan Lemmer-Weber"
   #:url "https://dustycloud.org/misc/digital-humanities/HowTo.html"
   #:note "Accessed 2021/09/28. A very helpful supplement to the Racket/Scribble documentation."))

@(define Scrbl-Dcmtn
  (make-bib
   #:title "Scribble: The Racket Documentation Tool"
   #:author (authors "Matthew Flatt" "Eli Barzilay")
   #:url "https://docs.racket-lang.org/scribble/index.html"
   #:note "Accessed 2021/09/29."))
@(define PG-icad
  (make-bib
   #:title "LISP is classic"
   #:author "Paul Graham"
   #:date "2018"
   #:url "http://www.paulgraham.com/icad.html"
   #:note "Accessed 2018/03/29."))
@(define RF-HP
  (make-bib
   #:title "Robert Bruce Findler's Home page"
   #:author "Robert Bruce Findler"
   #:date "2021"
   #:url "https://users.cs.northwestern.edu/~robby/"
   #:note "Accessed 2021/09/29"))
@(define MIT-SICP-lect-note-1
  (make-bib
   #:title "1A:Overview and Introduction to Lisp"
   #:author (authors "Hal Abelson" "Gerald Jay Sussman")
   #:date "1986"
   #:url "https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-001-structure-and-interpretation-of-computer-programs-spring-2005/video-lectures/1a-overview-and-introduction-to-lisp/"
   #:note "Accessed 2021/09/29."))
@(define HTDP-1
  (make-bib
   #:title "How to Design Programs"
   #:author (authors "Matthias Felleisen" "Robert Bruce Findler" "Matthew Flatt" "Shriram Krishnamurthi")
   #:date "2001"
   #:location (book-location #:publisher "Cambridge: The MIT Press")
   #:url "www.htdp.org"
   ))
@(define SK-CD-Engineering
  (make-bib
   #:title "Curriculum Design as an Engineering Problem"
   #:author "Shriram Krishnamurthi"
   #:url "https://www.youtube.com/watch?v=rM_E2IwlprY"
   #:date "2018"))
@(define SK-PS-about
  (make-bib
   #:title "About"
   #:author "Shriram Krishnamurthi"
   #:url "https://parentheticallyspeaking.org/about/"
   #:date "2021"
   #:note "Accessed 2021/09/29"))
@(define MA-DGs
  (make-bib
   #:title "A Brief History & Ethos of the Digital Garden"
   #:author "Maggie Appleton"
   #:date "2021"
   #:url "https://maggieappleton.com/garden-history"
   #:note "Accessed 2021/09/29"
   ))
@(define RMS-EP
  (make-bib
   #:title "EMACS: The Extensible, Customizable Display Editor"
   #:author "Richard Stallman"
   #:date "1981"
   #:url "https://www.gnu.org/software/emacs/emacs-paper.html"
   #:note "Accessed 2021/09/29"))

@(define Wirth-Law-WkP
  (make-bib
   #:title "Wirth's Law"
   #:author "Wikipedia"
   #:date "2021"
   #:url "https://en.wikipedia.org/wiki/Wirth%27s_law"
   #:is-book? #f
   #:note "Accessed 2021/09/29."))

@(define LH-MfM
  (make-bib
   #:title "Mathematics for the Millions: How to Master the Magic of Numbers"
   #:author "Lancelot Hogben"
   #:date "1971"
   #:location (book-location #:publisher "New York: W.W. Norton & Company")
   #:note "First Copyright 1937. First published as a Norton paperback 1983; reissued 1993"
   #:url "https://archive.org/details/HogbenMathematicsForTheMillion"))
   
@(define IaR-SI
  (make-bib
   #:title "Speculative Instruments"
   #:author "I.A. Richards"
   #:date "1955"
   #:is-book? #t
   #:url #f
   #:location (book-location #:publisher "Chicago: University of Chicago Press")
   #:note "Essays which form an inquiry into what happens when somebody understands something and into the questions which this question brings up."))
@(define IaR-SI-RiToE
  (make-bib
   #:title "Responsibilities in the Teaching of English"
   #:author "I.A. Richards"
   #:date "1955"
   #:location (book-chapter-location "VIII"
				      #:volume "Speculative Instruments"
				      #:pages (list 91 106))
   ))
@(define IaR-SI-IoU
  (make-bib
   #:title "The Idea of a University"
   #:author "I.A. Richards"
   #:date "1955"
   #:is-book? #f
   #:location  (book-chapter-location "IX"
				       #:volume "Speculative Instruments"
				       #:pages (list 107 112))
   ))
@(define Iar-SI-SO
  (make-bib
   #:title "Toward a More Synoptic View"
   #:author "I.A. Richards"
   #:date "1955"
   #:location  (book-chapter-location "X"
				      #:volume "Speculative Instruments"
				      #:pages (list 113 126))
   #:note ""
))
@(define AR-SB-Prospect
  (make-bib
   #:title "India’s shame"
   #:author "Arundhati Roy"
   #:date "2014"
   #:is-book? #f
   #:location "Prospect Magazine on-line Site"
   #:url "https://www.prospectmagazine.co.uk/magazine/indias-shame"
   #:note "Accessed 2021/10/15. Article from December 2014 issue of Prospect Magazine"
   ))
@(define IaR-PsnSs
  (make-bib
   #:title "Poetries and Sciences"
   #:author "I.A. Richards"
   #:date "1970"
   #:is-book? #t
   #:location (book-location #:publisher "New York: W.W. Norton & Company")
   #:note "a reissue with a commentary of Science and Poetry (1926, 1935)"
   ))
@(define GTherapy
  (make-bib
   #:title "Gestalt Therapy: Excitement and Growth in the Human Personality"
   #:author (authors "Frederick S. Perls" "Ralph Hefferline" "Paul Goodman")
   #:date "1994"
   #:location (book-location #:publisher "Gouldsboro: The Gestalt Journal Press")))
@(define GTherapy-Intro
  (make-bib
   #:title "Introduction to The Gestalt Journal Edition of Gestalt Therapy"
   #:author (authors "Isadore From" "Michael Vincent Miller")
   #:date "1994"
   #:location (book-chapter-location ""
				     #:volume  "Gestalt Therapy: Excitement and Growth in the Human Personality"
				     #:pages (list "viii" "xxii"))))
@(define JWS-RPFW
  (make-bib
   #:title "Racket Programming the Fun Way"
   #:author "James W. Stelly"
   #:date "2021"
   #:is-book? #t
   #:location (book-location #:publisher "San Franciso: No Starch Press")
   ))
@(define DTC
  (make-bib
   #:title "don't teach coding until you read this book"
   #:author (authors "Stephen R. Foster" "Lindsey D. Handley")
   #:date "2020"
   #:is-book? #t
   #:location (book-location #:publisher "Hoboken: John Wiley & Sons")
   ))
@(define RS-SCI
  (make-bib
   #:title "Satan, Cantor & Infinity"
   #:author "Raymond M. Smully"
   #:date "1992"
   #:is-book? #t
   #:location (book-location #:publisher "Mineola: Dover Publications")
   ))
@(define RLS-TP
  (make-bib
   #:title "Learning Perl: Making Easy Things Easy & Hard Things Possible"
   #:author (authors "Randal L. Schwartz" "Tom Phoenix")
   #:date "2001"
   #:is-book? #t
   #:location (book-location #:publisher "Sebastopol: O'Reilly Media"
			     #:edition "Third")
   ))
@(define PS-PCL
  (make-bib
   #:title "Practical Common Lisp"
   #:author "Peter Seibel"
   #:date "2005"
   #:is-book? #t
   #:location (book-location #:publisher "Berkely: Apress")
   ))
@(define PS-PCL-Ja
  (make-bib
   #:title "実践 Common Lisp"
   #:author "Peter Seibel"
   #:date "2008"
   #:is-book? #t
   #:location (book-location #:publisher "オーム社")
   #:note "Practical Common Lispの平成20年、佐野匡俊 水丸淳 園城雅之 金子祐介 の共訳"))
@(define TK-UR
  (make-bib
   #:title "シリーズUseful R (9) ドキュメント・プレゼンテーション生成"
   #:author "高橋康介"
   #:date "2014"
   #:is-book? #t
   #:location (book-location #:publisher "東京: 共立出版")
   ))
@(define JH-FaB
  (make-bib
   #:title "Freedom and Beyond"
   #:author "John Holt"
   #:date "1983"
   #:location (book-location #:publisher "Boston: Holt Associates")
   #:note "The Pinch Penny Press edition of the original 1972 edition"
   ))
@(define IaR-SI-Ftr-Hmnts
  (make-bib
   #:title "The Future of the Humanities in General Education"
   #:author "I.A. Richards"
   #:date "1955"
   #:location (book-chapter-location "IV"
				     #:volume "Speculative Instruments"
				     #:publisher "Chicago: University of Chicago PRess")))
   
@(define IaR-LitCrit-Science
  (make-bib
   #:title "Notes toward an Argreement between Literary Criticism and Some of the Sciences"
   #:author "I.A. Richards"
   #:date "1955"
   #:location (book-chapter-location "I"
				     #:volume "Speculative Instruments"
				     #:publisher "Chicago: Unversity of Chicago Press")))


@(define IaR-BEuses
  (make-bib
   #:title "Basic English and Its Uses"
   #:author "I.A. Richards"
   #:date "1943"
   #:is-book? #t
   #:location (book-location #:publisher "New York: W.W. Norton & Company")))

@(define IaR-PoR
  (make-bib
   #:title "The Philosophy of Rhetoric"
   #:author "I.A. Richards"
   #:date "1936"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Oxford University Press")
   #:note "Lectures that were delivered at Bryn Mawr Colleg, February and March 1936 on a fund established by Bernard Flexner in honor of his sister: The Mary Flexner Lectures on the Humanities III."))
@(define IaR-PoR-Ja
  (make-bib
   #:title "新修辞学原論"
   #:author "I.A. リチャーズ"
   #:date "1961"
   #:is-book? #t
   #:location (book-location #:publisher "東京: 南雲堂")
   #:note "訳者: 石橋幸太郎. Translation of The Philosophy of Rhetoric by I.A. Richards."
   ))
@(define IaR-HtRaP
  (make-bib
   #:title "How to Read a Page: A course in efficient reading with an introduction to 100 great words"
   #:author "I.A. Richards"
   #:date "1942"
   #:is-book? #t
   #:location (book-location #:publisher "USA: W.W. Norton & Company")
   #:note "First published as a Beacon Paperback in 1959"))
			    
@(define IaR-DfE
  (make-bib
   #:title "Design for Escape: World Education Through Modern Media."
   #:author "I.A. Richards"
   #:date "1968"
   #:is-book? #t
   #:location (book-location #:publisher "New York: Harcourt, Brace & World")))

@(define BR-OE
  (make-bib
   #:title "On Education"
   #:author "Bertrand Russel"
   #:is-book? #t
   #:date "1926"
   #:location (book-location #:publisher "Abingdon: Routledge")
   #:note "First published in the Routledge Classics in 2010. Also available on-line with Japanese translation: https://www.russell-j.com/beginner/OE-CONT.HTM"))

@(define BR-HaA
  (make-bib
   #:title "History as an Art"
   #:author "Bertrand Russel"
   #:date "1954"
   #:is-book? #f
   #:url "https://www.russell-j.com/1057_HasA.HTM"
   #:note "accessed 2021/09/28"))
  
@(define DGraddol-ENext
    (make-bib
     #:title "English Next"
     #:author "David Graddol"
     #:url "https://www.teachingenglish.org.uk/article/english-next"
     #:is-book? #t
     #:date "2006"
     #:location (book-location #:publisher "Plymoth: Latimer Trend & Company")))

@(define EigoBakaNaru
  (make-bib
   #:title "英語を学べばバカになる: グローバル思考という妄想"
   #:author "薬師院仁志"
   #:is-book? #t
   #:date "2005"
   #:location (book-location #:publisher "東京: 光文社")))
@(define EiTangoRevo
  (make-bib
   #:title "英単語レボリューション Book 1"
   #:author "宮岸羽合" ;; Miyagishi Hago
   #:date "2010"
   #:location (book-location #:publisher "東京: 南雲堂")))

@(define StatsTutor-Categorical
  (make-bib
   #:title "Summarising categorical variables in R"
   #:url "https://www.sheffield.ac.uk/polopoly_fs/1.714591!/file/stcp-karadimitriou-categoricalR.pdf"
   #:date 2020 ; accessed 2020年  3月 28日 土曜日 14:52:17 JST
   #:author (authors "Sofie Maria Karadimitrious" "Ellen Marshall")))

@(define VoxCorona
  (make-bib
   #:title "These 2 questions will determine if the coronavirus becomes a deadly pandemic"
   #:author "Julia Belluz"
   #:date  2020  ; "Feb 18, 2020"
   #:url "https://www.vox.com/2020/2/18/21142009/coronavirus-covid19-china-wuhan-deaths-pandemic"
   ))

@(define SolomonMessingBlog
  (make-bib
   #:title "Insight From Cleveland And Tufte On Plotting Numeric Data By Groups"
   #:author "Solomon Messing"
   #:date "2012"
   #:url "https://solomonmg.github.io/blog/2012/visualization-series-insight-from-cleveland-and-tufte-on-plotting-numeric-data-by-groups/"
   ))
@(define McKTeachTips
  (make-bib
   #:title "McKeachie's Teaching Tips: Strategies, Research, and Theory for Colleg and University Teachers"
   #:author (authors "Marilla D. Svinicki" "Wilbert J. McKeachie")
   #:date "2014"
   #:location (book-location #:publisher "Wadsworth Cengage Learning")
   #:is-book? #t))
@(define NC-DDemocracy
  (make-bib
   #:title "Deterring Democracy"
   #:author "Noam Chomsky"
   #:location (book-location #:publisher "Hill and Wang")
   #:date "1991"
   #:is-book? #t))
@(define TufteBeautifulEvidence
  (make-bib
   #:title "Beautiful Evidence"
   #:author "Edward Tufte"
   #:location (book-location #:publisher "Graphics Press")
   #:date "2006"
   #:is-book? #t))
@(define WendBerryWhatFor
  (make-bib
   #:title "What are People For?"
   #:author "Wendell Berry"
   #:location (book-location #:publisher "Counterpoint")
   #:date "1990"
   #:is-book? #t))

@(define WainerOrderAdvice
   (make-bib
    #:title "Understanding Graphs and Tables"
    #:author "Howard Wainer"
    #:location (journal-location "Educational Researcher"
                       #:pages '(14 23)
		       #:number "No. 1 (Jan. - Feb., 1992)"
		       #:volume "Vol. 21")
    #:date "1992"
    #:url "http://www.jstor.org/stable/1176346"))

@(define bib-Koizumi202108
  (make-bib
   #:title "映画のこと"
   #:author "小泉初恵"
   #:url "http://www.soshisha.org"
   #:location (journal-location "ごんずいGONZUI"
				#:pages '(13 14)
				#:number "162"
				#:volume "一般財団法人 水俣病センター 相思社")
   #:date "2021"))

@(define bib-Nagano202108
  (make-bib
   #:title "アイリーン美緒子スミスさんインタビュー"
   #:author (authors "永野路美智" "坂本一途")
   #:date "2021"
   #:is-book? #f
   #:location (techrpt-location #:institution "一般財団法人 水俣病センター 相思社"
				#:number "ごんずいGONZUI-162")
   #:url "http://www.soshisha.org"
   #:note "pp. 3-10"))

@(define WindFarm-Nakamura-Aileen-10tricks
  (make-bib
   #:title "水俣と福島に共通する１０の手口"
   #:author "中村陸市"
   #:date "2012"
   #:is-book? #f
   #:location (techrpt-location #:institution "ウィンドファール・ブログ 「風の便り」"
			       #:number "2012/04/16")
   #:url "https://www.windfarm.co.jp/blog/blog_kaze/post-9710"
   #:note "元は：「かつて水俣を、今福島を追う　アイリーン・美緒子・スミスさんに聞く」（毎日新聞　2012年02月27日　東京夕刊）"))

@(define KyotoJournal2021AileenPt1
  (make-bib
   #:title "Documenting Minamata with W. Eugene Smith"
   #:author (authors "Jennifer Teeter" "Ken Rodgers")
   #:date "2021"
   #:is-book? #f
   #:location (techrpt-location #:institution "Kyoto Journal"
			       #:number "KJ99: Travel, Revisited, Dec.2020")
   #:url "https://www.kyotojournal.org/conversations/aileem-mioko-smith-interview-pt1/"
   #:note "An extended version of Interview with Aileen Mioko Smith, part 1"))
   
@(define KyotoJournal2021AileenPt2
  (make-bib
   #:title "Four Decades of Nuclear Activism"
   #:author (authors "Jennifer Teeter" "Ken Rodgers")
   #:date "2021"
   #:is-book? #f
   #:location (techrpt-location #:institution "Kyoto Journal"
			       #:number "KJ99: Travel, Revisited, Dec.2020")
   #:url "https://www.kyotojournal.org/conversations/aileem-mioko-smith-interview-pt2/"
   #:note "An extended version of Interview with Aileen Mioko Smith, part 2"))

@(define Aravinda-India-Dams-Paperless
  (make-bib
   #:title "People's Knowlege in a Paperless Society"
   #:author "Ls Aravinda"
   #:date "2001"
   #:is-book? #f
   #:url "https://zcomm.org/zmagazine/peoples-knowledge-in-a-paperless-society-by-ls-aravinda/"
   #:note "An article about how courts in India ignore the concerns of tribal villages in order to push through the funding for Sardar Sarovar Project, one of the Narmada Valley Development Projects."
   #:location (techrpt-location #:institution "Z Magazine"
				#:number "January 1, 2001")))

@(define BB-ResonHabit
  (make-bib
   #:title "The Habit of Reason"
   #:author "Brand Blanshard"
   #:date "1984"
   #:is-book? #f
   #:url "http://www.anthonyflood.com/blanshardhabitofreason.htm"
   #:note "accessed 2021 09 27, originally Boston University's 111th Commencement appeared in Free Inquiry, Winter 1985-86, 22-25 when he was 92 years old."))

@(define DCL
   (make-bib
    #:title "対話によるCommon Lisp 入門"
    #:author "栗原正仁"
    #:location (book-location #:publisher "森北出版社株式会社")
    #:date "1993"
    #:is-book? #t))

@(define JBerger-UnderstandingPhotograph
  (make-bib
   #:title "Understading a Photograph"
   #:author (authors "John Berger" "Geoff Dyer")
   #:is-book? #t
   #:date "2013"
   #:location (book-location #:publisher "New York: Aperture")
   #:note "A collection of essays about Photography: photographers, albums, and photos."))

@(define BasilDavidson-ModernAfrica
  (make-bib
   #:title "Modern Africa: A Social and Political History"
   #:author "Basil Davidson"
   #:is-book? #t
   #:date "1994"
   #:location (book-location #:edition "Third"
			     #:publisher "Singapore: Pearson Education Asia")
   #:note "First published in 1983"))
@(define TLE1st  ;; red book
    (make-bib
   #:title "Toward Liberal Education"
   #:author (authors "Louis g. Locke" "William M. Gibson" "George Arms")
   #:is-book? #t
   #:date "1952" ;; 
   #:location (book-location #:edition "Revised" #:publisher "New York: Holt Rinehart and Winston")
   #:note (elem "Eight Printing,July 1955. This collection of classic essays, first copyrighted in 1948, was originally half of a larger book," (italic "Readings for Liberal Education") " which included " (italic "Introduction to Literature"))))
@(define TLE-Rvsd-GenSpec
  (make-bib
   #:title "General and Special Education"
   #:author "The Harvard Committee"
   #:date "1955"
   #:is-book? #f
   #:location (book-chapter-location ""
                             #:volume "Toward Liberal Education: Revised Edition"
                             #:pages (list 63 67))
   #:note "In the red book this part of another 'red book' is Reprinted by permission of the publishers from Paul H. Buck and others. General Education in a Free Society: The Report of the Harvard Committee, Cambridge, Mass: Harvard University Press, 1945."
   ))
@(define TLE4th ;; blue book 
  (make-bib
   #:title "Toward Liberal Education"
   #:author (authors "Louis g. Locke" "William M. Gibson" "George Arms")
   #:is-book? #t
   #:date "1962"
   #:location (book-location #:edition "Fourth" #:publisher "New York: Holt Rinehart and Winston")
   #:note "This collection of classic essays, first copyrighted in 1948, was originally half of a larger book, Readings for Liberal Education which included Introduction to Literature"))

@(define SIHaya-Poetry-Adverts
  (make-bib
   #:title "Poetry and Advertising"
   #:author "S.I.Hayakawa"
   #:location (book-chapter-location ""
				     #:pages (list 372 377)
				     #:volume "Toward Liberal Education: First Edition"
				     #:publisher  "New York: Holt Rinehart and Winston")
   #:date "1955"
   #:is-book? #t
   #:note "Originally in Poetry: A Magazine of Verse, LXVII (January, 1946), 204-212. A Paper given at the Sixth Conference on Science, Philosophy, and Religion."))

@(define ToynbeeHistoryRepeat
  (make-bib
   #:title "Does History Repeat Itself?"
   #:author "Arnold J. Toynbee"
   #:location (book-chapter-location ""
				     #:pages (list 685 691)
				     #:volume "Toward Liberal Education: Fourth Edition"
				     #:publisher "New York: Holt Rinehart and Winston")
   #:date "1962"
   #:is-book? #t
   #:note "Originally part of the the book Civilization on Trial(1948) this essay is not seen in other editions of the essay collection Towards Liberal Education"))

@(define HutchinsCandles
  (make-bib
   #:title "That Candles May Be Brought"
   #:author "Robert M. Huthchins"
   #:date "1962"
   #:is-book? #t
   #:location (book-chapter-location ""
				     #:volume "Toward Liberal Education: Fourth Edition"
				     #:pages (list 192 195)
				     #:publisher "New York: Holt Rinehart and Winston")
  #:note "A version of this essay was the 1957 Hunter College Commencement Address: https://library.hunter.cuny.edu/old/sites/default/files/111th_commencement_address.pdf"))

@(define EduAsPhilo
  (make-bib
   #:title "Education as Philosophy"
   #:author "Brand Blanshard"
   #:date "1945"
   #:is-book? #f
   #:location (book-chapter-location ""
                                     #:volume "Readings for Liberal Education"
                                     #:pages (list 587 596)
                                     #:publisher "New York: Rinehart & Company")
   #:note "From Swarthmore College Bulletin, XLII, No. 4 (July, 1945)"))
                                    
@(define RFLE
  (make-bib
  #:title "Readings for Liberal Education"
  #:author (authors "Louic G. Locke" "William M. Gibson" "George Arms")
  #:date "1948"
  #:is-book? #t
  #:location (book-location #:publisher "New York: Rinehart & Company")
  #:note "Contains I. Toward Liberal Education and II. Introduction to Literature."))
    
@(define TLS
   (make-bib
    #:title "The Little Schemer"
    #:author (authors "Daniel P. Friedman" "Matthias Felleisen")
    #:date "1996"
    #:location (book-location #:publisher "MIT")
    #:is-book? #t))

@(define HHTL
   (make-bib
    #:title "初めての人のためのLISP:増補改訂版"
    #:author "竹内郁雄"
    #:location (book-location #:publisher "翔泳社")
    #:date "2010"
    #:is-book? #t))

@(define EPTB ; Emacs Lisp Technic Bible
   (make-bib
    #:title "Emacs LISP テクニックバイブル"
    #:author "るびきち"
    #:date "2011"
    #:location (book-location #:publisher "技術評論社")
    #:is-book? #t))
			      
@(define EmacsListPlay
   (make-bib
    #:title "リスト遊び：Emacsで学ぶLispの世界"
    #:author "山本和彦"
    #:date "2000"
    #:location (book-location #:publisher "株式会社アスキー")
    #:is-book? #t))

@(define FFPL
   (make-bib
    #:title "初めてのLisp関数型プログラミング---ラムダ計算からリファクタリングまで一気にわかる"
    #:author "五味 弘"
    #:date "2016"
    #:location (book-location #:publisher "技術評論社")
    #:is-book? #t))
			      

;; @;{ Alonzo Church history, Guy and Gene Scheme v. Lisp }
@(define CB-ROR
   (make-bib
    #:title "Realm of Racket: Learn to Program, One Game at a Time!"
    #:author (authors "Conrad Barski" "David Van Horn" "Mathias Felleisen")
    #:date "2013"
    #:location (book-location #:publisher "No Starch Press")
    #:is-book? #t))

;; @;{ core of liberal arts program, pencil and paper advice }
@(define HTPD
   (make-bib
    #:title "How to Design Programs: An Introduction to Programming and Computing"
    #:author (authors "Matthias Felleisen" "Robert Bruce Findler" "Matthew Flatt" "Shriram Krishnamurti")
    #:date "2001"
    #:location (book-location #:publisher "MIT")
    #:is-book? #t
    #:url "https://htdp.org/"))

 @(define KD-TSPL
   (make-bib
    #:title "The Scheme Programming Language: Fourth Edition"
    #:author "Kent Dybvig"
    #:location (book-location #:publisher "MIT")
    #:date "2009"
    #:is-book? #t
    #:url "https://www.scheme.com/tspl4/"))

@(define PG
   (make-bib
    #:title "プログラミングGauche"
    #:author (authors "河合史郎" "Kahuaプロジェクト")
    #:date "2008"
    #:is-book? #t
    #:location (book-location #:publisher "O'REILLY オライリー・ジャパン")))

@(define DNoble-PoP
  (make-bib
   #:title "Progress Without People: In Defense of Luddism"
   #:author "David Noble"
   #:date "1993"
   #:is-book? #t
   #:location (book-location #:publisher "Chicago: Charles H. Kerr Publishing Company")
   ))

@(define AFeenberg-JaRespMdrn
  (make-bib
   #:title "Nishida, Kawabata, and the Japanese Response to Modernity"
   #:author "Andrew Feenberg"
   #:is-book? #t
   #:date "2019"
   #:location (book-location #:publisher "Nagoya: Chisokudõ Publications")))

@(define PC-Rasta-Vegan
  (make-bib
   #:title "The Unsung Caribbean Roots of the Vegan Food Movemen"
   #:author "Paige Curtis"
   #:date "2021"
   #:is-book? #f
   #:url "https://www.yesmagazine.org/opinion/2021/07/21/vegan-history-caribbean-rastafarian"
   #:note "Dated JUL 21, 2021. Accessed 2021/10/13"))
@(define haru-on-Cochin
  (make-bib
   #:title "Nagoya Cochin (名古屋コーチン)"
   #:author "haru"
   #:url "https://favy-jp.com/topics/701"
   #:is-book? #f
   #:date "2019"
   #:note "Updated： September 24, 2019. Accessed 2010/10/13"))
@(define CC-FB-end-ka
  (make-bib
   #:title "The latest revelations mark the beginning of the end for the House of Zuckerberg"
   #:author "Carole Cadwalladr"
   #:date "2021"
   #:url "https://www.theguardian.com/commentisfree/2021/oct/10/latest-revelations-mark-the-beginning-of-the-end-for-the-house-of-zuckerberg"
   #:is-book? #f
   #:note "Dated Sun 10 Oct 2021 08.30 BST. Accessed 2010/10/13"))
   

