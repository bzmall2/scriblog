#+title: Sources in org file
#+startup: showall
#+options: toc:nil num:nil timestamp:nil ^:nil
#+html_head: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+html_head: <style> h1.title { display: none; }  </style>
#+html_head: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+html_head: <style> div#postamble { display: none; }  </style>
 
** Ideas of Good and Evil
   
 - W.B. Yeats


** Reading for Liberal Education
   
 - Louis G. Locke
 - William M. Gibson
 - George Arms

**    
