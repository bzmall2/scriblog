| pretentious | solid          | Tango Book |
|-------------+----------------+------------|
| accompany   | go with        |         88 |
| appeared    | looked, seemed |         20 |
| arrive      | come           |            |
| become      | get            |            |
| cause       | make           |         90 |
| cease       | stop           |        100 |
| complete    | finish         |         26 |
| continue    | keep on        |          6 |
| delve       | dig            |            |
| discover    | find           |         52 |
| indicate    | say            |        112 |
| individual  | person         |            |
| locate      | find           |         34 |
| place       | put            |            |
| possess     | have           |         54 |
| prepare     | get ready      |         96 |
| questioned  | asked          |         56 |
| receive     | get            |         50 |
| relate      | tell           |          6 |
| remain      | stay           |         36 |
| remove      | take off       |        104 |
| retire      | go to bed      |         16 |
| return      | go back        |            |
| secure      | get            |         58 |
| transform   | change         |        116 |
| verify      | check          |          8 |
