#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(require "bibliography/sources.scrbl")
@(define-cite ~cite citet generate-bibliography)
@(require "scripts/csv-tbl-to-scrbl-tablr.scrbl")
@(define-syntax-rule (slra-incl slfl) @; scrbl in racket-style include
  (include slfl))

@title{2021/10/31 philosophy cite tests}

Just a quick test of new sources before getting back to work on org->scrbl transformations and real writing.

I want make meme-style anntotated images with this quote from Ursula Le Guin. She says pretty much the same thing, just a little wider, than Edward Abbbey does in memes based on his words about the economy's ideology.

@nested[#:style 'inset]{
We have, essentially, chosen cancer as the model of our social system.
@~cite[(in-bib MB-TNR-ULK-FW ", xi")]}

Ursula Le Guin made that comment in the Foreward to a book of essay by Murray Bookchin.@~cite[MB-TNR] While typing in the bibliography details and the sentence I remembered Ursula Le Guin's review of Neil Gaiman's work onNorse Mythology.

@nested[#:style 'inset]{
The Norse myths were narrative expressions of a religion deeply strange to us. Judaism, Christianity and Islam are divine comedies: there may be punishment for the wicked, but the promise of salvation holds. What we have from the Norse is a fragment of a divine tragedy. Vague promises of a better world after the Fimbulwinter and the final apocalypse are unconvincing; that’s not where this story goes. It goes inexorably from nothingness into night. You just can’t make pals of these brutal giants and self-destructive gods. They are tragic to the bone.@~cite[ULK-NG-rvw]}

Her synopsis of the Viking world view reminds me of Bertrand Russel on the world view that science has given us.

@nested[#:style 'inset]{
 Such, in outline, but even more purposeless, more void of meaning, is the world which Science presents for our belief. Amid such a world, if anywhere, our ideals henceforward must find a home. That Man is the product of causes which had no prevision of the end they were achieving; that his origin, his growth, his hopes and fears, his loves and his beliefs, are but the outcome of accidental collocations of atoms; that no fire, no heroism, no intensity of thought and feeling, can preserve an individual life beyond the grave; that all the labours of the ages, all the devotion, all the inspiration, all the noonday brightness of human genius, are destined to extinction in the vast death of the solar system, and that the whole temple of Man's achievement must inevitably be buried beneath the debris of a universe in ruins--all these things, if not quite beyond dispute, are yet so nearly certain, that no philosophy which rejects them can hope to stand. Only within the scaffolding of these truths, only on the firm foundation of unyielding despair, can the soul's habitation henceforth be safely built. @~cite[BR-FMW-rjcom]}

I have a pile of books I'd like to get into @emph{Scribble}'s bibliography format. It would be nice to catch up on good articles about the Attention economy and recent revelaitons about the most discussed SNS platform. With more time it would be interesting to think about Simone Weil (advertising should be outlawed!) @~cite[SW-TNfR]and Hannah Arendt (on Kant's Taste, like Ricards's Sincerity from Confucius...)@~cite[HA-THC] too. But the discussion of Kant's taste was in @emph{The Life of the Mind} I think.


@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211031-010.scrbl}}}

@generate-bibliography[#:tag "20211031-010-bibliography"]
@generate-footnotes[]
@;index-section[#:tag "20211031-010-glossary"]
