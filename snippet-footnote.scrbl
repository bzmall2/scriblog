@(require scriblib/footnote)
@(define-footnote a-fn generate-footnotes)
@; name modeled on generate-bibliography from scribble/autobib
@a-fn{This is a footnote reference that will be moved to a footnote scection by @italic{generate-footnotes[]}. Regular pages get a footnote section at thebottome of the page, index-pages (with a toc?) get a separate footnote page that is generated with every iteration. The separate page generation clutters up the parent/main directory.}

@generate-footnotes[]
