#lang scribble/manual


@title{2021/09/08 First Post}

Writing posts with Scribble should help me get good at Racket. I keep having to re-learn how to use that at-exp notation, examples with evaluators, and literate programming techniques. Using Scribble regularly should make the techniques, or the details, feel less awkward.