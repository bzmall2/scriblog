#lang scribble/manual
@(require scriblib/footnote)
@(define-footnote fn mk-fn)

@title{2021/09/11 Scribble Footnotes}

I have been mining a previous attempt at using @italic{gitlab pages} with scribble. With @italic{Essay Racket} I was trying to do too much at once: it got overwhelming. @fn{@url{https://gitlab.com/bzmall2/essay-racket}}

Learning to use footnotes in Scribble, @italic{scriblib/footnote} should help me keep links out of the main body of my posts and pages. @note{In @italic{The Shallows} Nicolas Carr introduces research about how hyperlinks adversely effect reading comprehension.}

I can use @italic{scriblib/footnote}'s @italic{note} and still be considerate of readers. But the note doesn't show up at all when the browser (@italic{Firefox} or @italic{Chromium}) window is narrow. @margin-note{@italic{note} works for @italic{Latex}, producing a footnote, so it may serve a different purpose than @italic{margin-note}} But margin-note, that comes with scribble/manual, is always visible: no matter how narrow the screen is.

While exploring Scribble with @italic{Essay Racket} I figured out how to add css styles and de-colorize links:

@italic{scribble --quiet ++style quiet.css post.scrbl}

I'll have to think about making the footnote numbers black instead of blue. It just seems like a considerate thing to do.


@mk-fn[]
