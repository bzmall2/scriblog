
;;;;;;;;;;; emacs and shell conveniences
;; 
(fset 'other-bufferz-shell-run-last-command
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([24 19 24 111 134217790 134217840 return 24 111] 0 "%d")) arg)))
;; C-x-C-kO
;; Save-buffer, switch to other-buffer, go to end, Alt-p former command, RET for test and debugging scripts by running over and over again

;;
(fset 'scribble-to-shell-window-end-of-buffer-run-last-cmd
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([24 98 115 104 101 108 108 tab return 134217790 134217840 return] 0 "%d")) arg)))
(global-set-key [24 11 83] 'scribble-to-shell-window-end-of-buffer-run-last-cmd)
;; C-x-C-kS : See script run in Shell, other window

;;
(fset 'shell-in-other-window-repeat-command
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([24 19 24 53 111 134217790 134217840 return 24 53 111] 0 "%d")) arg)))
(global-set-key [24 11 87] 'shell-in-other-window-repeat-command)
;; C-xC-kW  other window has shell where last command will run again

;;;;;;;;;;; Scribble Commands
;;
(set-register ?i "@italic{")
(fset 'scribble-italic-insert
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("rii}" 0 "%d")) arg)))
(global-set-key [24 11 105] 'scribble-italic-insert)
;; C-x-C-ki

;; When you decide to italicize a word after it's been typed
(fset 'scribble-italicize-word
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("i\346} " 0 "%d")) arg)))
(global-set-key [24 11 119] 'scribble-italicize-word)
;; C-x-C-k-w

;; for block-quotes, I've been using C-x-C-ki italic inside nested inset too
(set-register ?n "@nested[#:style 'inset]{")
(set-register ?q "@nested[#:style 'inset]{")
(fset 'scribble-inserted-nested-inset-for-quote
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([24 114 105 113 return return 125 16] 0 "%d")) arg)))
(global-set-key [24 11 81] 'scribble-inserted-nested-inset-for-quote)
;; C-x-C-k-Q

;; Replace e-register withname of example evaluator on the page
(set-register ?e "@gregor-example[")
(fset 'scribble-example-code-insert
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([24 114 105 101 return return 93 16] 0 "%d")) arg)))
(global-set-key [24 11 69] 'scribble-example-code-insert)
;; C-x-C-k-E ; depends on example name in e register as in example above


