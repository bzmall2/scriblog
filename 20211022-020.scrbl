#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(require "bibliography/sources.scrbl")
@(define-cite ~cite citet generate-bibliography)
@(define-syntax-rule (slra-incl slfl) @; scrbl in racket-style include
  (include slfl))

@title{2021/10/22 test new template}

The new @italic{pre-post} template works! I'll just be content with getting this far along toward a reasonable @bold{D.R.Y.} appraoch for now.

@(slra-incl "quotes/IaR-SI-RespEngl-cnfsd-mrl-frstrn.rkt")

It's good to avoid dealing with "too many confusable problems at once." Why cause "intellectual and moral frustration" for yourself or anyone else?

While testing the quotes, in addition to reading about @italic{eval} and @italic{load} for the first time, I re-read pages about @italic{syntax}. I also re-discovered @italic{filebox} in the previous post as I got practice with @literal|{ @literal|{}|}| for in-line viewing of @italic{scribble code} and,

@verbatim|{ @verbatim|{}| }|

for block-level viewing of @literal|{@-exp}|ression code. Getting comfortable with the details of the @italic{notation} will make it easier to write naturally. 

@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211022-020.scrbl}}}

@generate-bibliography[#:tag "20211022-020-bibliography"]
@generate-footnotes[]
@;index-section[#:tag "20211022-020-glossary"]
