#lang scribble/lp2
@(require scribble/manual scriblib/footnote)
@(define-footnote a-fn generate-footnotes)

@title{2021/09/13 Literate Programming}


I mentioned @italic{Literate Programming} in an earlier post, @italic{Start Post script} @a-fn{See: @italic{Start Post Script}: @url{https://bzmall2.gitlab.io/scriblog/2021_09_11_Start_Post_script.html}} but never finished the script. A script to make scriblog posting easier is a good opportunity to learn @italic{Racket} and @italic{Literate Programming} with @italic{#lang scribble/lp2}.

@chunk[<args>
(require racket/list)
(define v-args (current-command-line-arguments))
(define l-args (vector->list v-args)) @;]
(define-values (num ttl)
  (values (first l-args)
          (string-join (rest l-args))))]
@; l-args] @;need closing square-bracket at end of last ling or get error

The function @italic{current-command-line-arguments} puts out a @italic{vector} of everything you time in after @italic{$ racket my-script}. It's easier to work with lists: @italic{vector->list} takes care of the ease.

So now I should be able to use the @italic{gregor} package code from the previous post.

@chunk[<today-string>
(require gregor racket/string)
(define (today-date-string sep)
  (define td (today))
  (string-join
   (map number->string
       (list (->year td)(->month td)(->day td)))
       sep))]

But now that I'm working on this script with @italic{scribble/lp2} I see that sometimes file-names should not include numbers for the date and another sequencing. 

@chunk[<greg-func>
<args>
<today-string>
(define (today-fname-ptitle num str)
  (define fname (string-append
                 (today-date-string "")
                 "-" num @; "-"
                 @;(string-replace str " " "-")
                 ".scrbl"))
  (define ptitle (string-append
                  (today-date-string "/")
                  " " str))
  (values fname ptitle)) @; ]
@;(today-fname-ptitle

(values num ttl)]  @;"020" "Next Post's Title")]
@; "2021913-020-Next-Post's-Title.scrbl" 
@; "2021/9/13 Next Post's Title"

So now that I have the @italic{file name}, "2021913-020.scrbl", and @italic{post title}, "2021/9/13 Next Post's Title", I can writing out a post template.

@chunk[<*>
@;  <args>
  <greg-func>]



@margin-note{This page's source: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20210913-010.scrbl}} @; put file-name between the slash/ and curly-braces }}
@; ; /fn}}

@generate-footnotes[]
