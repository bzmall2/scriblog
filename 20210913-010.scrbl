#lang scribble/manual

@(require scriblib/footnote)
@(define-footnote a-fn generate-footnotes)

@title{2021/09/13 Prep Post script}




With @italic{Literate Programming} techniques,@a-fn{See: @hyperlink["https://docs.racket-lang.org/scribble/lp.html"]{@italic{scribble/lp2}}} I tried to write a script that starts a @italic{blog post} from the @italic{command-line}. The details overwhelmed me. I moved that @italic{scribble/lp2} try into the @italic{bak} folder and just wrote the script directly, testing it in @italic{emacs} @italic{shell} as I went along. The simple, direct approach let me get something I can use.

Maybe as @italic{scribble/manual} and @italic{scribble/lp2} become second nature to me I'll become able to document and code at the same time. But now, the extra details interrupt my flow. With @italic{scribble/example} the close square-bracket, @italic{]} can be on its own line. But with @italic{scribble/lp2}'s @italic{chunck} I get an error about missing @italic{closing parenthesis}...

Anyway, the code @italic{prep-post.rkt} works well enough for now. If I do become able to use @italic{scribble/lp2} I'll make a another script, @italic{lp2s-post.rkt} (Literate Program 2 Script Post), that does not add a date and number to the post title as it generates a file name. A script should have an memorable, reasonable name. It's hard to remember the date a script was finished so there is no reason to have a date in the command.

This is the code I used to get this post started:

@racketblock{
#lang racket
(require gregor)

(define v-args (current-command-line-arguments))
(define l-args (vector->list v-args)) 
(define-values (num ttl)
  (values (first l-args)
          (string-join (rest l-args))))

(define (pad-single-digit num)
  (define str (number->string num))
  (if (< 1 (string-length str)) str
      (string-append "0" str)))

(define (today-date-string sep)
  (define td (today))
  (string-join
   (map pad-single-digit
       (list (->year td)(->month td)(->day td)))
   sep))

(define (today-fname-ptitle num str)
  (define fname (string-append
                 (today-date-string "")
                 "-" num
                 ".scrbl"))
  (define ptitle (string-append
                  (today-date-string "/")
                  " " str))
  (values fname ptitle))
;; (today-fname-ptitle num ttl) ; ok!

(define-values (fn pt)
  (today-fname-ptitle num ttl))
(define lang-line "#lang scribble/manual")

(display-lines-to-file
 (list lang-line ""
       (string-append "@title{" pt "}") "")
 fn
 #:exists 'error)}

Later I should add a noted at the end that links to the generated page's source. 

The script: @hyperlink["https://gitlab.com/bzmall2/scriblog/-/blob/main/prep-post.rkt"]{prep-post.rkt}


@margin-note{This page's source: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20210913-010.scrbl}}

@generate-footnotes[]
