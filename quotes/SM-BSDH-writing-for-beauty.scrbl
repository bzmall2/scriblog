#lang scribble/manual
@(require racket/include scriblib/autobib)
@(include "../bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@;(define SM-BSDH-bunko-beauty-write
@nested[#:style 'inset]{
書いて美しくなれるなら、 女性ライターは全員美女のはずだが、 むろんそんなことはない。 書くのはむしろ美容の敵だ。 目は血走る、 肌はボロボロ、 髪はポサポサ、 運動不足で足腰はガタガタ、 ろくなことはない。...@~cite[(in-bib SMinako-BSDH-bunko ",156")]}
@;)
