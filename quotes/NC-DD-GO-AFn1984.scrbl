
(define NC-DD-GO-AFn1984
(nested #:style 'inset
"Fame, fortune, and respect await those who reveal the crimes of official enemies; those who undertake the vastly more important task of raising a mirror to their own societies can expect quite different treatment. George Orwell is famour for \"Animal Farm\" and \"1984\", which focus on the official enemy. Had he addressed the more interesting and significant question of thought control in relatively free and democratic societies, it would not have been appreciated, and instead of wide acclaim, he would have faced silent dismissal or obloquy. Let us nevertheless turn to the more important and unacceptable questions."
(~cite(in-bib NC-DDemocracy ", 372"))))
