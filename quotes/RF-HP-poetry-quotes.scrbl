
“The programmer, like the poet, works only slightly removed from pure thought-stuff.”
—Fredrick Brooks


“The poet's eye, in a fine frenzy rolling, / Doth glance from heaven to earth, from earth to heaven; / And, as imagination bodies forth / The forms of things unknown, the poet's pen / Turns them to shapes, and gives to airy nothing / A local habitation and a name.”
—William Shakespeare

@~cite[RF-HP]

2021年  9月 29日 水曜日 09:29:56 JST
https://users.cs.northwestern.edu/~robby/

