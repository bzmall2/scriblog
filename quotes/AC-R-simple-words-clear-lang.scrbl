  "The mutual understanding and communication discovered by rebellion can survive only in the free exchange of conversation. Every ambiguity, every misunderstanding, leads to death; clear language and simple words are the only salvation from this death."
@~cite[(in-bib AC-R-V ", 283")

"反抗によって発見された総合理解とコミュニケーションは、自由な会話のなかでなくては永続しえない。あいまいと誤解は死を招く。明白な言語、単純なことばのみが、この死を救うことができる。"
@~cite[(in-bib AC-R-Ja ", 259")]

  "It is worth noting that the language peculiar to totalitarian doctrines is always a scholastic or administrative language."
@~cite[(in-bib AC-R-V ", 283")  
  
  "全体主義の教義に固有のことばが、いつもスコラ的か官庁的ことがであることに注意すべきであろう。"
@~cite[(in-bib AC-R-Ja ", 259")]  
