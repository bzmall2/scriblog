#### Albert Camus on Plain Style in *The Rebel* 反抗者の文体

>  The mutual understanding and communication discovered by rebellion can survive only in the free exchange of conversation. Every ambiguity, every misunderstanding, leads to death; clear language and simple words are the only salvation from this death.[fn1]

>  [fn1] It is worth noting that the language peculiar to totalitarian doctrines is always a scholastic or administrative language.

 --- Albert Camus *The Rebel* (p. 283)

> 反抗によって発見された総合理解とコミュニケーションは、自由な会話のなかでなくては永続しえない。あいまいと誤解は死を招く。明白な言語、単純なことばのみが、この死を救うことができる。[fn2]
>   (p. 259)
> [fn2] 全体主義の教義に固有のことばが、いつもスコラ的か官庁的ことがであることに注意すべきであろう。 (p.278)

 --- 反抗的人間 カミュ全集 6 訳：佐藤朔 白井浩司 新潮社版 (p. 259)

#AlbertCamus #TheRebel #PlainStyle #writing #Japanese #Translation #和訳
   
 - Christopher Lasch's Plain Style
https://ruhrspora.de/posts/1961906x

