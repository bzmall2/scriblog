
@nested[#:style 'inset]{
George Orwell still considered himself a socialist when he wrote @emph{1984}@bold{,} but @emph{Life} and @emph{Time} hailed the novel as a condemnatin of socialism.@~cite[(in-bib CLasch-Plain ", 56")]}

@nested[#:style 'inset]{
George Orwell considered himself a socialist; the American press made him a hero of the free world in its struggle agains socialism. @~cite[(in-bib CLasch-Plain ", 57")]}



