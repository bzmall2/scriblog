
### Meaningful Work: Paul Goodman, 片桐ユズル訳, Noam Chomsky and Adam Smith

> In brief, it takes *effort* to make a middle class obsessional, and it takes *effort* to make a poor boy stupid.  &#x2014; Paul Goodman in *Growing Up Absurd* (1956 ~ 1960) p. 161

> 一言でいえば、中流階級に脅迫観念をとりつかせるには *ほねがおれるし*, 貧しい少年をまねけにするには *ほねがおれる* 。
>    &#x2014; 片桐ユズル(訳) *不条理に育つ、 管理社会の青年たち* (1971) p. 221

> It is inevitable that in a *closed* status structure middle class values will become disesteemed, for such values are rewarded by upward "betterment." And more philosophically, all value requires and *open* system allowing for surprise, novelty, and growth. A closed system cannot make itself valuable, it must become routine, and devoted merely to self-perpetuation. (When a mandarin bureaucracy is valuable it is because of the vastness of the underlying population and the absence of communication: each mandarin individually embodies the emperor.) 
> 
> So the rat race is run desperately by bright fellows who do no believe in it because they are afraid to stop. &#x2014; Paul Goodman

> 必然的に、/閉じられた/ 地位構造においては、中流階級的価値は軽視されることになる、というのはそのような価値はうわむきの「向上」によってむくわれるからだ。もっと哲学的いえば、すべての価値は、おどろき、新奇, 成長をうけいれるような *開かれた* 体系を必要とする。閉じられた体系はそれ自体に価値をあたえることができない、それは日常的手続にならざるを得ず、たんに自分自身を忘れさせないための献身であるにすぎない。(中国の官僚主義が貴重なのは、したにある人口が莫大であり、コミュニケーションがなかった。つまり官使ひとりひとりが皇帝を具現したものであった。) 
> 
> というわけでラット・レースは、それを信じない頭のいい奴らによってづというのは、彼らは立ちどまるのがこわいからだ。

\#Japanese #translation #PaulGoodman #KatagiriYuzuru #education #value #bureaucracy #mandarins #NoamChomsky #AdamSmith

> Everybody reads the first paragraph of The Wealth of Nations where he talks about how wonderful the division of labor is. But not many people get to the point hundreds of pages later, where he says that division of labor will destroy human beings and turn people into creatures as stupid and ignorant as it is possible for a human being to be. And therefore in any civilized society the government is going to have to take some measures to prevent division of labor from proceeding to its limits. &#x2014; Noam Chomsky on Adam Smith

> &#x2026; The founders of classical liberalism, people like Adam Smith and Wilhelm von Humboldt, who is one of the great exponents of classical liberalism, and who inspired John Stuart Mill — they were what we would call libertarian socialists, at least that ïs the way I read them. For example, Humboldt, like Smith, says, Consider a craftsman who builds some beautiful thing. Humboldt says if he does it under external coercion, like pay, for wages, we may admire what he does but we despise what he is. On the other hand, if he does it out of his own free, creative expression of himself, under free will, not under external coercion of wage labor, then we also admire what he is because he’s a human being. He said any decent socioeconomic system will be based on the assumption that people have the freedom to inquire and create — since that’s the fundamental nature of humans — in free association with others, but certainly not under the kinds of external constraints that came to be called capitalism.
> 
> It’s the same when you read Jefferson. He lived a half century later, so he saw state capitalism developing, and he despised it, of course. He said it’s going to lead to a form of absolutism worse than the one we defended ourselves against. In fact, if you run through this whole period you see a very clear, sharp critique of what we would later call capitalism and certainly of the twentieth century version of it, which is designed to destroy individual, even entrepreneurial capitalism.
> 
> There’s a side current here which is rarely looked at but which is also quite fascinating. That’s the working class literature of the nineteenth century. They didn’t read Adam Smith and Wilhelm von Humboldt, but they’re saying the same things. Read journals put out by the people called the “factory girls of Lowell,” young women in the factories, mechanics, and other working people who were running their own newspapers. It’s the same kind of critique. There was a real battle fought by working people in England and the U.S. to defend themselves against what they called the degradation and oppression and violence of the industrial capitalist system, which was not only dehumanizing them but was even radically reducing their intellectual level. So, you go back to the mid-nineteenth century and these so-called “factory girls,” young girls working in the Lowell [Massachusetts] mills, were reading serious contemporary literature. They recognized that the point of the system was to turn them into tools who would be manipulated, degraded, kicked around, and so on. And they fought against it bitterly for a long period. That’s the history of the rise of capitalism. &#x2014; Noam Chomsky on Meaningful Work

-   <https://chomsky.info/warfare02/>
