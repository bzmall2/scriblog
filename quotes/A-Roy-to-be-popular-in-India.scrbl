@nested[#:style 'inset]{
But if Roy insists on staying on in India, there are a few things she could do to soften the hatred she often inspires in some Indians. Wear saris, shut up, stay at home, have babies, grow her hair long and start plaiting it.@~cite[Ar-LnH]}
