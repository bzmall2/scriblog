@nested[#:style 'inset]{
Philosopy is, or should be, a single-minded pursuit of truth...  The man who takes the road of reason honestly does not know where he is going to come out...

... A belief that is true but unreasoned is at the mercy of the sophistries of the day. A belief that is false but also reflective carries with ti the means of its own amendment... philosophy is skeptical; reason builds by first destroying... But it destroys in order to rebuild, and perhaps it is better to live in a modest house of one's own, build by authentic insights and the sweat of one's brow, than in something far more imposing that one has inherited without question from authority. @~cite[(in-bib EduAsPhilo ", 596")]
}
