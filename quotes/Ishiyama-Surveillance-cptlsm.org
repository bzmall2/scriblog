
石山宏一

surveillance capitalism　監視資本主義

＜第175回＞　Updated　February 3, 2020　更新日：2020年2月3日

surveillance capitalism （発音は「サーベイランス・キャピタリズム」）とは surveillance（監視）と capitalism（資本主義）の合成語で「現在進行中で、あらゆる産業であらゆる企業が消費者を日常的に監視し、その情報を収集・分析し、それを収益に結びつけている新しい形の資本主義」を指す経済分野の新語（注1）。この新語は2014年頃に Harvard Business School のショシャナ・ズボフ（Shoshana Zuboff）名誉教授が造語したとされ、昨年（2019年）1月に発刊した同名を入れた著書「The Age of Surveillance Capitalism」（「監視資本主義の時代」）でその危険性を指摘して次第に米国で有名になったが、最初は世界中には広まらなかった。しかし、最近数か月になってこの surveillance capitalism に関して、産業経済の platformer（プラットフォーマー＝基盤）と言われる GAFA（Google、Amazon、Facebook、Appleの頭文字を合わせたもの）がその個人情報を売っていることが大問題となってから俄然爆発的に人気を集め、欧州やアジア諸国で大きな話題となったため、毎日新聞等の日本や欧米諸国の主要マスコミも大きく報道し、世界的に広まった（注2）。

同教授の理論は非常に論理的で説得力がある。彼女は2016年に登場した「ポケモンGO」を一例に出してこう論じている。つまり、同ゲームはバーチャル（仮想現実）なポケモンが現実世界にリンクして出現するゲームであるが、世界の人々はこのアプリをAR（augmented reality＝拡張現実）への“ほぼ無害な入口”（mostly harmless foray）」と捉えたが、教授は実はユーザーがこれからどこに行くのか、その途中で何を目にするのか等の一見すると何のこともない行動の予測情報が蓄積されビジネス取引の材料となりうると論じる（注3）。

つまり、教授の主張は膨大なデータ抽出からの予測という手法でビジネスモデルを作った巨大ネット企業がこうした人間の行動を基に未来行動を予測し、それを収益の柱にしており、「これは surveillance capitalism だ」と説くのである（注4）。これは納得いく理論である。筆者もネット通販しているが、時々以前に買った靴等の広告が時々ネットのメール上に出てくるので「監視されている」と思える近頃である。これは本当に怖いし、読者も大いに注意すべきであろう。

surveillance capitalism の邦訳語であるが、いろいろ調べた結果、直訳の「監視資本主義」を適訳語とした（注5）。カタカナ語訳の「サーベイランス・キャピタリズム」ではパンチ力に欠けるであろう。

［訳例］としては毎日新聞（オンライン版）がコラム「余録：タクシーに乗り込むと・・・」と題した記事で「．．．『監視資本主義』。ハーバード大のズボフ名誉教授が名付けた新たな資本主義である...」と「監視資本主義」を訳語として利用（注6）。

［用例1］としては Wired.co.uk（online）が “Pokemon Go was a warning about the rise of surveillance capitalism”と題した記事で、“…In Shoshana Zuboff’s new book, The Age of Surveillance Capitalism, the author holds the augmented reality game up as just one example of a new form of capitalism…”として “surveillance capitalism” を使っていた（注7）。

［用例2］としては Wikipedia（online）が “Surveillance capitalism” と題した記事で、“Surveillance capitalism has a number of meanings around the commodification of personal information. Since 2014, social psychologist Shoshana Zuboff has used and popularized the term…” として “surveillance capitalism” を使用（注8）。
注1） 	上記［訳例］［用例1］［用例2」（ここでは引用されていない）参照。そして毎日新聞（オンライン版）2019年4月21日、「余録：タクシーに乗り込むと・・・」(https://mainichi.jp/articles/20190421/ddm/001/070/164000c) 参照。またWikipedia, “Surveillance capitalism” (https://en.wikipedia.org/wiki/Surveillance_capitalism) 参照。
注2） 	同上。
注3） 	Wired. co.uk (online), “Pokemon Go was a warning about the rise of surveillance capitalism”(https://www.wired.co.uk/article/the-age-of-surveillance-capitalism-facebook-shoshana-zuboff) (Seen on Jan. 27, 2020).
注4） 	同上。
注5） 	最新オンラインン辞典の「英辞郎」（アルク）と Weblio には surveillance capitalism の項目そのものは無かった（2020年1月27日時点）。市販の日本の紙版の英和辞典には勿論、同項目は無い。
注6） 	上記毎日新聞記事参照。
注7） 	Wired. co.uk (online), “Pokemon Go was a warning about the rise of surveillance capitalism” (https://www.wired.co.uk/article/the-age-of-surveillance-capitalism-facebook-shoshana-zuboff) (Seen on Jan. 27, 2020).
注8） 	Wikipedia (online), “Surveillance capitalism” (https://en.wikipedia.org/wiki/Surveillance_capitalism) (Seen on Jan. 27, 2020).


 - https://l-world.shogakukan.co.jp/colum/watching175.html
   2021年 10月 13日 水曜日 08:28:49 JST
