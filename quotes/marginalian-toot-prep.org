

** On Writing
> Put in the most simplistic terms during her acceptance speech for Peace Prize of the German Book Trade, ‘A writer, I think, is someone who pays attention to the world.’ She urged writers to ‘be serious,’ without forgetting the importance of humour in both life and literature.

> 3. ‘Love words, agonize over sentences, and pay attention to the world.’
Fiction. Narrative. Poetry. Non-fiction. Reviews. Every genre is a commentary on life. If you don’t pay attention to the world, you won’t have much to say about it. Susan Sontag contextualized her personal view of the world within a framework of the Vietnam War and her voluntary participation in the Siege of Sarajevo. She stressed the political role and social duty of the artist and the ways in which language can both create and distort reality.

  - https://www.writerswrite.co.za/7-pearls-of-writing-wisdom-from-susan-sontag/   

** Einstein and Eddington
> The scientific triumph was also a heartening, humane moment — just after the close of World War I, a pacifist English Quaker, who had refused to be drafted in the war at the risk of being jailed for treason, and a pacifist German Jew united humanity under the same sky, under the deepest truths of the universe. It was an invitation to perspective in the largest sense — one to which the third annual Universe in Verse was dedicated.
 - https://www.themarginalian.org/2019/05/29/eddington-einstein-janna-levin/
 #MariaPopova #Einstein #Eddington   

** Ella Wheeler Wilcox
> The lawlessness of wealth-protecting laws
That let the children and childbearers toil
To purchase ease for idle millionaires.
 - https://www.themarginalian.org/2017/01/31/protest-poem-ella-wheeler-wilcox-amanda-palmer/
  #themarginalian #EllaWheelerWilcox #MariaPopova
     
** metaphors 
>... from the moment we begin trying to make sense of the world, and even as we face the terrifying prospect of its meaninglessness, the familiar becomes our foothold for the unfamiliar; the images that already carry meaning, already invoke felt feeling-tones, become mirrors and magnifying glasses for those that don’t yet. Our entire experience of reality, bent through the lens of our meaning-hungry consciousness, becomes, as Nietzsche memorably put it, “a movable host of metaphors, metonymies, and anthropomorphisms.” 
 - https://www.themarginalian.org/2021/07/07/jane-hirshfield-metaphor/
  #MariaPopova #metaphor
  
** Alan Watts
> “If the universe is meaningless, so is the statement that it is so… The meaning and purpose of dancing is the dance.”
 - https://www.themarginalian.org/2016/11/01/alan-watts-wisdom-of-insecurity-3/
 #MariaPopova #AlanWatts
 
** Poetry Jan Hirshfield
> Poetry, indeed, has always been one of humanity’s sharpest tools for puncturing the shrink-wrap of silence and oppression, and although it may appear to be galaxies apart from science, these two channels of truth have something essential in common: nature, the raw material for both. To impoverish the world of the birds and the bees is to impoverish it of the bards and the biologists. 
 - https://www.themarginalian.org/2017/04/18/jane-hirshfield-on-the-fifth-day/
  #MariaPopova


** Joan Didion
> “Character — the willingness to accept responsibility for one’s own life — is the source from which self-respect springs.”
  - https://www.themarginalian.org/2012/05/21/joan-didion-on-self-respect/

    
** Susan Sontag, Paul Graham 

> To have that sense of one’s intrinsic worth which constitutes self-respect is potentially to have everything: the ability to discriminate, to love and to remain indifferent. To lack it is to be locked within oneself, paradoxically incapable of either love or indifference.
  - https://www.themarginalian.org/2012/12/05/susan-sontag-on-courage-and-resistance/
   - https://www.themarginalian.org/2012/02/27/purpose-work-love/#graham

** Sontag on fear of aging

> “The fear of becoming old is born of the recognition that one is not living now the life that one wishes. It is equivalent to a sense of abusing the present.”

 - https://www.goodreads.com/quotes/2095028-the-fear-of-becoming-old-is-born-of-the-recognition

   
** Sontag on Camp, Fashion

   > Camp was not gender or sexuality specific, Sontag argued, but the aesthetic had been embraced by the LGBTQ community as a way to “neutralize moral indignation” by promoting a playful approach to that which others took seriously.
 - https://time.com/5584111/met-gala-2019-camp-history/
 #Camp #SusanSontag

The essay launched Sontag’s career as a literary critic, in which “she argued for a more sensuous, less intellectual approach to art,” TIME noted in her obituary, when she died in 2004 at the age of 71. “It was an irony lost on no one, except perhaps her, that she made those arguments in paragraphs that were marvels of strenuous intellection.”    
