#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title{2021/10/07 No! for unpopular writers}
 
Reading classic essay collections is fun. In @italic{Reading for Rhetoric}@~cite[RoRhetoric] Leslie Fiedler's @italic{No, In Thunder!} rings true and has me thinking of Arundhati Roy.

@nested[#:style 'inset]{
... What people, what party, what church, needs an enemy when it has a great writer in its ranks!

Unless he bite the hand that feeds him the writer cannot live; and this those who would prefer him dead (so they can erect statues of him) can never understand.@~cite[(in-bib LF-NoIT ", 279")]}

Fiedler's analysis from 1962, that people want to kill the great writers and then perhaps erect statues of them, sounds a lot like Saba Nagvi's analysis from 2006:

@nested[#:style 'inset]{
 As far as they are concerned, Roy should be the first citizen in their rogue’s gallery of ‘anti-national’ elements. No other writer inspires as much anger and mountains of hate mail to publications where she writes as this ‘petite woman’.@~cite[AR-LnH]}

It's funny to read the "great writer" referred as "he" and how people want "him" dead in Fiedler's essay. @a-fn{Fiedler mentions Mary McCarthy in passing. Did she translate Hannah Arendt in addition to writing novels? Another mention goes to Djuna Barnes with her @italic{Nightwood} so, among all the men and their novels there is a little touch of gender diversity in his essay.} Nowadays the great writer seems most likely to match Fiedler's description is Arundhati Roy, a woman in India.

@nested[#:style 'inset]{
Most middle-class Indians hate Arundhati Roy---or, rather, they hate the political activist she has apparently become. She was palatable as a novelist...@~cite[AR-air]}

Asokan's appraisal of public opinion suggests Arundhati Roy worked as a serious, probably great, artist over the eleven years since Nagvi's appraisal.  Another analysis by Somak Ghoshal add support to the thought that Arundhati Roy is the sort or serious, probably great, artist about whom Leslie Fiedler writes.

@nested[#:style 'inset]{
There are several reasons why Roy is such an easy target of hate but the most obvious of these is perhaps to do with the way she frustrates any label.@~cite[AR-EasyTarget]}

Somak Ghoshal's appraisal of public opinion's response in India brings me back to Leslie Fiedler.

@nested[#:style 'inset]{
There is some evidence that the Hard No is being spoken when the writer seems a traitor (Graham Greene has amd the point convincingly) to those whom he loves and who have conditioned his very way of responding to the world. @italic{When the writer says of precisely the cause that is dearest to him what is always and everywhere the truth about all causes; that it has been imperfectly concieved, inadequately represented, and that it is bound to be betrayed, consciously or unconsciously, by its leading spokesman---we know that he is approaching art of real seriousness if not actual greatness}.@~cite[(in-bib LF-NoIT ", 279")]}

Arundhati Roy seems to have spoken the  "Hard No!" with serious non-fiction. Her essays are art as well as her fiction.

@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211007-040.scrbl}}}

@generate-bibliography[#:tag "20211007-040-bibliography"]
@generate-footnotes[]
@index-section[#:tag "20211007-040-glossary"]
