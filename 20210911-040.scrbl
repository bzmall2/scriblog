#lang scribble/manual
@(require scriblib/autobib)
@(define-cite ~cite citet generate-bibliography)
@(require racket/include)
@(include "bibliography/sources.rkt")

@(require scriblib/footnote)
@(define-footnote fn mk-fn)


@title{2021/09/11 Scribble Bibliography}

This page is just a quick test (with good substance for the practice) of @italic{scriblib/autoib}'s @italic{citet} and @italic{in-bib}. Once again, previous attempts with @italic{Essay Racket}@fn{See: @hyperlink["https://gitlab.com/bzmall2/essay-racket/-/blob/master/public/plot-7-virus-R0s/Seven-Virus-R0-view.scrbl"]{Essay Racket page using @italic{autobib} and @italic{citet}}} are helping me move along a lot faster during this attempt.

@;not sure how to do a block-quote maybe `nested`?
@; ; figure out how to add a quote-style? just italicize?

@section{Wendell Berry: A Law for Decent Use}

Wendell Berry wrote an essay @italic{Why I am not going to buy a computer}, but I feel that his observations on land use and scale can show us parallels with computer use and programming, free software and small groups.

@; Simone Weil's Jesus: When two or three are gathered in my name, I am there.
@; Teruo Kawamoto: Three people get together and Monjyu-no-chie, A Bodhisvatta's wisdom

@nested[#:style 'inset]{
@italic{"If we wish to make the best use of people, places, and things then we are going to have to deal with a law that read about like this: as teh quailty of use increases, the scale of use (that is, the size of operations) will decline, the tools will become simpler, and the methods and the skills will become more complex. That is a difficult law for us to believe, because we have assumed otherwise for a long time, and yet our experience  overwhelmingly suggest that is @italic{is} a law, and that the penalties for disobeying it are severe."@~cite[(in-bib WendBerryWhatFor ", 113-114")]}
}


@section{Howard Wainer quote}


@nested[#:style 'inset]{
@italic{"Often we look most carefully at what is on top and less carefully further down."}
@~cite[(in-bib WainerOrderAdvice ", p. 19")]
}

@generate-bibliography[#:tag "20210911-040-bibliography"]

@mk-fn[]

