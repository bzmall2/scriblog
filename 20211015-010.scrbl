#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title{2021/10/15 Roy Lappe Robinson}

Arundhati Roy on how we can try to make sense of our world.
@nested[#:style 'inset]{
I wonder sometimes, in this age of so much, you know, @italic{WhatsApp}  and video and @italic{Netflix}  and movies, and living in a country where so many people are either semiliterate or illiterate, why is it that a certain kind of writing, like, say, my essays, and even my... [novels] now, you know they're translated ... spontaneously into so many languages, into 51 languages in the case of The M@italic{inistry} , many Indian languages. You know, when I go to speak in places, like anywhere in India, literally thousands show up. Why? Not because I’m some superstar, but because everybody is looking to understand what is happening at this moment, when really the era that we think we know and understand is coming to an end. And this is the simplest way of saying a complicated thing, you know? Literature is.
@~cite[AR-DN-LitPower]}

Mabye @emph{FoodFirst}'s @italic{Backgrounder}s are a form of literature that can help us "understand what is happening" with clear views of the @italic{myths} that put us in "blind alleys." Talk about "organizing beliefs to help us interpret the endlessly confusing rush of world events" sounds likes something I read in a collection of classic @italic{Liberal Arts} essays.@~cite[TLE4th]
@nested[#:style 'inset]{
In troubled times, all of us seek ways to make sense of the world. We grasp for organizing beliefs to help us interpret the endlessly confusing rush of world events. Unfortunately, however, the two of us have come to see that the way people think about hunger is the greatest obstacle to ending it. So in this @italic{Backgrounder} we encapsulate 40 years of learning and in-depth new research to reframe ten such ways of thinking explored in our latest book @emph{World Hunger: 10 Myths}. We call them “myths” because they often lead us down blind alleys — or simply aren’t true.
@~cite[FF-10Myths]}

Also in the @italic{Power of Fiction} interview with @emph{Democracy Now!}, Arundhati Roy mentions the importance of generalists.@a-fn{Neil Postman in @emph{Technopoly} or @emph{The End of Education} mentions Lewis Mumford as one of the "great noticers" and Lewis Mumford was described either by himself of by Postman as a "generalist." Who was it that said the "educated" or "cultivated" or "civilized" person could do the work of any person?5o}


@nested[#:style 'inset]{
... the radical understanding now has to come from not thinking of climate—”Oh, I’m a specialist in climate change,” “I’m a specialist in river valleys,” “I’m a specialist on Kashmir,” “I’m a specialist”—you know, this kind of compartmentalization is actually reducing the real problem that we have, because now you have to understand there’s a connection between caste and climate change and capitalism and nationalism and internationalism. And I think this is where literature and a way of grappling with history as a kind of supple narrative is important.
@~cite[AR-DN-LitPower]}

I.A. Richards writes of the importance of a "Synoptic View" and refers back to Plato. There are sentences in @emph{General Education in a Free Society} that sound like I.A. Richards, but before searching the book for the sentences the leave that impression, it would be interesting to compare the chapter @italic{General and Special Education} with Arundhati Roys concerns with becoming a @italic{One Trick Pony}.

It's interesting to see that Yanis Varoufakis who works as both an academic and a politician, chose to write a novel.

@nested[#:style 'inset]{
...  we called upon people of the Earth to collaborate in the context of a Progressive International. ... During Black Friday, last December, we organized a unique industrial action. We called it #MakeAmazonPay, in support of workers that are exploited, both in terms of low pay but also in terms of the automation of their bodies through the machinery that Amazon uses in its warehouses — [inaudible] humans, without humanizing the robots. So, we had this rolling strike in Amazon warehouses that was called and organized by the Progressive International. It started in Bangladesh. On the same day, it rolled into India. Then it went to Germany. Then it went to New Jersey. Then it went to Seattle.
@~cite[YV-DN-ANow]}

Yanis Varoufakis chose science fiction as the genre in which to think how @italic{Another World is Possible}.@~cite[SG-AWP] Susan George's work provided inspirations and grounds for science fiction writer Richard K. Morgan's @emph{Market Forces} book where he fantasizes that people  upper management physcially kill one another.  So it is fitting that the world-wide rolling strike called by @italic{Progressive International} is making me think of @emph{Ministry for the Future}.@~cite[KSR-MftF], another "Alternative Now" that mentions various "Plan B" options.

@emph{Ministry for the Future} shows some of the policies and actions that make sense when we pay attention to the sort of things we see in Arundhati Roy's writings.

@nested[#:style 'inset]{
... as we have read now, you’re really rushing towards extinction. Just yesterday, there was a report about more than a million species... going extinct, that just over the last 30 years you’ve had such an accelerated form of mammals disappearing. The understanding of—like I call it the understanding, you know, the connection between insects, mammals, the acidity in the ocean, corals, fish, water in rivers, forests, rain. You know, you can develop artificial intelligence, but you can’t understand these basic things, which are just—you can’t understand the connection between the planet you live on and your place in the web of life.
@~cite[AR-DN-extnct]}



Kim Stanley Robinson depicts an international carbon sequestration program that meshes well with the @emph{FoodFirst} @italic{Backgrounder}, and provides a catharsis while remembering the epidemic of farmer suicides in India, and also the unforgettable tragedy of Surekha Bhotmange. Arundhati Roy's writings provide a course in the humanities. Her writings push us "Toward Liberal Education" and help us develop a "Synoptic View". They also serve in "the struggle of the people against power" which is "the struggle of memory against forgetting." Just as the writings of Ishimure Michiko serve people and memory by making unforgettable the victimes of corporate mercury poisonings (and inspired Eugene Smith's greatest work), Arundhati Roy is against power and forgetting by making unforgettable the massacre of Muslims in Gujarat and lynching Surekha Bhotmange's family in Maharashtra.@~cite[AR-SB-Prospect] Imagine if instead of poisoning, murdering, and otherwise abusing the primary peoples, like the fisher people of Minamata and farmer peoples of India, they were paid to sequester carbon while doing the work (feeding everyone) that they want to do. Arundhati Roy's essays show us why we need to imagine a better world, and Kim Stanley Robinson's novel points us toward specific policies.

@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211015-010.scrbl}}}

@generate-bibliography[#:tag "20211015-010-bibliography"]
@generate-footnotes[]
@;index-section[#:tag "20211015-010-glossary"]
