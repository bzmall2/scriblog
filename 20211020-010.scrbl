#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title{2021/10/20 Russel on Orwell}

These sentences about George Orwell may have been written before Noam Chomsky learned of the unpublished introduction to @emph{Animal Farm}. @a-fn{For other mentions of @emph{Animal Farm}'s unpublished introduction see a previous post: @seclink["2021_10_12_Lasch_on_Orwell"]}
@; @seclink to 
@(include "quotes/NC-DD-GO-AFn1984.scrbl")
@NC-DD-GO-AFn1984

In a later work Noam Chomsky mentions @emph{Animal Farm}'s unpublished introduction and takes us to Bertrand Russel.

@(include "quotes/NC-PnP-WaIR-GO-AFintro.scrbl")
@NC-PnP-WaIR-GO-AFintro

And here is Bertrand Russel writing about George Orwell's @emph{1984} in a way that "holds a mirror up" to his own society and leads to "scandalous abuse" for "integrity and honesty".

@(tabular #:sep @hspace[1] #:row-properties '(left left right)
(list
  (list
  "George Orwell's 1984 is a gruesome book which duly made its readers shudder. It did not, however, have the effect which no doubt its author intended. People remarked that Orwell was very ill when he wrote it, and in fact died soon afterward. They rather enjoyed the frisson that its horrors gave them and thought: \"Oh well, of course it will never be as bad as that except in Russia! Obviously the author enjoys gloom; and so do we, as long as we don't take it seriously.\" Having soothed themselves with these comfortable falsehoods, people proceeded on their way to make Orwell's prognostications come true. Bit by bit, and step by step, the world has been marching toward the realization of Orwell's nightmares; but because the march has been gradual, people have not realized how far it has taken them on this fatal road. "
  "オーウェル(注：George Orwell, 1903-1950：英国の作家で，全体主義的・管理社会的ディストピア＝反ユートピアの世界を描いた『1984年』で有名／ラッセルのこのエッセイはオーウェルの死後４年目に書かれたことになる。)の『1984年』は，まさに読者を戦慄させた，身の毛のよだつような本である。けれども、この本は，疑いもなく、著者が意図した効果（影響）をもたなかった。（注：オーウェルが自ら語っているように，彼は民主社会主義者であり、彼はこの小説において社会主義そのものを糾弾しているのではなく、当時のソ連に象徴される全体主義的な社会や国家を批判していることに注意）人々はオーウェルがこれを書いていた時には彼は重病だったと言ったが，事実，彼はその後まもなく亡くなった（注：小説『1984年』は1949年6月8日に出版され，1950年1月21日に死亡した）。読者（彼ら）は，むしろ，その小説が与える恐怖による身震いを楽しみ，次のように考えた。「いや,もちろん，ロシアを除いてけっしてそんなにひどくはならないだろう。オーウェル（著者）が陰気臭さを楽しんでいたのはあきらかだ。真面目にとらずに我々読者も楽しむことにしよう。」　こういった心地よい欺瞞（虚偽）で自らを慰め，オーウェルの予言が現実のものとなる道を歩んできたのである。少しずつ，一歩一歩，世界はオーウェルの悪夢の実現に向けて進んできている。しかし，この歩みは少しずつであったため，どのくらいこの致命的な道をどんなにか遠くまで歩んできたか，人々は気が付いてこなかった（気がついていない）のである。 ")
  (list
  "Only those who remember the world before 1914 can adequately realize how much has already been lost. In that happy age, one could travel without a passport, everywhere except in Russia. One could freely express any political opinion, except in Russia. Press censorship was unknown, except in Russia. Any white man could emigrate freely to any part of the world. The limitations of freedom in Czarist Russia were regarded with horror throughout the rest of the civilized world, and the power of the Russian Secret Police was regarded as an abomination. Russia is still worse than the Western World, not because the Western World has preserved its liberties, but because, while it has been losing them, Russia has marched farther in the direction of tyranny than any Czar ever thought of going. "
  "　1914年より前の世界を覚えている人たちのみが，これまでにどれほど多くのものが失われてしまったかをよく理解することができる。（1914年以前の）あの幸福な時代には，ロシアを除いたどこにおいても，パスポート（旅券）なしに旅行をすることができた。ロシア以外では，いかなる政治的意見も自由に発言することができた。新聞の検閲は，ロシア以外では,知られていなかった。白人は誰でも，世界中のどこへでも自由に移住することができた。ツァー体制化のロシアにおける自由の制限は，他の文明世界全体で恐怖の眼をもってみられ，ロシアの秘密警察の権力は，忌み嫌うべきこととみなされた。ロシアはまだ西側世界より悪いが、それは西側世界が自由を保持してきたからではなく，西側世界が自由を失ってきた一方で，ロシアはどのツァーも考え及ばなかったほど圧制の方向にさらに進んだからである。")
 
  (list @~cite[BR-GO-1984] @~cite[BR-GO-1984-ja])
 )@;end first list
)@; end tabular

Bertrand Russel's @emph{On Education} describes the ethic of responsible people: not just writers and intellectuals.

@(tabular #:sep @hspace[1] #:row-properties '(left right)
(list
  (list
  "Truthfulness is something of a handicap in a hypo-critical society, but the handicap is more than out-weighed by the advantages of fearlessness, without which no one can be truthful. We wish our children to be upright, candid, frank, self-respecting; for my part, I would rather see them fail with these qualities than succeed by the arts of the slave. A certain native pride and integrity is essential to a splendid human being, and where it exists lying becomes impossible, except when it is prompted by some generous motive. I would have my children truthful in their thoughts and words, even if it should entail worldly misfortune, for something of more importance than riches and honours is at stake."
  "我々は,ごまかしに満ちた世界に住んでいる。ごまかすことなく育てられた子供は,通常尊敬に値すると考えられている多くの事柄をきっと軽蔑する（軽蔑するように運命づけられている）。軽蔑はよくない感情だから,これは残念なことである。子供の好奇心がそういう事柄に向かったときには満足させてあげなければならないが,私は,（わざわざ）そういうこと（注：偽善的な事柄）に子供の注意を促すようなことはすべきではないだろう。正直（誠実）であることは,偽善に満ちた社会においては,ちょっとしたハンディキャップになる。しかし,このハンディキャップは,恐怖心を持たないという利点によって,十二分に償われる。恐怖心があれば,だれ一人として,真実を語ることはできないのだ。私たちは,わが子が公平で,正直で,卒直で,自尊心のある人間になってほしいと願っている。私としては,わが子が奴隷の技能で成功するよりも,むしろ,こういう性質をもって失敗するのを見たいと思っている。すばらしい人間になるためには,生まれつきの誇りと高潔さがある程度不可欠である。そういう性質があれば、ある種の寛容な動機から嘘を言う場合は別として,嘘をつくことは不可能になる。私は,たとえ世俗的な不幸を招くとしても,わが子には思想と言葉（発言や論文執筆）において正直（誠実）であってほしいと思っている。なぜなら,富や名誉よりも重要なものが問われているからである。")
  (list @~cite[BR-OE-en] @~cite[BR-OE-TF-ja])))
   
Thinking of responsible people, Eduardo Galeano and Arundhati Roy also write of honor in failure.

@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211020-010.scrbl}}}

@generate-bibliography[#:tag "20211020-010-bibliography"]
@generate-footnotes[]
@;index-section[#:tag "20211020-010-glossary"]
