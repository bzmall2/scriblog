#lang scribble/manual

@;from https://dustycloud.org/misc/digital-humanities/HowTo.html

@title[#:tag (list "DustyCloud DigitalHumanities")]{Learn from dustycloud's Digital Humanities Tutorial}
@itemlist[#:style 'ordered
	  @item{item lists}
	  @item{citation conventions for:
	  @itemlist[#:style 'ordered
	  @item{book-location}
	  @item{note}]}]

And this became a new snippet too with @italic{new-yas-snippet}:

