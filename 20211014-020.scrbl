#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title{2021/10/14 Arundhati Roy Quotes}

Arundhati Roy said "Literature is the simplest way of saying a complicated thing."@~cite[AR-DN-LitPower] She made me think of Ishimure Michiko's @italic{documentary literature} @italic{Paradise in a Sea of Sorrow} (@italic{苦海浄土}、 Suffering Sea Purifying Land). My guess is that it was @italic{literature} that moved the doctors and researchers and photo-journalists to start attending to Minamata. Eugene Smith ended up in Minamata because of a 1950s Japanese housewife's new form of literature.  A company doctor that later worked to find his factory's organic mercury as causing Minamata Disease in cats was given a copy of Arthur Miller's @italic{An Enemy of the People} just as he was called to examine the first (recognized) victim of his employer. So it is eerily appropriate that the line that makes me think of Ishimure Michiko arose from Arundhati Roy's @italic{Arthur Miller Freedom to Write Lecture}.

@nested[#:style 'inset]{
Literature provides shelter. That's why we need it

...

In the era of surveillance capitalism, a few people will know everything about us, and will use this information to control us.

...

So, as we lurch into the future, in this blitzkrieg of idiocy, Facebook “likes,” fascist marches, fake-news coups, and what looks like a race toward extinction—what is literature’s place? What counts as literature? Who decides? Obviously, there is no single, edifying answer to these questions. So, if you will forgive me, I’m going to talk about my own experience of being a writer during these times—of grappling with the question of how to be a writer during these times, in particular in a country like India, a country that lives in several centuries simultaneously.

...

So, as we lurch into the future, in this blitzkrieg of idiocy, Facebook “likes,” fascist marches, fake-news coups, and what looks like a race toward extinction—what is literature’s place? What counts as literature? Who decides? Obviously, there is no single, edifying answer to these questions. So, if you will forgive me, I’m going to talk about my own experience of being a writer during these times---of grappling with the question of how to be a writer during these times, in particular in a country like India, a country that lives in several centuries simultaneously.

...

 Most were stories about people who have fought against these attacks – specific stories, about specific rivers, specific mountains, specific corporations, specific peoples’ movements, all of them being specifically crushed in specific ways. These were the real climate warriors, local people with a global message, who had understood the crisis before it was recognized as one. And yet, they were consistently portrayed as villains—the anti-national impediments to progress and development.

...

When the essays were first published (first in mass-circulation magazines, then on the Internet, and finally as books), they were viewed with baleful suspicion, at least in some quarters, often by those who didn’t necessarily even disagree with the politics. The writing sat at an angle to what is conventionally thought of as literature. Balefulness was an understandable reaction, particularly among the taxonomy-inclined—because they couldn’t decide exactly what this was—pamphlet or polemic, academic or journalistic writing, travelogue, or just plain literary adventurism? To some, it simply did not count as writing:

...

But in other places, let’s call them places off the highway, the essays were quickly translated into other Indian languages, printed as pamphlets, distributed for free in forests and river valleys, in villages that were under attack, on university campuses where students were fed up of being lied to. Because these readers, out there on the frontlines, already being singed by the spreading fire, had an entirely different idea of what literature is or should be.

...

Fact and fiction are not converse. One is not necessarily truer than the other, more factual than the other, or more real than the other. Or even, in my case, more widely read than the other. All I can say is that I feel the difference in my body when I’m writing.

Sitting between the two professors, I enjoyed their contradictory advice. I sat there smiling, thinking of the first message I received from John Berger. It was a beautiful handwritten letter, from a writer who had been my hero for years: “Your fiction and nonfiction—they walk you around the world like your two legs.” That settled it for me.

...

Could I write as compellingly about irrigation as I could about love and loss and childhood? About the salinization of soil? About drainage? Dams? Crops? About structural adjustment and privatization? About the per unit cost of electricity? About things that affect ordinary peoples’ lives? Not as reportage, but as a form of storytelling? Was it possible to turn these topics into literature? Literature for everybody—including for people who couldn’t read and write, but who had taught me how to think, and could be read to?

I tried. And as the essays kept appearing, so did the five male lawyers (not the same ones, different ones, but they seemed to hunt in packs). And so did the criminal cases, mostly for contempt of court. One of them ended in a very short jail sentence, another is still pending. The debates were often acrimonious. Sometimes violent. But always important.

Almost every essay got me into enough trouble to make me promise myself that I wouldn’t write another. But inevitably, situations arose in which the effort of keeping quiet set up such a noise in my head, such an ache in my blood, that I succumbed, and wrote. Last year when my publishers suggested they be collected into a single volume, I was shocked to see that the collection, My Seditious Heart, is a thousand pages long.
@~cite[AR-PenLect]
}

It will be interesting to think about this speech along with @emph{Food First}'s @italic{Backgrounder}, @emph{World Hunger: Ten Myths} and maybe Yanis Varoufakis's discussion of choosing to write a novel instead of a "boring" treatise. 

@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211014-020.scrbl}}}

@generate-bibliography[#:tag "20211014-020-bibliography"]
@generate-footnotes[]
@;index-section[#:tag "20211014-020-glossary"]
