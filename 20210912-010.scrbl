#lang scribble/manual
@(require scriblib/footnote)
@(define-footnote a-fn generate-footnotes)

@title{2021/09/12 Scribble images}

On blog pages, it can be helpful to show photos. So, I'll have to get comfortable with @italic{scribble}'s @italic{image} procedure. Using @italic{image} should be fairly simple but I'd also like to put pictures on the left half or third of the page so that they can be seen along with relevant text. It will be more challening to figure out how to add styles to an @italic{image} so that it @italic{floats} to one side, or so that it is part of a @italic{grid} and can be seen along with an accompanying paragraph. It took a long time for me to see how to make style adjustments before. @a-fn{https://gitlab.com/bzmall2/essay-racket/}

@section{Mask Cascading}

When I forget the hand-made cloth mask it seems like a waste to send disposable masks directly to one of Japan's incinerators. Cascading is a key thought, a threshold concept, an important idea. Re-using things for a long time and avoid further burdening the environment is a good idea.

@image{public/imgs/20210912-010-mask-cascading-zeppelin-bands.jpg}

Pulling the cords off a mask and tying Zeppelin bends @a-fn{Many knots fail when dealing with modern synthetic rope materials, but this bend isn’t one of them.}  lets you replace rubber bands. The part that gives masks structure around the nose might replace those other little ties too.

@section{Zeppelin Bend}

@nested[#:style 'inset]{
@italic{The Zeppelin Bend, also known as the Rosendahl Knot, is a practical knot that uses a combination of overhand knots that interlock together. It’s extremely reliable, rarely slips or binds, and can be untied even after securing a heavy load. However, it can’t be untied while loaded!

It’s a popular knot with an unusual history. In truth, that history is up for debate, but it’s an interesting story nonetheless. It’s believed to have been used as a knot deployed for securing zeppelins. This is probably untrue. However, the myth is quite a good one. It’s said that Vice-Admiral Charles Rosendahl, the CO of the Los Angeles/ZR3 American Zeppelin, insisted on this particular knot to be used to moor his ship. Despite another name for the Zeppelin Bend Knot being the Rosendahl Bend, the story is very likely to be untrue.} @a-fn{See: https://www.boatsafe.com/zeppelin-bend-knot/}}

@margin-note{This page's source: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20210912-010.scrbl}}

@generate-footnotes[]





