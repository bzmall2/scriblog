
;; don't need module and provide for a simple include....
;;(module bz-styles racket
;;(provide pagecodelink-style)
(require scribble/core scribble/html-properties)
(define pagecodelink-style
  (make-style "PageCodeLink"
	      (list (make-css-addition "pagecodelink.css"))))

;;) ;; might be better to re-name as bz-scrbl-styles.rkt,
;; ; subvocalize better with (include bz-srbl-styles.rkt); show not using at-exp
