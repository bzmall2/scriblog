#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@;(require "bibliography/sources.scrbl")
@(require "bibliography/Tolstoy-w-LaoTzu.scrbl")
@(define-cite ~cite citet generate-bibliography)

@title{2021/10/24 }

A sentence from the first pages of a Japanese version of 老子(Lao Tzu, or Lao Tse: I prefer to think "RouShi" to avoid unfortunate tacky connotations that somehow became encrusted on "Tao de Ching") got me searching for Tolstoy's thoughts.

@nested[#:style 'inset]{
... 「人類を悩ますあらゆる災禍は、 人間が必要なことを為す怠るとこから生ずるのではない。 かえってさまざまな不必要ななことを為すとこから生ずる」 といい、 「二兎がもし老子のいわゆる "無為" を行うならば、 ただにその個人的な災禍をのがれるのみならず、 同時にあらうる形式の政治に固有する災禍をも免れるであろう」 といったのは、 トルツトイであるが (「無為」柳田泉訳)、 人類の文明の歪みと危険性を警告し、 人年の不必要ないとなみの徹底的な切り棄てを教えて、 無為の安らかな社会に人類の至福を説いたのも、 老子がその最初の哲人である。@~cite[(in-bib RoShi-AsahiBunko-1 ", ")]}

Another article turned up by the search mentioned that Tolstoy relinquished most of his copyrights. So @emph{無為}(@emph{Non-Activity})  is probably available on-line. Right? Yes:

@nested[#:style 'inset]{
There is a little known Chinese philosopher Laozi (the first and the best translation of his "The Book of the Way and Virtue" by Stanislas Julien). The essence of the teaching of Laozi is that the highest well-being of individuals in particular, as well as of the collection of peoples, can be acquired through knowledge "Tao" - a word, which translates to "by way, by virtue, by truth"; and the cognition of "Tao" can be acquired only through inaction, "le non agir", as Julien translates it. All the disasters of people, according to the teaching of Laozi, come not so much from what they have not done out of what was needed, as much as what they do out of what they shouldn’t. And therefore people would have got rid of all the disasters, personal and especially public which the Chinese philosopher mostly referred to, if they have maintained the inaction (s'il pratiquaient le non agir).

And I think he is absolutely right. Let everyone work hard. But what? Punter, the banker returns from the exchange where he worked hard; colonel - from training people to murder, manufacturer - from his enterprise where thousands of people ruin their lives over producing mirrors, tobacco, vodka. All these people work, but is it possible to approve their work?@~cite[LT-NA]}

The search prompted by the small little Japanes books on RouShi turned up a variety of interesting on-line pages.

@nested[#:style 'inset]{
... When he was 29, Tolstoy finally freed his serfs, and almost immediately began work to organize a farmers’ cooperative, giving instructions on how to manage the system. At the same time, he became very involved in helping educate farm children. ...

The Doukhobor movement, in opposition to the ritualistic Russian Orthodox Church, began in the Ukraine in the late 18th century. They were strictly puritanical and banned the taking of any form of life. Tolstoy shared many of their beliefs. In the late 19th century, the Doukhobors mounted a serious challenge to the state, rejecting the Czar and the Church. Despite the state’s clampdown on the sect, they steadfastly held to a policy of non-violent opposition, which won wide public support. The government was at a loss as to what to do. Since the movement wasn’t a criminal one, further oppression meant international isolation for the Russian state. The government finally decided to allow the Doukhobors to emigrate to Canada. ... 

Tolstoy’s thoughts have also left a solid legacy in Japan. There is a statue of him in front of the auditorium of Showa Women’s University, founded by Japanese poet Hitomi Enkichi (1883-1974). Enkichi’s wife, Midori, had wanted the new school to be “a university of love as Tolstoy might have founded.” Japanese religious leader Tenko Nishida (1872-1968) founded an organization of ascetic practice and social service based on Tolstoy’s principles. He called it Ittoen and located it in the Yamashina area of Kyoto.
@~cite[LTolstoy-DavisFumiko-1]}

The literary criticism from @emph{The New Republic} seems interesting too.

@nested[#:style 'inset]{
The shackled prisoners march through the city, and a little boy knew without any doubt—he was quite sure, for he had the knowledge straight from God, that these people were just the same as he and everyone else was, and therefore something wicked had been done to them, something that ought not to be done and he was sorry for them, and horrified not only at the people who were shaved and fettered but at the people who had shaved and fettered them.

I doubt anyone but the old Tolstoy could get away with saying that the little boy's knowledge comes "straight from God." And then the scene in which the prisoners lie about in the Siberian muck and Nekhlyudov sees that all the vices which developed among the convicts ... were neither accidents nor signs of mental and physical degeneration ... but that they were the inevitable result of the delusion that one group of human beings has the right to punish another.@~cite[TNR-LT-old]}



@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211024-010.scrbl}}}

@generate-bibliography[#:tag "20211024-010-bibliography"]
@generate-footnotes[]
@;index-section[#:tag "20211024-010-glossary"]
