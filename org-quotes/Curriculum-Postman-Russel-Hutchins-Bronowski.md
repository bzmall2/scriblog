### Ideas for a Coherent Curriculum
#### From Bertrand Russel, Neil Postman, and Robert Maynard Hutchins

https://ruhrspora.de/posts/2166137
2018年  4月  4日 水曜日 17:21:32 JST


> The conception is that of the human race as a whole, fighting against chaos without and darkness within, the little tiny lamp of reason growing gradually into a great light by which the night is dispelled. The divisions between races, nations, and creeds should be treated as follies, distracting us in the battle against Chaos and Old Night, which is our one truly human activity.  &#x2014; Bertrand Russel

-   On Education (1926) p. 168

> この観念は，外部の混沌と（人間の）内部の暗闇と闘い，理性のちっぽけな灯火が次第に成長して大きな明りとなり暗黒を追い払う，といった，全体としての人類の観念である。人種や国家や教義の間で区別を行うことは，私たちの唯一の人間的な活動とも言うべき（ミルトン『失楽園』の）「混沌（王）と暗闇（王）（Chaos and Old Night）」 との闘いから私たちの心を逸らしてしまう愚行であるとして取り扱わなければならない。 

I've learned a lot from the Japanese translations on this site. I don't know how readable the Japanese is, but the notes provide a great background.  In this paragraph we see that _Chaos_ and _Old Night_ called up associations with Milton's _Paradise Lost_ for readers in 1926.  Another paragraph from this same book explains where the character _Uriah Heep_ comes from: a Dickens book. 
-   教育論 オンライン <http://russell-j.com/beginner/OE15-040.HTM>)

> One obviously treads on shaky ground in suggesting a plausible theme for a diverse, secularized population. Nonetheless, with all due apprehension, I would propose as a possibility the theme that animates Jacob Bronowski's *The Ascent of Man*. It is a book, and a philosophy, filled with optimism and suffused with he transcendent belief that humanity's destiny  is the discovery of knowledge. Moreover, although Bronowski's emphasis is on science, he finds ample warrant to include the arts and humanities as part of our unending quest to gain a unified understanding of nature and our place in it. &#x2014; Neil Postman

-   *Technopoly The Surrender of Culture to Technology* (1993) pp. 187-88

> さまざまにちがう、 非宗教的な人々にたいして、 もっもらしいテーマを提案するのは、 あきらかに危ない橋を渡ることになる。だが、 こうした当然の心配にもかかわらず、 私はヤコブ・ブロノウスキーの著書「人間の上昇」に活気をあたえているテーマを一つの可能性として提起したい。 これは楽天主義に満ち、 人間の天命が知の発見にあるというすぐれた信念の横溢した本であり、 哲学である。 さらに、 ブロノウスキーの強調は科学にあるけれども、かれは、 自然の統一した理解と、 自然のなかでの私たちの位置を把握しょうとするあくなき探求の一部に、 芸術と人文科学をふくめる理由が十分あるとする。 &#x2014; ニール・ポストマン

-   *技術vs人間  ハイテク社会の危険* (1994) GS研究会訳 pp. 245-246

> Thus, to chart the ascent of man, which I will here call "the ascent of humanity," we must join art and science. But we must also join the past and the present, for the ascent of humanity is above all a continuous story. It is, in fact, a story of creation, although not quite the one that the fundamentalists fight so fiercely to defend. It is the story of humanity's creativeness in trying to conquer loneliness, ignorance, and disorder. And it certainly includes the development of various religious systems as a means of giving order and meaning to existence&#x2026;

> このようにして、「人間の上昇&#x2014;私はここでは「人間性の上昇」とよぶが&#x2014;を企画するには、芸術と科学を結びつけなければならない。 だが、 私達はまた、 過去と現在を結びつけなければならない。 なぜなら、 人間性の上昇は、 なかんずく途切れのない物語だからである。 それは事実上、 創造の物語だが、 ファンダメンタリストたちが擁護しょうと熱烈にたたかっているものと同一のものではない。 それは、 孤独、 無知、 そして無秩序を征服しょうと努力している人間性の創造力の物語である。 また、 それは、 まちがいなく存在に秩序と意味を与える手段としてのさまざまな宗教的システムをふくんでいる。&#x2026;

> In any event, the virtues of adopting the ascent of humanity as a scaffolding on which to build a curriculum are many and various, especially in our present situation. For one thing, with a few exceptions which I shall note, it does not require that we invent new subjects or discard old ones. The structure of the subject-matter curriculum that exists in most schools at present is entirely usable. For another, it is a theme that can begin in the earliest grades and extend through college in ever-deepening and -widening dimensions. Better still, it provides students with a point of view from which to understand the meaning of subjects, for each subject can be seen as a battleground of sorts, an arena in which fierce intellectual struggle has taken place and continues to take place. Each idea within a subject marks the place where someone fell and someone rose. Thus, the ascent of humanity is an optimistic story, not without its miseries but dominated by astonishing and repeated victories. From this point of view, the curriculum itself may be seen as a celebration of human intelligence and creativity, not a meaningless collection of diploma or college requirements.

> どんな出来事においても、カリキュラムをつくりあげる足場として人間性の上昇を取り上げる長所は、 現在の状況下ではとくにいろいろたくさんある。 わずかな例外はあとに記すとして、 その一つとして、 人間性の上昇には、 新しい課目別のカリキュラム編成は、 すべて利用できる。 もう一つ別のとをいえば、 それは、 小学校低学年にはじまり大学まで、 その範囲が絶えず深く、 絶えず広がっていくテーマなのである。 さらにいいことに、 それは、 課目の意味を知る見方を生徒たちにあたえる。なぜなら、 それぞれの課目は、 ある種の戦場、 はげしい知的たたかいがくりひろげられてきた、 またひきつづきくりひろげられている闘技場とみていいからである。 課目内のそれぞれの考え方が、 だれかが敗れ、 だれかが勝つ場を示す。 こうして、 人間性の上昇は、 悲惨なことがないわけではないが、 目ざましい、 また繰り返される勝利が支配する楽天的な物語である。 この観点から、 カリキュラム自身が人間の知性と創造性の称賛とらえられるし、 無意味な学位や大学資格の寄せ集めではなくなる。

> Best of all, the theme of the ascent of humanity gives us a nontechnical, noncommercial definition of education. It is a definition drawn from an honorable humanistic tradition and reflects a concept of the purposes of academic life that goes counter to the biases of technocrats.  I am referring to the idea that to become educated means to become aware of the origins and growth of knowledge and knowledge systems; to be familiar with the intellectual and creative processes by which the best that has been thought and said has been produced; to learn how to participate, even if as a listener, in what Robert Maynard Hutchins once called The Great Conversation, which is merely a different metaphor for what is meant by the ascent of humanity. You will not that such a definition is not child-centered, not skill-centered, not even problem-centered. It is idea-centered and coherence-centered. &#x2026; &#x2014; Neil Postman

-   *Technopoly The Surrender of Culture to Technology* (1993) pp. 187-88

> もっともいいことは、 人間性の上昇というテーマが教育の非技術的、 非商業的定義をあたえることである。 それは、 高潔な人間的伝統から引き出される定義である、 学研生活の目的はタクノクラートの偏向に反対することであるという考え方を反映する。 教養のある人になるということは知と知の体系の期限と成長について熱知するようになることであり、 思考においても言動においても最良のものが生産されるような知的・創造的作業に親しむことであり、 たとえ聞き手としてであっても、 かつてメイナード・ハッチンズが「偉大な会話」&#x2014;人間性の上昇と言っていることの別のメタファにすぎない&#x2014;とよんでいるものにどう参入するかを学ぶことだという考えを私は言っているのである。 このような定義は子ども中心でもなく、 訓育中心でもなく、 技能中心でもなく、 問題中心でさえない。 それは考え方を中心におき、 首尾一貫性を中心におく。 &#x2014; ニール・ポストマン

-   *技術vs人間  ハイテク社会の危険*  pp. 245-246

I like Robert Maynard Hutchins's /metaphor of **The Great Conversation** along with the _education of free persons_.

> The aim of liberal education is human excellence, both private and public (for man is a political animal). Its object is the excellence of man as man and man as citizen. It regards man as an end, not as a means; and it regards the ends of life, and not the means to it. For this reason it is the education of free men. Other types of education or training treat men as means to some other end, or are at best concerned with the means of life, with earning a living, and not with its ends.

>  substance of liberal education appears to consist in the recognition of basic problems, in knowledge of distinctions and interrelations in subject matter, and in the comprehension of ideas.

> Liberal education seeks to clarify the basic problems and to understand the way in which one problem bears upon another. It strives for a grasp of the methods by which solutions can be reached and the formulation of standards for testing solutions proposed. The liberally educated man understands, for example, the relation between the problem of the immortality of the soul and the problem of the best form of government; he understands that the one problem cannot be solved by the same method as the other, and that the test that he will have to bring to bear upon solutions proposed differs from one problem to the other. 

- https://www.thegreatideas.org/libeducation.html

\#facdev #facultydevelopment #education #curriculum #NeilPostman  #BertrandRussel #RobertMaynardHutchins #Japanese #Translation