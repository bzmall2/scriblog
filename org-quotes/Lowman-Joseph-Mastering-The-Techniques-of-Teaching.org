#+TITLE: Joseph Lowman: Mastering the Techniques of Teaching
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>

  I got an interest in Joseph Lowman's book /Mastering the Techniques of Teaching/ from Edward Tufte's /Beautiful Evidence/.(See quotes from [[https://hub.libranet.de/channel/bsmall2/?f%3D&mid%3D195cd35a2f27a368643499c2ecdc713d00079f214d1f922461c35cb0a018cec2@hub.libranet.de][page 161 of /Beautiful Evidence/]].  I will put some quotes here to keep them in mind.
 
#+BEGIN_QUOTE
  Lowman, Joseph.(1995) /Mastering the techniques of teaching: second edition/. San Francisco: John Wiley & Sons
#+END_QUOTE
** Mastering the Techniques of Teaching (1995)

*** p.10 exemplary teacher?
#+BEGIN_QUOTE
... what makes an exemplary college teacher?
#+END_QUOTE

*** p.11 routine evaluations
#+BEGIN_QUOTE
Student evaluations have become routing at most U.S. colleges in the past two decades, and their primary purposes have been to aid in faculty personnel decisions and to give instructors personal feedback to stimulate improvement. They are also sometimes used to provide public information for course selection...

the wisdom of collecting systematic student evaluation data now seems self-evident in most circles.
#+END_QUOTE

*** p.44 The Psychology of College Teachers
#+BEGIN_QUOTE
Some college teachers gain little satisfaction from meeting their classes and welcome the opportunity to spend their time at other academic pursuits, especially scholarly research on which reputation, promotion, and salary depend. But many---in fact, I suspect, the majority--- /do/ receive considerable personal satisfaction from classroom teaching, although the culture in many schools and departments does not encourage them to express such satisfaction openly
#+END_QUOTE

#+BEGIN_QUOTE
Teaching students what one knows also provides the warm satisfaction that comes whenever one gives away something one values, as when one purchases a present or composes a poem for a special occasion. Teaching is giving knowledge away, and many college teachers are compulsive sharers of what they know, eager to pass on information or insights to willing listeners. Teaching at any level is pleasurable for such individuals. People who find little joy in giving to others are likely to find less personal satisfaction in teaching than those who are intrinsically generous.
#+END_QUOTE

*** p.45 public performance
#+BEGIN_QUOTE
Whereas classroom instruction inevitably requires public performance, being front and center is not universally exhilarating. Many instructors find teaching pleasurable /in spite/ of the fact that it must be done standing alone in front of a group. Others, however, clearly relish the chance to captivate, to entertain, or astonish the audience that appears regularly to hear them. A frequently heard adage about college teaching is that "all great teachers are hams at heart." It is not essential for teacher to be "hams" to be outstanding. However, instructors who do enjoy performing are more likely to find their job rewarding.
#+END_QUOTE

*** p.51 Teacher messages
#+BEGIN_QUOTE
Teacher messages that are overtly controlling or emphasize the hierarchical power relationship between teacher and student encourage students to be less independent (Lowman, 1990b). Whether a student finds such remarks positive or negative depends on how much the student likes having things structured by someone else; but extensive directions are more likely to be appreciated when associated with affection than when coupled with hostility.
#+END_QUOTE

*** p.55 Student needs for mastery
#+BEGIN_QUOTE
Student needs for mastery can be frustrated in several ways. Uninteresting or confusing presentations can dampen almost any student's curiosity and desire to seek challenges. Although some students relish overcoming obstacles more than others, all expect to be challenged, and classes that move too slowly, focus on obvious points excessively, or are devoid of even occasional references to critical questions are likely to be dissatisfying.
#+END_QUOTE

#+BEGIN_QUOTE
Some students' need for mastery is indistinguishable from their need to surpass their classmates by receiving higher grades. Unfortunately, it is difficult to design a course that satisfies grading-oriented students' competitive needs without frustrating others' desire to meet individual learning needs---for example, focusing class presentations on specific facts and theories please those students who are most concerned about their grades, but frustrates those who are eager to consider the implications of the content. Teachers share with athletic coaches the dilemmas of handling students emotionally to avoid complacency among those who "win" and hopelessness among those who "lose," and to help students learn to see external challenges as opportunities to test their skills or assess their learning rather than as obstacles to be overcome at any cost.
#+END_QUOTE

*** p.122-123 performance preparation time 
#+BEGIN_QUOTE
College teachers must give exciting and moving performances day after day... Like other performers, college teachers will find it difficult at times to "get up" for teaching... Luckily, college teachers can benefit from some of the techniques that professional actors use to give their best performances time and again.

The single most useful technique is to recognize that you must prepare yourself emotionally as well as intellectually before your "performance." No instructor is likely to go into a class without some idea or specific plan of what he or she wants to do and the necessary materials (books, props, maps, slides) to carry it out. Many college teachers, though, walk directly from the parking lot or committee meeting into a classroom, with only a short pause to collect their thoughts. The outstanding professors I interviewed typically set aside from five to thirty minutes beforehand to think about the class they are going to teach. Some close their office doors and hole telephone calls; others walk a longer route to their classroom buildings than necessary. However you manage to find a few minutes of solitude before class (in the bathroom, if necessary!), recognize the importance of emotional preparation in ensuring a high-quality performance, especially if you are tired or depressed.
#+END_QUOTE

  #FD #FacultyDevelopment #College #Teaching #JosephLowman #EdwardTufte
