#+TITLE: Arundhati Roy The Air We Breathe
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>

# # BackTalk Pages (Ann Berthoff's "Dialectical Journal" styles
#+HTML_HEAD: <style> div.outline-2 { clear: left; }  </style>
#+HTML_HEAD: <style> h2 { margin-bottom: 2px; }  </style>
#+HTML_HEAD: <style> h3 { margin-top: 2px; }  </style>
#+HTML_HEAD: <style> div.outline-3 { float:left; width: 48%; }  </style>
# * page 
# ** quote 
# ** response 


#+BEGIN_QUOTE
 ... a decent reader would recognize that her fiction and nonfiction are animated by the same fierce love of justice—and make the same demands on readers. 
#+END_QUOTE

#+BEGIN_QUOTE
Of course, as a novelist, I never want to write about “issues” like “the Indian family.” What I want to write about is the air we breathe. These days, I feel that novels, I don’t know for what reason—maybe because of the speed and the way that books have to be sold—these days, novels are becoming kind of domesticated, you know? They have a title, and a team, and they are branded just like NGOs: you writing on gender, you writing on caste, you writing on whatever. But for me, the fact is that these are not “issues”—this is the air we breathe. 
#+END_QUOTE

#+BEGIN_QUOTE
The Muslim community is literally being pushed into ghettos. People are living in graveyards. To the West, it sounds like magic realism, but in Delhi you see it all the time: people living amongst graves and over graves in the old city. 
#+END_QUOTE

* page
** quote
#+BEGIN_QUOTE
The structure of a novel is important—it’s what allows you to break out of this process of domestication. The sprawling structure of this book, the narrative style—it’s almost like looking at a city whose plans are ambushed. It has unauthorized colonies and illegal entries. People come together in such places. 
#+END_QUOTE

** response
#+BEGIN_QUOTE

#+END_QUOTE

* page
** quote
#+BEGIN_QUOTE
All these people have stories. They’ve come from different places. And this allows them to share their experiences and create a form of solidarity that could not exist in isolated villages. In The Ministry of Utmost Happiness, my challenge was to walk the reader through a city—the narrative city as well as the geographical city—and unlock its potential. 
#+END_QUOTE

** response
William Gibson
#+BEGIN_QUOTE

#+END_QUOTE

* page
** quote
#+BEGIN_QUOTE
I wrote those essays with a tremendous sense of urgency. They were written with a very particular purpose, each of them, because something was closing in. When I wrote “Walking With the Comrades,” the atmosphere in India at the time—you know, Manmohan Singh had declared the Maoists to be “India’s single largest internal-security threat”; Operation Green Hunt had been announced; the media was going crazy. Every day on TV, they just kept saying, “Maoists are terrorists, Maoists are terrorists”—they were describing the poorest people in the country, in the world, as terrorists, simply because they were standing up for their rights. 

There was no mention whatsoever about mining or privatization or land grabs in the mainstream media. So I went in just to say, “Look, people, this is not the whole story.” 

Also, about Kashmir, I’ve traveled there a lot, but I haven’t written a big essay about the place ... The air there is seeded with 25 years of the densest military occupation in the world. What does that do to the psychology—of the soldier, of the Indian, all the factions of Kashmir? It’s a psychotic situation there now, and only fiction can deal with it. 
#+END_QUOTE

** response

 - https://www.thenation.com/article/the-air-we-breathe-a-conversation-with-arundhati-roy/


