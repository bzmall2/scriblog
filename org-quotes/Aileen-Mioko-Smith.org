

** Aileen-Mioko-Smith-Sunada-sanTachi.jpg
   2021年  9月 21日 火曜日 10:02:12 JST
   051-teaser-story-big.jpg
    https://www.kyotojournal.org/wp-content/uploads/2021/03/051-teaser-story-big.jpg
   
> As Gene and Aileen conclude in their book, Minamata, “The morality that pollution is criminal only after legal conviction is the morality that causes pollution.” ...

> The organizer of that show, Motomura Kazuhiko, suggested that Gene photograph in Minamata, Kumamoto Prefecture, where a public health crisis was playing out (see sidebar). Aileen and Gene rented a house there, thinking they might stay three months. Three months became three years, documenting the lives and the struggles of victims of deadly methylmercury industrial pollution and their families. Publication of their early photo-essay in Life brought Minamata to international attention, and their book, Minamata, co-authored by Aileen and Gene, reached a worldwide audience upon publication in 1975.

> A poignantly intimate wide-format integration of hard-hitting reportage and graphic documentary photography, Minamata indicted cynical corporate negligence and malfeasance, and administrative ineptitude. Together, Aileen and Gene recorded local citizens’ heroic resistance through protest and protracted court hearings to a final verdict in their favor. Although Gene and Aileen only appear in the book as witnesses documenting the devastation it has been adapted into a screenplay about their time in Minamata, and filmed as—believe it or not—a Hollywood “true story drama thriller.” Directed by Andrew Levitas, the movie, starring Johnny Depp, was shot in Montenegro, Serbia and Japan , and is scheduled to be released in 2021. As described by Levitas, “This uplifting story of hope, passion, and community reminds us that alone we may be the parts per million, but together we are the millions.” ...

> ... And you don’t see it just a pollution issue. I think you see it as life, but each person sees it differently or it resounds in them differently. I feel that the photograph is completed when the viewer sees it. And whatever happens inside that person is the final product. So I want to give an opportunity to have people experience that.

> But, I’m still honoring the the agreement that we reached with the parents, of not releasing it further. The parents don’t want it out anymore. For the last 20 years, I have used my copyright to say no to further release. But I’m going to release it through discussion with the parents if the book gets republished, and for a few moments in the movie, because they simulated the scene. Without the real photograph, the simulation would be the image that exists, rather than the real thing, so although my agreement with the movie people was not to show the image, I said yes. ....

> ... When it hits somebody in junior high or high school is where it’s the strongest. A person that was 17, who felt a chip on his shoulder, who had never been loved, saw this photograph and it just completely dissolved something inside and changed him. And Fukushima Atsuko, one of the leaders now of the Fukushima evacuees fighting in the lawsuit, says in junior high school, every single day, she went to the library and looked at this book, and she decided to go into environmental monitoring work because of it. That’s her whole career. She said, “You just changed my life.” That’s why I want to republish the book.

> But I think the most exciting thing I heard was after people came out of the Berlinialle premiere. We had a flyer about what’s happening with mercury now— the levels are going up and up and up worldwide and we’re trying to get it down. There is a convention, but we want everybody to ratify it and really commit to reducing the pollution all over the world, including from coal—coal-fired plants emit mercury. When the audience came out, I had to go to the after-party with Johnny Depp and everybody, but [friends] were there handing out the leaflet that we had made. And the reaction was really exciting. They described three types of reactions. One is “I’m just absolutely flabbergasted this kind of thing could have happened in an advanced country like Japan, so completely floored that this was going on, in Japan.” Or “Yeah, sure, I’ll take a flyer.” Then for another group of people like, “It was just too much for me, I can’t even take a flyer,” that kind of feeling. And then other people said, “Yeah, I’ll grab a flyer, how many can I have?—I want to show it to my friends.”

> interview with Aileen Mioko Smith

 > https://www.kyotojournal.org/wp-content/uploads/2021/03/051-teaser-story-big.jpg

 - https://www.kyotojournal.org/conversations/aileen-mioko-smith-pt1/

**  Kyoto Journal pt2

> ... The Three Mile Island accident happened in March 1979, just a year before it got published. Hajime was involved in the very first lawsuit trying to stop nuclear power in Japan. When the accident at Three Mile Island happened, people noticed all sorts of physical things like metallic smell and taste, changes in the color of the air, changes in vegetation and birds, et cetera, et cetera. Some people had tingling also going through their body and were getting really hot, all sorts of things. It connected with Minamata, where all the scientists were saying, “There are only acute cases. Everybody else is fine. There’s no problem,” whereas the local people were suffering in a really widespread way. Local people had to do go to the Kumamoto doctors and convince them to come down and examine them. ...

> Hajime and I went to Three Mile Island in the fall of 1982 and lived there and did lots of interviews for half a year. People we interviewed said, “Japan must not have any nuclear power, because you’re seismically so active.” And we said, “No, there are nuclear power plants.” “Well, how many?” I didn’t even know how many. I didn’t even know how many were around Kyoto. To answer them, I had to find out, and I learned that there were so many right near Kyoto. Over and over again, the people of Three Mile Island said, “Don’t go back to such a dangerous place. You must stay here.” The Three Mile Island site was still operating but with only one reactor, versus Japan, this seismic country, with 17 nuclear power reactors at the time, many of them near Kyoto.

> After we came back to Japan, everybody working on trying to stop nuclear power wanted to hear about the experience of people at Three Mile Island. It ended up being this incredible introduction to the whole citizens’ struggle from Hokkaido to Kyushu. I was invited to come and speak. Opposition movements were active all over Japan, in places trying to prevent a plant from being built or in those where one had already been built.

 - https://www.kyotojournal.org/conversations/aileem-mioko-smith-interview-pt2/
