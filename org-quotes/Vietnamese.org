#+TITLE: THE VIETNAMESE FOLK LITERATURE: Dương Đình Khuê
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>


* THE VIETNAMESE FOLK LITERATURE: Dương Đình Khuê

#+BEGIN_QUOTE
 - Giầu điếc sang đui.
   - The rich man is deaf and the nobleman is blind.
#+END_QUOTE

#+BEGIN_QUOTE
 - Dốt đặc cán mai.
   - His ignorance is as dense as a spade’s handle.
#+END_QUOTE

#+BEGIN_QUOTE
 - Ăn ít ngon nhiều.
   - Less eating, more appetite.
#+END_QUOTE

#+BEGIN_QUOTE
 - Tửu nhập ngôn xuất.
   - When alcohol goes in, words go out.
#+END_QUOTE

#+BEGIN_QUOTE
 - Tốt danh hơn lành áo.
   - A good name is better than fine clothes.
#+END_QUOTE

#+BEGIN_QUOTE
 - Trâu chết để da
   - The dead buffalo leaves its skin
 - Người ta chết để tiếng.
   - The dead man leaves behind his reputation.
#+END_QUOTE

#+BEGIN_QUOTE
 - Sống đục sao bằng thác trong.
   - Better to die with honor than to live in dishonor.
#+END_QUOTE

#+BEGIN_QUOTE
 - Chớ thấy sông cả
   - On a large river
 - Mà ngã tay chèo.
   - Do not panic and abandon the rudder.
#+END_QUOTE

#+BEGIN_QUOTE
 - Nước chẩy đá mòn .
   - Running water may wear down a stone.
#+END_QUOTE

#+BEGIN_QUOTE
 - Tấc đất tấc vàng .
   - An inch of land is worth an ounce of gold.
#+END_QUOTE

#+BEGIN_QUOTE
 - Có đi mới đến
   - If you wish to be in some place, you have to go there
 - Có học mới hay .
   - If you wish to become a learned man, you have to study.
#+END_QUOTE

#+BEGIN_QUOTE
 - Dao có mài mới sắc
   - To be cutting, a knife must be sharpened
 - Người có học mới khôn .
   - To be wise, a person must study.
#+END_QUOTE

#+BEGIN_QUOTE
 - Làm khi lành
   - Work while you are healthy
 - Để dành khi đau .
   - To protect yourself when you get sick.
#+END_QUOTE

#+BEGIN_QUOTE
 -  Làm phúc cũng như làm giầu.
    - By helping others, you accumulate your own wealth.
#+END_QUOTE

#+BEGIN_QUOTE
 - Lo chật bụng
   - Worry about your mean heart
 - Lo chi chật nhà.
   - Rather than about your narrow house.
#+END_QUOTE


#+BEGIN_QUOTE
 - Biết thì thưa thớt
   - Speak if you know,
 - Không biết thì dựa cột mà nghe.
   - Other wise you’d better lean on the post and listen.
#+END_QUOTE

