#+TITLE: Edward Lear from Chesterton and Russel
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>


*** Nonsense Poems

[[file:images/Lear-Old-Man-With-Beard.png]]

#+BEGIN_QUOTE
There was an old man with a beard

Who said, "It is just as I feared! ---

Two Owls and a hen, four Larks and a 

Wren,

Have all built their nests in my beard."
#+END_QUOTE

*** Russel
**** On Edward Lear
#+BEGIN_QUOTE
... the best books for children are those that happen to suit them, though written for grown-up people. The only exceptions are books written for children, but delightful also to grown-up people, such as those of /Lear/ and /Lewis Carroll/.
#+END_QUOTE
  - http://russell-j.com/beginner/ON_EDU-TEXT.HTM
#+BEGIN_QUOTE
最良の児童図書とは，もともとはおとなのために書かれたものであるが，たまたま子供にも向いているような本である。唯一の例外は，たとえば，リア(注： Edward Lear, 1812-1886, 英国の画家・詩人)やルイス・キャロルが書いた本のように，子供のために書かれたもので，おとなが読んでも楽しい本である。
#+END_QUOTE
  - Japanese | English: http://russell-j.com/beginner/OE15-080.HTM

**** Triumph of Stupidity

#+BEGIN_QUOTE
The fundamental cause of the trouble is that in the modern world the stupid are cocksure while the intelligent are full of doubt.
#+END_QUOTE

  - http://wist.info/russell-bertrand/3375/

**** Yeats: The Second Coming

#+BEGIN_QUOTE
Things fall apart; the centre cannot hold; 
#+END_QUOTE

#+BEGIN_QUOTE
The ceremony of innocence is drowned;
The best lack all conviction, while the worst   
Are full of passionate intensity. 
#+END_QUOTE

 - https://www.poetryfoundation.org/poems/43290/the-second-coming



*** Chesterton

#+BEGIN_QUOTE
... we quote chiefly from Mr. Lear's 'Nonsense Rhymes.' To our mind he is both chronologically and essentially the father of nonsense; we think him superior to Lewis Carroll. 
#+END_QUOTE

#+BEGIN_QUOTE
This sense of escape is certainly less emphatic in Edward Lear, because of the completeness of his citizenship in the world of unreason. We do not know his prosaic biography as we know Lewis Carroll's. We accept him as a purely fabulous figure, on his own description of himself:

'His body is perfectly spherical,
He weareth a runcible hat.'

While Lewis Carroll's Wonderland is purely intellectual, Lear introduces quite another element—the element of the poetical and even emotional. Carroll works by the pure reason, but this is not so strong a contrast; for, after all, mankind in the main has always regarded reason as a bit of a joke. Lear introduces his unmeaning words and his amorphous creatures not with the pomp of reason, but with the romantic prelude of rich hues and haunting rhythms.

'Far and few, far and few,
Are the lands where the Jumblies live,'

is an entirely different type of poetry to that exhibited in 'Jabberwocky.' Carroll, with a sense of mathematical neatness, makes his whole poem a mosaic of new and mysterious words. But Edward Lear, with more subtle and placid effrontery, is always introducing scraps of his own elvish dialect into the middle of simple and rational statements, until we are almost stunned into admitting that we know what they mean. There is a genial ring of commonsense about such lines as,

'For his aunt Jobiska said "Every one knows
That a Pobble is better without his toes,"'

which is beyond the reach of Carroll. The poet seems so easy on the matter that we are almost driven to pretend that we see his meaning, that we know the peculiar difficulties of a Pobble, that we are as old travellers in the 'Gromboolian Plain' as he is.

Our claim that nonsense is a new literature (we might almost say a new sense) would be quite indefensible if nonsense were nothing more than a mere aesthetic fancy. Nothing sublimely artistic has ever arisen out of mere art, any more than anything essentially reasonable has ever arisen out of the pure reason. There must always be a rich moral soil for any great aesthetic growth. The principle of art for art's sake is a very good principle if it means that there is a vital distinction between the earth and the tree that has its roots in the earth; but it is a very bad principle if it means that the tree could grow just as well with its roots in the air. Every great literature has always been allegorical—allegorical of some view of the whole universe. The 'Iliad' is only great because all life is a battle, the 'Odyssey' because all life is a journey, the Book of Job because all life is a riddle. There is one attitude in which we think that all existence is summed up in the word 'ghosts'; another, and somewhat better one, in which we think it is summed up in the words 'A Midsummer Night's Dream.' Even the vulgarest melodrama or detective story can be good if it expresses something of the delight in sinister possibilities—the healthy lust for darkness and terror which may come on us any night in walking down a dark lane. If, therefore, nonsense is really to be the literature of the future, it must have its own version of the Cosmos to offer; the world must not only be the tragic, romantic, and religious, it must be nonsensical also. And here we fancy that nonsense will, in a very unexpected way, come to the aid of the spiritual view of things. Religion has for centuries been trying to make men exult in the 'wonders' of creation, but it has forgotten that a thing cannot be completely wonderful so long as it remains sensible. So long as we regard a tree as an obvious thing, naturally and reasonably created for a giraffe to eat, we cannot properly wonder at it. It is when we consider it as a prodigious wave of the living soil sprawling up to the skies for no reason in particular that we take off our hats, to the astonishment of the park-keeper... 
#+END_QUOTE

  - Bertrand Russel on Children's books in /On Education/: [[http://russell-j.com/beginner/OE15-080.HTM][russel-j.com]]
  - Edward Lear's /The Book of Nonsense/: [[https://www.gutenberg.org/ebooks/13646][gutenberg]]
  - Chesterton writes of Lear in /The Defendant/: [[https://www.gutenberg.org/files/12245/12245-h/][gutenberg]]


file:images/Lear-Dong-Luminous-Nose.png

#+BEGIN_QUOTE
That it is good for a man to realize that he is 'the heir of all the ages' is pretty commonly admitted; it is a less popular but equally important point that it is good for him sometimes to realize that he is not only an ancestor, but an ancestor of primal antiquity; it is good for him to wonder whether he is not a hero, and to experience ennobling doubts as to whether he is not a solar myth.

The matters which most thoroughly evoke this sense of the abiding childhood of the world are those which are really fresh, abrupt and inventive in any age; and if we were asked what was the best proof of this adventurous youth in the nineteenth century we should say, with all respect to its portentous sciences and philosophies, that it was to be found in the rhymes of Mr. Edward Lear and in the literature of nonsense. 'The Dong with the Luminous Nose,' at least, is original, as the first ship and the first plough were original.

It is true in a certain sense that some of the greatest writers the world has seen—Aristophanes, Rabelais and Sterne—have written nonsense; but unless we are mistaken, it is in a widely different sense. The nonsense of these men was satiric—that is to say, symbolic; it was a kind of exuberant capering round a discovered truth. There is all the difference in the world between the instinct of satire, which, seeing in the Kaiser's moustaches something typical of him, draws them continually larger and larger; and the instinct of nonsense which, for no reason whatever, imagines what those moustaches would look like on the present Archbishop of Canterbury if he grew them in a fit of absence of mind. We incline to think that no age except our own could have understood that the Quangle-Wangle meant absolutely nothing, and the Lands of the Jumblies were absolutely nowhere. We fancy that if the account of the knave's trial in 'Alice in Wonderland' had been published in the seventeenth century it would have been bracketed with Bunyan's 'Trial of Faithful' as a parody on the State prosecutions of the time. We fancy that if 'The Dong with the Luminous Nose' had appeared in the same period everyone would have called it a dull satire on Oliver Cromwell.
#+END_QUOTE

**** /The Dong with a Luminous Nose/
#+BEGIN_QUOTE
When awful darkness and silence reign
Over the great Gromboolian plain,
Through the long, long wintry nights;
When the angry breakers roar
As they beat on the rocky shore;
When Storm-clouds brood on the towering heights
Of the Hills of the Chankly Bore,---

Then, through the vast and gloomy dark
There moves what seems a fiery spark,---
A lonely spark with silvery rays
Piercing the coal-black night,---
A Meteor strange and bright:
Hither and thither the vision strays,
A single lurid light

Slowly it wanders, pauses, creeps,---
Anon it sparkles, flashes, and leaps;
And ever as onward it gleaming goes
A light on the Bong-tree stems it throws.
And those who watch at that midnight hour
From Hall or Terrace or lofty Tower,
Cry, as the wild light passes along,---
"The Dong! the Dong!
The wandering Dong through the forest goes!
The Dong! the Dong!
The Dong with a luminous Nose!"

Long years ago
The Dong was happy and gay,
Till he fell in love with a Jumbly Girl
Who came to those shores one day.
For the Jumblies came in a sieve, they did,---
Landing at eve near the Zemmery Fidd
Where the Oblong Oysters grow,
And the rocks are smooth and gray.
And all the woods and the valleys rang
With the Chorus they daily and nightly sang,---
"Far and few, far and few,
Are the lands where the Jumblies live;
Their heads are green, and their hands are blue,
And they went to sea in a sieve."

Happily, happily passed those days!
While the cheerful Jumblies staid;
They danced in circlets all night long,
To the plaintive pipe of the lively Dong,
In moonlight, shine, or shade.
For day and night he was always there
By the side of the Jumbly Girl so fair,
With her sky-blue hands and her sea-green hair;
Till the morning came of that hateful day
When the Jumblies sailed in their sieve away,
And the Dong was left on the cruel shore
Gazing, gazing for evermore,---
Ever keeping his weary eyes on
That pea-green sail on the far horizon,---
Singing the Jumbly Chorus still
As he sate all day on the grassy hill,---
"Far and few, far and few,
Are the lands where the Jumblies live;
Their heads are green, and their hands are blue,
And they went to sea in a sieve."

But when the sun was low in the West,
The Dong arose and said,---
---"What little sense I once possessed
Has quite gone out of my head!"
And since that day he wanders still
By lake and forest, marsh and hill,
Singing, "O somewhere, in valley or plain,
Might I find my Jumbly Girl again!
For ever I'll seek by lake and shore
Till I find my Jumbly Girl once more!"

Playing a pipe with silvery squeaks,
Since then his Jumbly Girl he seeks;
And because by night he could not see,
He gathered the bark of the Twangum Tree
On the flowery plain that grows.
And he wove him a wondrous Nose,---
A Nose as strange as a Nose could be!
Of vast proportions and painted red,
And tied with cords to the back of his head.
---In a hollow rounded space it ended
With a luminous Lamp within suspended,
All fenced about
With a bandage stout
To prevent the wind from blowing it out;
And with holes all round to send the light
In gleaming rays on the dismal night

And now each night, and all night long,
Over those plains still roams the Dong;
And above the wail of the Chimp and Snipe
You may hear the squeak of his plaintive pipe,
While ever he seeks, but seeks in vain,
To meet with his Jumbly Girl again;
Lonely and wild, all night he goes,---
The Dong with a luminous Nose!
And all who watch at the midnight hour,
From Hall or Terrace or lofty Tower,
Cry, as they trace the Meteor bright,
Moving along through the dreary night,---
"This is the hour when forth he goes,
The Dong with a luminous Nose!
Yonder, over the plain he goes,---
He goes!
He goes,---
The Dong with a luminous Nose!"
#+END_QUOTE


 #EdwardLear #BertrandRussel #Chesterton #Children #Literature #nonsense 
