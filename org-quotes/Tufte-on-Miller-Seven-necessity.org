#+TITLE: Seven, George Miller in Edward Tufte's Beatiful Evidence
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>

** George Miller quoted by Edward Tufte in /Beautiful Evidence/
#+BEGIN_QUOTE
"George Miller's classic 1956 paper 'The Magical Number Seven, Plus or Minus Two'"
"These studies on memorizing nonsense then led some interface designers, as well as PP guideline writers seeking to make a virtue of a necessity, to concluded that only 7 items belong on a list or a slide, a conclusion that can only be reached by not reading [George] Miller's paper....[ 7(plus+ or minus- 2) is relevant for] presentations consisting of nonsense syllables that the audience must memorize and repeat back to a psychologist.. On the contrary the deep point of Miller's work is to suggest strategies, such as placing evidence within a context, that extend the reach of memory beyond tiny clumps of data...."
 --- (Edward Tufte /Beautiful Evidence/ 2006: 160)
#+END_QUOTE
