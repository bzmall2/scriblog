

> We present Fresh Current’s wide-ranging interview with Aileen Mioko Smith of Kyoto-based Green Action, whose lifetime of activism spans the movement from the early 1970s to achieve justice for the victims of Minamata Disease (mercury poisoning by corporate giant Chisso) to the contemporary movement to end nuclear power, care compassionately for the victims, and transform Japan’s energy profile.

> Smith reflects on the extraordinary size and persistence of recent mass protests against nuclear power, the reasons for their vitality, and the role of social media in facilitating spontaneous protest. But she also offers a clear-headed look at what will be required for the movement to achieve its goals, beginning with public education about nuclear power and energy alternatives, and educating the politicians who will make the decisions. But she also notes the powerful financial and institutional obstacles to political change at the center. Her careful comparison of similarities between government obfuscation and resistance to recognizing and acting on the disasters at Minamata and Fukushima is a powerful warning of the difficulties that anti-nuclear forces face.

   - https://apjjf.org/2012/10/33/Aileen-Mioko-Smith/3808/article.html
     
** Recommended Citation: Aileen Mioko Smith, "Post-Fukushima Realities and Japan’s Energy Future," The Asia-Pacific Journal, Vol 10 Issue 33, No. 2, August 13, 2012.
   

     2021年  9月 23日 木曜日 20:30:39 JST
