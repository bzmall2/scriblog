#+TITLE: James-Harvey-Robinson

** The Mind in the Making

 1. We have available knowledge and ingenuity and material resources to make a far fairer world than that in which we find ourselves, but various obstacles prevent our intelligently availing ourselves of them.
 2. When we contemplate the shocking derangement of human affairs which now prevails in most civilized countries, including our own, even the best minds are puzzled and uncertain in their attempts to grasp the situation.
 3. The world seems to demand a moral and economic regeneration which it is dangerous to postpone, but as yet impossible to imagine, let alone direct.
 4. to create an unprecedented attitude of mind to cope with unprecedented conditions, and to utilize unprecedented knowledge This is the preliminary, and most difficult, step to be taken
 5. in order to take it[the first step] we must overcome inveterate natural tendencies and artificial habits of long standing.
    - Wink on Third Way, Jesus Non-violence
 6. how are we to rid ourselves of our fond prejudices and open our minds?
 7. "[History is]... a study of how man has come to be as he is and to believe as he does.
 8. Aristotle's treatises on astronomy and physics, and his notions of "generation and decay" and of chemical processes, have long gone by the board, but his politics and ethics are still revered.
    1. Noam Chomsky on Slaver etc.. AI is mistaken interview
 9. relatively little of the same kind of thought has been brought to bear on human affairs.
    1. Neal Stephenson on Power Disorders
 10. The Senator will nevertheless unblushingly appeal to policies of a century back, suitable, mayhap, in their day, but now become a warning rather than a guide. The garage man, on the contrary, takes his mechanism as he finds it, and does not allow any mystic respect for the earlier forms of the gas engine to interfere with the needed adjustments.
 11. 

