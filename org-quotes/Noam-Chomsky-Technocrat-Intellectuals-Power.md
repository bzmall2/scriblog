#### p. 13

> Quite generally, what grounds are there for supposing that those whose claim to power is based on knowledge and technique will be more benign in their exercise of power than those whose claim is based on wealth or aristocratic origin? On the contrary, one might expect the new mandarin to be dangerously arrogant, aggressive, and incapable of adjusting to failure, as compared with his predecessor, whose claim to power was not diminished by honesty as to the limits of his knowledge, lack of work to do, or demonstrable mistakes.{8} In the Vietnam catastrophe, all of these factors are detectable. There is no point in overgeneralizing, but neither history nor psychology nor sociology gives us any particular reason to look forward with hope to the rule of the new mandarins. &#x2014; Noam Chomsky in  "Objectivity and Liberal Scholarship" *American Power and the New Mandarins* p. 13

> ごく一般的につちえ、知識と技術に権力の基盤を置くことを主張する人々が、富と貴族の血統を置く人々よりも、権力行使に際しては恵み深いと過程できるとんな理由があるというのだろうか。それどころか逆に、この新しい高級官僚はおの前任者と比較して、危険は程高慢で、攻撃的で、自らの欠陥を正す能力を失いている者であることが分かるだろう。そして、こうした人々の権力要求は、知識の限界、執行業務の不足、あるいはあらわな失策を正直に認めることによっても減少するものではなかった。ベトナムの悲劇では、こうした要因のすべてが見出される。一般論に走りすぎては何にもならないが、歴史学、心理学、社会学は新しい高級官僚支配に期待を寄せるだけの特別の理由をなにも与えてくれあない。 &#x2014; ノアム チョムスキー "学問と各観性について" *アメリカン パワーと新官僚&#x2014;知識人の責任---* 訳：木村雅次、 吉田武士、 水落一朗 p. 32 

> The greatest evil of any form of power is just that it always tries to force the rich diversity of social life into definite forms and adjust it to particular norms. &#x2014;Rudolf Rocker quoted in Noam Chomsky's *American Power and the New Mandarins* (1969)

> いかなる権力形体の中でも、極悪なるものは、絶えず、社会生活の豊な多様性を、特定の形体に無理矢理とじ込め、また、特殊な基準へとそれを順応させてしまおうとするものである。 &#x2014;ルドルフ ロッカー からの引用 *アメリカン パワーと新官僚&#x2014;知識人の責任---* の "まえがき" p.23

> Political rights&#x2026; exist&#x2026; only when they have become the ingrown habit of a people, and when any attempt to impair them will meet the violent resistance of the populance. &#x2026; One compels respect from others when one knows how to defend one's dignity as a human being. This is not only true in private life; it has always been the same in political life as well. &#x2014; Rudolf Rocker quoted in "Introduction" to *Amerian Power and the New Mandarins* p. 19

> 政治的な権利は&#x2026; それが民衆の内部に生じた習慣となり、その権利が侵されそうな時に民衆の激しい抵抗に出会うようになって、確実なものとなる。 &#x2026; 人間としての威厳を守り抜くことを知るものは、他人から尊敬をとりつけるものだ。この法則は個人的せいかつ にのみあてはまるものではなく、政治生活においても、常に、同じことが言えるのだる。&#x2014;ルドルフ ロッカー からの引用 *アメリカン パワーと新官僚&#x2014;知識人の責任---* の "まえがき" p.23

> a society maimed through the systematic corruption of its intelligence. &#x2014; Conor Cruise O'Brien quoted in Noam Chomsky's *American Power and the New Mandarins* (1969)

> 知性の組織的な堕落により不具になった社会. &#x2014; コナー クルーズ オブライエン からの引用： *アメリカン パワーと新官僚&#x2014;知識人の責任---* p. 29

> the university "is no only failing to meet its responsibilities to its students; it is betraying a public trust" &#x2014; Senator Fulbright quoted in Noam Chomsky's *American Power and the New Mandarins* p. 24 (1969)

> 大学は「学生にたいする責任を果たしていないばかりではなく、一般の人々の信頼を裏切っている。」 &#x2014; フルブライト上院議員 からの引用： *アメリカン パワーと新官僚&#x2014;知識人の責任---* p. 30 (1969)



> A system that turns potentially independent thinkers into politically subordinate clones is as bad for society as it is for the stunted individuals.
> 
> &#x2026;
> 
> If you are a student, understanding the political nature of professional work can help you hold on to your values and moral integrity as you navigate the minefields of professional training and, later, employment. 
> 
> &#x2026;.
> 
> Groups that simply trust professionals without truly understanding them are very likely to be misdirected or sold out by those professionals. 
>     &#x2014; Jeff Schmidt in  *Disciplined Minds* p.5-6  (2000)

  #NoamChomsky #Chomsky #JeffSchmidt #DisciplinedMinds #intellectuals #professionals #education #ConorCruiseOBrien #RudolfRocker #日本語 #translation #官僚 #知識人 #intellectuals 
