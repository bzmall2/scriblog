#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title{2021/10/07 Elements for Practical Stylist}

While using the @italic{English Through Pictures}@~cite[EP1-KKS] books as a base for teaching GDM@~cite[GDM-about] I introduce textbooks I used in high school and college. We used @italic{The Elements of Style}@~cite[SnW-EoS] and @italic{The Practical Stylist}@~cite[SB-PS]. A newer @italic{Practical Stylist}@~cite[SB-LPS] still has two (only two!) paragraphs from George Orwell. The @italic{Sixth Edition} from the 1980s has all of @italic{Politics and the English Langauge}. The classic essay was cut down to one paragraph in @italic{The Longman} version. The new international version is also missing the lines that have remained in my head for decades.

I'll have to see if @italic{The Practical Stylist}'s earlier incarnations as @italic{The Complete Stylist} and @italic{The Essayist} have the complete essay, but this last paragraph of an excerpt by James Baldwin has always seemed important and deserving of more attention:

@nested[#:style 'inset]{
@italic{Now I am percectly aware that there are other slums in which white men are fighting for their lives, and mainly losing.} I know that blood is also flowing through those streets and that the human damage there is incalculable. People are continually pointing out to me the wretchedness of white people in order to console me for the wrtechedness of blacks. But an itemized account of the American failure does not console me and it should not console anyone else. That hundreds of thousands of white people are living, in effect, no better than the "niggers" is not a fact to be regarded with complacency. @italic{The social and moral bankruptcy suggested by this fact is of the bitterest, most terrifying kind.}}

For years I thought other lines I saw in @italic{The Practical Stylist} were from James Baldwin and not Stephen Jay Gould. I looked through a few books searching James Baldwin's essays for the lines about reasonable assumption that people with minds equal to that of Albert Einstein have probably died in cotton fields and other places of slavery. But the lines, the essay comes to mind if you watch Tarantino's @italic{Django}, are from Stephen Jay Gould. In @italic{Wide Hats and Narrow Minds} he describes eminent scientists in the late 1800s were unperturbed by their own "outrageous example of data selected to conform with a priori prejudice"@~cite[(in-bib SJG-WHNM ", 239")] and proved their superior intelligence by bragging about their hat sizes. The important lesson to get from the  1880s "science" making and embarrassing spectacle of itself, is that there are probably areas in which "scientists" of today are also self-flattering morons. @a-fn{See a previous post's glossary for moron: @secref["20210924-010 index"]}

@nested[#:style 'inset]{
The physical structure of the brain must record intelligence in some way, but gross size and external shape are not likely to capture anything of value. I am, somehow, less interested in the weight and convolutions of Einstein's brain than in the near certainty that people of equal talent have lived and died in cotton fields and sweatshops. @~cite[(in-bib SJG-WHNM ", 239")]}

I enjoyed reading some books about writing (and writers' style) in Japanese too. Maybe it should not be surprising that there seems to be a lot of common ground shared by writers who advise about writing. Minako Saito makes fun of all the advice: while serving up examples that implement that advice. But there are valuable observations to be found in the books, attentive people make their living by writing. Even when they take on a project out of venal necessity (which may have been the case for Jun'ichiro Tanizaki, the father of Japan's @italic{BunShoDokuHon}文章読本 @italic{Readers} that serve advice on writing, another writer just lent his name to a ghosted book..) they probably put down at least a few accurate observations. In 1935, well before school teachers and linguists, Tanizaki wrote that Japanese sentence do not need Subjects. Writers would try to follow the advice of imported grammar rules, but fall back into abbreviating the subject as they wrote in their appropriate style. Hiashi Inoue wrote that, if a writer has something important and original to say, their style will naturally come to dance... The substance, the ideas, should determine the writing, I guess.

@nested[#:style 'inset]{
@bold{Place yourself in the background}

Write in a way that draws the reader's attention to the sense and substance of the writing, rather than to the mood and temper of the author. If the writing is solid and good, the mood and temper of the writer will eventually be revealed and not at the expense of the work. Therefore, the first piece of advice is this: to achieve style, begin by affecting non---that is, to place yourself in the background. A careful and honest writer does not need to worry about style. As you become proficient in the use of language, your style will emerge, because you yourself will emerge, and when this happens you will find it increasingly easy to break through the barriers tha separate you from other minds, other hearts---which is, of course, the purpose of writing, as well as its principal reward. Fortunately, the act of composition, or creation, disciplines the mind; writing is one way to go about thinking, and the practice and habit of writing not only drain the mind but supply it, too.@~cite[(in-bib SnW-EoS ", 70")]}

I think George Orwell in @italic{Why I Write} made observations of his own writings that are consistent with Strunk and White's advice. @italic{Plain Style} advice may be hard to follow after generations of advertising and now that everyone is supposed to think of themselves as little CEOs in need of marketing hype. 

@section{P.S. James Baldwin's Fifth Avenue}

Yes! In @italic{The Essayist}@~cite[SB-TE] I can read all of @italic{Fifth Avenue Uptown: A Letter from Harlem}@~cite[JB-5thAv].

@nested[#:style 'inset]{
... the inequalities suffered by the many are in no way justified by the rise of the few. A few have always risen---in every country, every era, and in the teeth of regimes which can by no stretch of the imagination be thought of as free. Not all of these people, it is worth remembering, left the world better than they found it. The determined will is rare, but it is not invariably benevolent. Furhtermore, the American equation of success with the big time reveals an awful disrespect for human life and human achievement. This equation has placed our cities among the most dangerous in the world and has placed our youth among the most empty and most bewildered. The situation of our youth is not mysterious. Children have never been very good at listening to their elders, but they have never failed to imitate them. They must, they have no other models. That is exactly what our children our doing. They are imitating our immorality, our disrespect for the pain of others.@~cite[(in-bib JB-5thAv ", 236")]

...

The country will not change until it reexamines itself and discovers what it really means by freedom. In the meantime, generation keep being born, bitterness is increased by incompetence, pride, and folly, and the world shrinks around us.

It is a terrible, an inexorable, law that one cannot deny the humanity of another without diminishing one's own: in the face of one's victim, one sees oneself...}

In Albert Camus's @italic{The Rebel}, I see the same pattern of thought: @bold{Great Minds Think Alike!}

@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211007-030.scrbl}}}

@generate-bibliography[#:tag "20211007-030-bibliography"]
@generate-footnotes[]
@index-section[#:tag "20211007-030-glossary"]
