#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title{2021/09/29 Humanities Racket Citations}

A collection of speech transcripts and writings, @italic{Speculative Instruments}@~cite[IaR-SI] helps me think about education, studying: learning and teaching. The author instigated the teaching method that made EFL work worthwhile for me. His writings can help cohere the basic language learning, the humanities, and Noam Chomsky's work.@~cite[NC-CL]. Decent views of mind, language, poetry and the Humanities (or liberal arts) cohere well with @italic{Racket} @italic{Scribble} programming as well.

It's easy to see @italic{Racket} as among classics, to connect the language to lambda calculus and from there to Alfred North Whitehead and Bertrand Russel's @italic{Principia Mathematica}. Before going back to 1913 we can think of Paul Graham's lines about @italic{Racket}'s ancestor, Lisp:
@nested[#:style 'inset]{
... [the reason] this 1950s language is not obsolete is that it was not technology but math, and math doesn't get stale. The right thing to compare Lisp to is not 1950s hardware, but, say, the Quicksort algorithm, which was discovered in 1960 and is still the fastest general-purpose sort.@~cite[PG-icad]}

But once Whitehead and Russel come into the picture, the view is easliy expanded to encompass history, education, philosophy, and religion: just about everything I suppose. With @italic{Racket} serving as an threshold entrance, everything needed for a good humanities course is potentially within a coherent view, and useful in daily practice.

@italic{Racket} researcher and developer Robert Bruce Findler also makes it easy to link programming to the humanities other than math. His homepage features fascinating quotes:

@nested[#:style 'inset]{
“The programmer, like the poet, works only slightly removed from pure thought-stuff.” --- Fredrick Brooks @~cite[RF-HP]
}
and
@nested[#:style 'inset]{
@verbatim{
The poet's eye, in a fine frenzy rolling,
Doth glance from heaven to earth, from earth to heaven;
And, as imagination bodies forth
The forms of things unknown, the poet's pen
Turns them to shapes, and gives to airy nothing
A local habitation and a name.
  --- William Shakespeare @~cite[RF-HP]}
}  

I want look into more definitions for poetry and language from Richards and Chomsky. But first a quick glance back to where this fascination with @italic{Racket} and its potential for the Humanities came from. For me, the idea that programming might serve as an approach to  "higher education"@a-fn{For me the phrase comes from a @italic{Shriekback} album: @italic{Big Night Music}'s song, @italic{Sticky Jazz}: "Higher education / Never seen the world like this before."} or the humanities course.

The lecture starts off giving the pleasant impression that the teacher is humble, yet confident: with no need to armor himself by claiming to be a scientist,

@nested[#:style 'inset]{
... Computer science is a terrible name for this business. First of all, it's not a science. It might be engineering or it might be art... @~cite[MIT-SICP-lect-note-1]}

or by worshipping his machines,

@nested[#:style 'inset]{
... It's also not really very much about computers. And it's not about computers in the same sense that physics is not really about particle accelerators, and biology is not really about microscopes and petri dishes.@~cite[MIT-SICP-lect-note-1]}

What made me think of the humanities is the way he goes all the way back to ancient Egypt's surveying and the roots of geometry as an analogy for what is being done in today's "computer science" and the study of complex processes.

@nested[#:style 'inset]{
In fact, there's a lot of commonality between computer science and geometry. Geometry, first of all, is another subject with a lousy name. The name comes from Gaia, meaning the Earth, and metron, meaning to measure. Geometry originally meant measuring the Earth or surveying.

And the reason for that was that, thousands of years ago, the Egyptian priesthood developed the rudiments of geometry in order to figure out how to restore the boundaries of fields that were destroyed in the annual flooding of the Nile. And to the Egyptians who did that, geometry really was the use of surveying instruments.

Now, the reason that we think computer science is about computers is pretty much the same reason that the Egyptians thought geometry was about surveying instruments. And that is, when some field is just getting started and you don't really understand it very well, it's very easy to confuse the essence of what you're doing with the tools that you use. And indeed, on some absolute scale of things, we probably know less about the essence of computer science than the ancient Egyptians really knew about geometry.@~cite[MIT-SICP-lect-note-1]
}

This approach is in accordance with Neil Postman's recommendation in @italic{Technopoly}. Any subject can be taught as a part of Humanities' @italic{Great Conversation} and @italic{Racket}'s languages with their growth up through the languages @italic{Sceme} and @italic{Lisp} make it easy to start participating in the @italic{dialogue}. All the work done with @italic{Scribble} could make the @italic{Digital Humanities}@~cite[MLW-DH] a daily practice as the "core subject of a liberal arts education."@~cite[(in-bib HTDP-1 ", xviii")] This "core subject" might not even need its own time-slot, as Shiram Krishnamurthi suggests in his workshop talk, "Curriculum Design as an Engineering Problem." On-line searchng for Shriram Krishnamurthi's video@~cite[SK-CD-Engineering] had me reading his "@italic{not a blog}" blog,@~cite[SK-PS-about] and then reading about Digital Gardens.@~cite[MA-DGs]
@; https://www.youtube.com/watch?v=rM_E2IwlprY

Eventually I'll have to start writing under a project simpley named @italic{Scribblings} instead of this @italic{Scriblog}. But I might leave the date and number in the original filenames. They recorde when the writing was "First Planted"@~cite[MA-DGs]. The question now is how to automate the insertion of a "Last Tended" date. 

@nested[#:style 'inset]{
As a litmus test, I consider anything that puts dates in filenames and URLs as broken: the date when a posting was begun is essentially irrelevant to its state (after possibly several revisions) several years later.@~cite[SK-PS-about]}

Using @italic{Scribble}'s @italic{--htmls} option to output an @italic{index} page for all @italic{include-section} posts offers a compromise. The parent-directory files keep the "First Planted" "metadata" accessible. But the filenames and URLs exported to the @italic{html} site directory are renamed with the @italic{title}'s @italic{tag}s. That takes care of the file-names and urls being freed of the date metadata. But without a way have the @italic{title}-@italic{tag}-based name seen right next to the date-based name it will be hard to rearrange @italic{include-section} lines. Maybe taking the time to write a kind of @italic{abstract} for the site, one where the writer updates the post most worthy of attention, will work. For learning, debugging and curiousity the serialized listing of links might still be helpful. 

As prophesied in the 1986 lecture, here we are using the power of Racket to talk about "conventional methods of doing things."

@nested[#:style 'inset]{
... you start using the power of Lisp to talk not only about these individual little computations, but about general conventional methods of doing things.@~cite[MIT-SICP-lect-note-1]}

The interesting thing about @italic{Computer Science} education the way @italic{Racket}eers pursue it is that it is, like the @italic{Humanities}, "for everybody". @italic{Racket} is  "useful for everyone." @italic{Racket} could help work over conflicts between @italic{General} and @italic{Special} @italic{Education}, between the @italic{Humanities} (or @italic{liberal arts}) and various sciences. 

@nested[#:style 'inset]{
This scientific audience voted overwhelmingly that the central role in education should be assumed by the humanities.  These people obviously did not hold science cheap; they were giving their lives to it.  What did their vote mean?
... that science, pursued as a scientist pursues it, is a subject for the specialists, while the humanities are for everybody.  By the humanities I mean such subjects as literature, languages, history, philosophy, and art.  I do not think, nor did the London scientists, that these by themselves are the whole of a liberal education.  Science obviously has a part in it.  But at all levels in such education, its part is secondary, not primary. @~cite[BB-Edu-Age-of-Science]}

The above quote parallels:

@nested[#:style 'inset]{
@italic{traditional forms of programming} are useful for just a few people. But, programming @italic{as we the authors understand it} is useful for everyone: the administrative secretary who uses spreadsheets as well as the high-tech programmer.
@~cite[(in-bib HTDP-1 ", xx")]}

Of course the mention of a secretary brings to another plea for programming as an activity for everyone.

@nested[#:style 'inset]{
When large numbers of nontechnical workers are using a programmable editor, they will he tempted constantly to begin programming in the course of their day-to-day lives. This should contribute greatly to computer literacy, especially because many of the people thus exposed will be secretaries taught by society that they are incapable of doing mathematics, and unable to imagine for a moment that they can learn to program. But that won't stop them from learning it if they don't know that it is programming that they are learning! According to Bernard Greenberg, this is already happening with Multics EMACS.@~cite[RMS-EP]}

This strengthens my feeling that @italic{e-portfolios} would serve education well if done in @italic{Scribble}. Teachers doing their @italic{teaching portfolios} in scribble and researchers doing their @italic{Reproducible Research} in @italic{Scribble} as well would contribute to @bold{coherence} in the curriculum, and among educational communities.

There may be a common attitude among the capable, confident creators quoted in this post. Later I hope to return to the attitudes described in @italic{Cartesian Linguistics} but for now, from the field of mathematics the most obviously shows the overlap shared between @italic{Racket} and the @italic{liberal arts}, or @italic{Humanities}, let's quote another classic:

@nested[#:style 'inset]{
When clever people pride themselves on their own isolation, we may well wonderwhether they are very clever after all. Our studies in mathematicsare going to show us that whenever the culture of a people loses
contact with the common life of mankind and becomes exclusively the plaything of a leisure class, it is becoming a priestcraft. It is destined to end, as does all priestcraft, in superstition. To be proud of intellectual isolation from the common life of mankind and to be disdainful of the great social task of education is as stupid as it is wicked. It is the end of progress in knowledge. No society, least of all so intricate and mechanized a society as ours, is safe in the hands of a few clever people. The mathe­matician and the plain man each need the other. @~cite[(in-bib LH-MfM ", 14-15")]}

Lancelot Hogben stresses the importance of learning grammar to procect human rights and liberties, and Richard Stallman is a major figure in the Free Software movement. Like the @italic{Emacs} editing environments, the @italic{Racket} langauges are @italic{Free Software}. The should be worked to promote human rights, creative work, and self-realization.

Noam Chomsky writes about how conceptions of language should be considered along with "the concept of human nature that underlies them."@~cite[(in-bib NC-CL ", 73")] It should be interesting to think on the common ground shared among educators and programmers. Perhaps Humboldt's ideas of "the fundamental human right to develop a personal individuality through meaningful creative work and unconstrained thought." can link free software and programming education to Chomsky's "rationalist-romantic" attitudes. I still haven't gotten to the similarities among Richards's "Experimentalists" and Chomsky's "empiricists." I see so much common ground between them and others writing about the humanities it's hard for me to notice the conflicts among them.

There's one more passage I need to make available here. I think of it a lot, wondering if I should avoid over-use of the word "behaviour" when "activity" would be more appropriate. Specialized uses of words creep into general conversation and confuse things, maybe care is needed with "response" and "reaction" too.

I can't find where Richards talks about how when you fall down the steps, that is "behaviour". Going up the steps, that is "activity." So things governed by laws like circling planets exhibit behavior: responding to toher things with mass. People governed by innate principles and reacting to experience are doing activity...

Extending cares with "behaviour" and "activity" to "response" and "reaction" was instigated by a paragraph in @italic{Gestalt Therapy} featuring Arnold Toynbee. The paragraphs suggest why people are not accessible to "experimentalists", important reactions are not repeatable, reproducbile:

@nested[#:style 'inset]{
One can capture the particular flavor of Gestalt therapy by borrowing a formulation fo Arnold Toynbee's formulation. Toynbee claimed that history cannot be based on the model of natural science because human actions are not a cause but a challenge, and their consequences are not an effect but a response. The response to a challenge is not invariable, so history is inherently unpredictable. @~cite[(in-bib GTherapy-Intro ", xxii")]}


@section{Why are the humanities important?}

@nested[#:style 'inset]{
Manipulation and exploitatin---for the benefit of the operator, or of the subject---that is the chief danger man incurs thorugh the decline of the humanities. The humanities are his defence against emotional bamboozlement and misdirection of the will. The student of science---without the support of that which has been traditionally carried by literature, the arts and philosophy---is unprotected...@~cite[(in-bib IaR-SI-Ftr-Hmnts ", 67")] }


@section["complexity baffles"]
Checking Robby Findler's home page for his poetry quotes had me verifying that his Fred Brooks was the author of @italic{The Mythical Man Month} that Paul Graham mentions. The Wikipedia page for Fred Brooks, introduced me to a variety of similar views and interesting ideas. I'd like to keep track of at least one line after all that hyperlink jumping.

@nested[#:style 'inset]{
.. people are increasingly misinterpreting complexity as sophistication.  @~cite[Wirth-Law-WkP]}

Wikipedias line about Niklaus Wirth's argument in "A Plea for Lean Software" makes me think of quote I used to see attributed to a character in @italic{Sherlock Holmes}, Professor Moriarty:
@nested[#:style 'inset]{
If you can't dazzle them with dexterity, baffle them with bullshit.}

Encouraging people to mistake complexity for sophistication could be a thread that ties together other commercial products along with software. Commercial course-books come to mind. 


@section{Learned! and questioning}
Forgetting to use @italic{generate-bibliography} after citing produces a warning:
@nested[#:style 'inset]{
Warning: some cross references may be broken due to undefined tags:}
and @literal|{ "(??? ???)"}| in the body of the single-page post. If the citation has been used in other post's the source's author and year will appear. I thought there was a typo in the @italic{make-bib} bibliography entry. Maybe I should avoid this sort of aggravation by uncommenting everything in the comment, and commenting out unnecessary lines later. Maybe it doesn't matter if the bibliography stuff is @italic{require}d and set up...

Every bibliography entry needs a value for the @italic{date} keyword. I can't check Paul Graham's website right now to see if his essays have dates so I just used the year from the date I included while copying his line. 
@nested[#:style 'inset]{
Warning: some cross references may be broken due to undefined tags: }










@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20210929-010.scrbl}}}

@generate-bibliography[#:tag "20210929-010-bibliography"]
@generate-footnotes[]
@index-section[#:tag "20210929-010-glossary"]
