#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title[#:tag (list "cite gonzui: jrnl or trpt")]{2021/09/22 citing newsletters: journal or techrpt?}

Looking through the @italic{Scribble} bibliography documentation and examples I did not see an obvious to cite a newsletter, or a web page either. So I'm going to experiment with @italic{journal-location} and @italic{techrpt-location} formats. For years I've felt that I should write (or at least introduce) more about what people have learned in Minamata. My place is a two-hour drive from Minamata, I've visited several times, talked with a lot of good people that live there and read a lot of important writings about the place. With a set-up that makes it a smooth process to write responsibly, I might be able to do more from my position here in the middle of Kyushu.

In yesterday's post @a-fn{Yesterday's post: @secref["citation function start"]} I wrote about making a procedure to work a convetion for block-quotes with citations. That procedure is no closer to working, but I want to keep getting used to the details I want to use, like @italic{filebox}:

@filebox["quotes/Aileen-w-Michi-Oishikatta.txt"]{
おいしかったって言うのと、 海は宝、感謝、 恵み。 私たちの写真集の最初に、 患者さんの杉本栄子さんの想いを語る言葉を載せました。 海のことを悪く言われると怒る、 悲しい。 海は慰めてくれるところ。 お魚への気持ちが出ていると想います。

bib-Nagano202108
, 6}

Above, the @italic{make-bib} definition for @italic{bib-Nagano202108} uses the @italic{tchrpt-location} fucntion. Below,  @italic{bib-Koizumi-202108} uses the @italic{journal-location} function.

@filebox["quotes/Hatsue-Patients-talk-after-movie.txt"]{
六月に上映会の実行委員向けの試写があり、 そのあと患者と関係者向けの小さな試写があった。 そのとき、 上映後の患者の口から出てきたのは映画の感想というより自分の思い出話が多かった。 「あのときは日吉さんがいた」 「工場前で座り込みをしたときには、 チッソの第一組合来た」 「いろんあ人が支えてくれた」 「辛い時代を思い出した」。 そこで話す人たちには和やかに昔を懐しむ雰囲気があって、 映画をきっかけに思い出すことがあってよかったのかもしれないと思った。

bib-Koizumi202108
, 13
}

I don't like the @italic{bib-} naming convention, so I'll have to redo the @italic{sources.rkt} entries. But that is just as well since changing the definitions (@italic{bib-Koizumi202108} and @italic{bib-Nagano202108}) would ruin the comparisons for learning from this post.

@nested[#:style 'inset]{
おいしかったって言うのと、 海は宝、感謝、 恵み。 私たちの写真集の最初に、 患者さんの杉本栄子さんの想いを語る言葉を載せました。 海のことを悪く言われると怒る、 悲しい。 海は慰めてくれるところ。 お魚への気持ちが出ていると想います。@~cite[(in-bib bib-Nagano202108 ", 6")]
}

The @italic{citation} with the @italic{nested} block-quote looks the same. Above is the @italic{tchrpt-location style}. Below is the @italic{journal-location} style. I'll have to make a decision according to eitherthe appearance in the @italic{Bibliogrpahy} or the accuracy of the @italic{make-bib} entry.

@nested[#:style 'inset]{
六月に上映会の実行委員向けの試写があり、 そのあと患者と関係者向けの小さな試写があった。 そのとき、 上映後の患者の口から出てきたのは映画の感想というより自分の思い出話が多かった。 「あのときは日吉さんがいた」 「工場前で座り込みをしたときには、 チッソの第一組合来た」 「いろんあ人が支えてくれた」 「辛い時代を思い出した」。 そこで話す人たちには和やかに昔を懐しむ雰囲気があって、 映画をきっかけに思い出すことがあってよかったのかもしれないと思った。@~cite[(in-bib bib-Koizumi202108 ", 13")]
}

I think I'll go with @italic{journal-location}:

@codeblock{
(define bib-Koizumi202108
  (make-bib
   #:title "映画のこと"
   #:author "小泉初恵"
   #:url "http://www.soshisha.org"
   #:location (journal-location "ごんずいGONZUI"
				#:pages '(13 14)
				#:number "162"
				#:volume "一般財団法人 水俣病センター 相思社")
   #:date "2021"))}

It doesn't feel right to add page-numbers as a @literal|{#:note}| as I did for @italic{tchrpt-location}:

@codeblock{
(define bib-Nagano202108
  (make-bib
   #:title "アイリーン美緒子スミスさんインタビュー"
   #:author (authors "永野路美智" "坂本一途")
   #:date "2021"
   #:is-book? #f
   #:location (techrpt-location #:institution "一般財団法人 水俣病センター 相思社"
				#:number "ごんずいGONZUI-162")
   #:url "http://www.soshisha.org"
   #:note "pp. 3-10"))}

Maybe @italic{tchrpt-location} is good for citing online web pages. 

I'll probably have to work with @italic{define-syntax} in order to put the @italic{Scribble} @italic{nested} and @italic{cite}, @italic{in-bib} code in place of a procedure that takes @italic{new-line} separated text. I still have to experiment with multiple paragraphs @italic{new-line} separated text in a @italic{nested} block. (I want to use worthwhile material to practice with @italic{item-list} and @italic{tabular} today too. A vision is to transform convention-following texts into both scribble flows @bold{and} @italic{pict}s for @italic{Racket}'s @italic{slideshow}.)

@bold{Learned}!Writing this post taught me to add simple @literal|{#:tag}|'s to the @italic{title} for posts that will be linked from other posts. Automatically generated tags were hard to use with @literal|{@secref}|. This post also taught me that @literal|{#:date}| entries for @italic{make-bib} must be simple strings for the year: "2021" because something like "2021-08-25" causes errors. The errors occur for every page that has a @italic{bibliography} and @italic{date} complains that it got an @literal|{#f}| value. These important details came to my attention while learning to use @italic{journal-location} for newsletters and @italic{tchrpt-locatin} for web-pages. The next post should probably site a web-page while I work on putting a 10-item list of Japanese lines into a @italic{tabular} table with English translations. There will be opportunities to translat the other way too so intead of @italic{Ja} and @italic{En} I'll use names like @italic{orig} and @italic{elab} for "original" and "elaboration". Ooe Kenzaburo writes about elaboration, and I have Ann Berthoff's @italic{Double Entry Journals} in mind too.

@bold{Trouble!} I was just checking the @italic{index.html} page thinking I'll get into the habit of putting what I learned in the last paragraph of every post. It seemed like clicking the @italic{Bibliograhpy} link would be way to get past all the musings and jump to the last @italic{what I learned} paragraph. That clicking shows me that the @italic{index.html} page's @italic{Bibliography} links all lead to the first post to use a @italic{Bibliography}. I might have to add a @italic{css-style} and @italic{display: none;} those @italic{index-page} @italic{Bibliography} links. 

@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20210922-010.scrbl}}}

@generate-bibliography[#:tag "20210922-010-bibliography"]
@generate-footnotes[]
