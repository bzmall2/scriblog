#lang scribble/manual
@(require scribble/example)
@(define gregor-eval (make-base-eval))
@(gregor-eval '(require gregor
	      (only-in racket/string string-join string-replace)
                    (for-label racket/base)))
@(define-syntax-rule (gregor-example e ...)
  (examples #:label "Code:"  #:eval gregor-eval e ...))

@title[#:tag (list "20210911-020" "Learning Scribble Examples 1" "Thinking of Literate Programming 1")]{2021/09/11 Start Post script}

@section{Scribble Examples}

One aim of this blog is to get good at, or gain competency with, Racket and Scribble. Another aim is to grow a flow for writing that will work for me. The learning and growing will have to happen in a graded fashion. So, right now, while I would also like to re-learn scribble footnotes (org-mode is much more intuitive with foot-notes) I will limit this try to @italic{scribble/examples}. Later I will add links and foot-notes to my former attempt, @italic{Essay Racket}, but getting code examples to work is more than enough for one attempt.

I've been using scripts that I can call from the command-line. So now I'd like one, @italic{start-post} that will write a starter template. The starter template will be named with the day's date and the title-string given on the command. Even something simple like this involves a lot of details and decisions. If I decide to make the script directly usable it will involve even background techniques outside or racket. Later, I'll change the mode to @italic{executable} in @italic{emacs}'s @italic{dired-mode}.

But for now let's work with @italic{gregor}. Looking around in @italic{Racket}'s documentation I've seen @italic{date} and @italic{gregor}. I did not see any easy way to generate the day's date like this: @italic{2021/09/11}, or even @italic{20210911}. It should be easy:

@gregor-example[
(define td (today))
td]

Now we have today's @italic{date}, but I didn't see a built-in @italic{date->string} procedure so it's time to make a function.

With the @italic{->year}, @italic{->month}, and @italic{->day} functions the numbers come out of the @italic{#<date >} @italic{struct} (which is "a replacement of, not a supplement to, the built in Racket date." [1])

@gregor-example[
(define td-ls
  (list (->year td) (->month td) (->day td)))
td-ls
]

With the year, month, and day @italic{numbers} will be converted to @italic{strings} and then joined with @italic{"/"} by @italic{racket/string}'s @italic{string-join}. 

@gregor-example[
(string-join (map number->string td-ls) "/")
]

The above example was a chance to see that @italic{string-join} is not in @italic{racket/list}, and to work with @italic{require}'s @italic{only-in}.

Now that all the pieces work, how will a useful function work?

@gregor-example[
(define (today-date-string sep)
  (define td (today))
  (string-join
   (map number->string
       (list (->year td)(->month td)(->day td)))
       sep))
(today-date-string "/")
(today-date-string "-")
(today-date-string "")
]

For command-line script I'll want to take in a number like @italic{010}, or @italic{030} for the file name, and a string like "Start post script" for the @italic{post}'s @italic{title}.

Here's the @italic{file-name} part:

@gregor-example[
(define (today-fname-ptitle num str)
  (define fname (string-append
                 (today-date-string "")
                 "-" num "-"
                 (string-replace str " " "-")
                 ".scrbl"))
  fname)
(today-fname-ptitle "010" "Post Title")
]

I'm almost certain that all the command-line arguments come in as strings: no numbers... But now I need the title part.

@gregor-example[
(define (today-fname-ptitle num str)
  (define fname (string-append
                 (today-date-string "")
                 "-" num "-"
                 (string-replace str " " "-")
                 ".scrbl"))
  (define ptitle (string-append
                  (today-date-string "/")
                  " " str))
  (values fname ptitle))
(today-fname-ptitle "020" "Next Post's Title")
]

I'm surprised this works, but @italic{examples} let's you re-define a function in another code block. It's not as if you are adding another block in a script or the DrRracket Definitions window.

This is enough @italic{scribble/examples} work for today. If I don't just take the @italic{today-fname-ptitle} procedure and work directly with a file-writing script, I'll run out of time. Chances are that if this script is not done and usable today, it will never get done or be used. And I was hoping to re-learn how to use foot-notes too. You can only do so much in one day and I already have the footnote-example code opened... Maybe the usable script will have to wait...

@section{Literate Programming}

I'll have to try the @italic{sribble/lp2} with another script in the near future. 

A year or so ago, during my first attempts with @italic{Essay Racket} I tried to do @italic{Literate Programming} with @italic{lp2} along with @italic{scribble/example}. There must be a way to work the syntax so that some code chunks are also shown as examples. All the overhead of using @italic{scribble/example} @bold{and} @italic{lp2} at the same time was too distracting. It seemed like a neat idea at the time. The idea might come into use as working with scribble comes to be second nature.... 



 - [1] @hyperlink["https://docs.racket-lang.org/gregor/date.html#%28def._%28%28lib._gregor%2Fmain..rkt%29._date%29%29"]{gregor}

 This page's source is at:
  @url["https://gitlab.com/bzmall2/scriblog/-/blob/main/20210911-020.scrbl"]
