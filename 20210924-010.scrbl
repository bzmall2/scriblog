#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title{2021/09/24 index and new bib}

Today's learning aims: See if unique names for @italic{generate-bibliography} will fix the @italic{index} page links,@a-fn{all @italic{Bibliography} links take the clicker to the first post with a Bibliography.} and try out he @italic{index} functions.

While adding quotes and bibliography entries for this post's practice, I'm re-thinking conventions for text files. The data I've downloaded for classroom administration was a @italic{.csv} file with the first two lines dedicated to metadata. Using the downloaded data required taking the first two lines of course-data (course-name, classroom-number, time-slot...) and then taking the rest of the lines of student-data to produce roll-sheets and seating-charts.@a-fn{For course-data work see: @hyperlink["https://gitlab.com/bzmall2/classroom-roll"]{classroom-roll repo on gitlab} That same sort of convention seemed useful for questionnaire data-visualization: See @hyperlink["https://gitlab.com/bzmall2/class-questionnaire-result-plotting"]{class-questionnaire-result-plotting}}. So for the text files in the @italic{quotes} dir I'll start putting the @italic{bib}liography name and @italic{(maybe)} page number at the start of the file.@~cite[DGraddol-ENext] 

To work with @italic{index} I want to use definitions from @italic{Design for Escape}.@~cite[IaR-DfE] I.A. Richards's approach for decent education should be compared with the approaches explained in @italic{English Next}.

Now I see that changing the name of the  @italic{generate-bibliography} function did not fix the @italic{Bibliography} links on the @italic{index.html} page. Just in case, I'll try changing the @italic{~cite} names too but I'm not expecting much. AND, it didn't work. Some day I'll have to learn to use @italic{IRC} and ask question on the @italic{#racket} @italic{channel}. A @italic{Haskell} coder blogging about @italic{macros} mentioned that asking questions on the @italic{#racket} channel would be quicker but they preferred to waste hours seeking solutions through on-line searches.

Many of the essays in @italic{Towards Liberal Education}@~cite[TLE1st]  are worth reading, and I plan to use definitions from the collections of essays. The essay @italic{Poetry and Advertising} takes care to define @index*[(list "venal") (list "Venal" "available for hire")]{@italic{venal}
as "available for hire"}
and
@index*[(list "selling") (list "Selling" "advantaging the speaker at the expense of the hearer")]{@italic{selling}
as "advantaging the speaker at the expense of the hearer"}.@~cite[SIHaya-Poetry-Adverts]
Another edition of @italic{Torwards Liberal Education}@~cite[TLE4th] defines
@index*[(list "moron") (list "Moron" "a person who cannot think")]@italic{moron}
as "a person who cannot think"
and suggests that, @italic{technical educaion} with its aim @italic{to adjust the young to the group}, has contributed a "good deal" to the "unpopularity of thinking." @index*[(list "thinking") (list "Thinking" "required to get through college" "not necessary to get through life")]{Thinking, Hutchins says "proceeds in the effort to raise and answer questions":} it is a handicap for mechanized operators (a great number of workers) after the Industrial Revolution.@~cite[(in-bib HutchinsCandles ", 192")]. David Noble in @italic{America By Design} documents the decades of effort that went to alter the aims of education and to engineer workers away from thinking.

The little book @italic{Design for Escape: World Education Through Modern Media} has a @italic{preface} with definitions for seven words in its title. @index*[(list "design")(list "Design" "sketch" "to purpose(intend) a person or thing to do or be something")]{@italic{Design} is offered as "not only a plan but the name for the concept" and also as a "sketch."}@~cite[IaR-DfE] To have designs is to have both intentions and plans to achieve those intentions. The little-book's author, I.A. Richards, investigates designs for us, as prisoners of world situation that has trapped us for lack of design, to @italic{escape} from maiming and destruction toward freedom. The designs are on the @italic{world}: "all of it, including the underdog majority." @index*[(list "education")(list "Education" "not the agelong degradations of the traditional fear-haunted classroom")]{@italic{Education} in the title is meant as "something different from, indeed cleanly opposed to" other approaches to education.}@index*[(list "through")(list "Through" "via" "road" "way to")]{@italic{Through} in the is given as "via" or "a way" and "perhaps the only way."} @index*[(list "modern")(list "Modern" "present day" "recently discovered" "the trend of development")]{@italic{Modern} is both threatening and encouraging and refer to a "trend of development" that we should critically examine to see what is coming.}  We should take care that instead allowing "the appeals that sell fastest and the cries most widely heard" to control education, it should be "the concerns that best support our sanity" and survival that influence education. For @italic{Media} "all the theory of channels is relevant" so we should ponder "what can and cannot be transmitted through a given channel" and how channels "must limit, sift, screen, and warp what" they can carry.


A post with an @italic{index} gets a link to it on the @italic{index.html} page. Since I added a @italic{#:tag} to the @italic{index-section} procedure, maybe the @italic{index} links will work for separate posts, unlike @italic{Bibliography} links. Maybe I can add @italic{#:tag}s to @italic{generate-bibliography}? @bold{YES!!} I can! I'm very happy to discover this detail while learning to use @italic{index}. With enough comfort and experience with @italic{Scribble} and @italic{Racket}'s @italic{higher order functions} I imagine it's possible to see that @italic{3:tag}s would solve the problem with @italic{index} page links to the  @italic{Bibliography} on various posts. But the @italic{define-cite} documentation @a-fn{for @italic{define-cite} documentation, see: @url{https://docs.racket-lang.org/scriblib/autobib.html#%28form._%28%28lib._scriblib%2Fautobib..rkt%29._define-cite%29%29}} overwhelmed me until now. After this learning experience with @italic{make-index} and @italic{generate-bibliography} I might get better at reading @italic{contract}s.


@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20210924-010.scrbl}}}

@generate-bibliography[#:tag "20210924-010 sources"]
@;@generate-bibliography[]
@generate-footnotes[]
@index-section[#:tag "20210924-010 index"]
