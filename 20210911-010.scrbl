#lang scribble/manual

@title{2021/09/11 Pages setup}

What a relief to notice why this pages site kept showing the 404 error page. While first learning to use git for back ups to usb sticks I acted on my discomfort with the master/slave metaphor. Someday I'll have to dedicate time and attention toward getting good with git, but I've already forgotten how I changed the branch name to main. The name "neck1" was another option that came to mind.

There are some many details to keep track of to make use of the pages offerings from the git service providers. It's hard for an amateur to get set up, and then have enough energy and attention left over for writing.

Like anything else, I probably just have to dedicate enough quality  time to the activity to get competent. Now I want to go back and re-discover how I set up emacs abbrev-defs and templates and things. But it will take too much time.

For my level of use and familiarity, for the way I use different computers at work and at home: I have to keep it simple. Maybe something will grow out of a simple core, but right now the trick is not to get overwhelmed by details.

So now, to avoid details, I'm just going ot make a post-tmpt.txt file with the #lang line and a title line already including the year and date. I will @italic{C-x-i} the post-template text at the start of each post.

Eventually I should search for a clever way to auto-magically insert the days date, and a link to the  .scrbl source file. That would be good for task-based learning. 

Just getting this far has good questions coming to mind. It's time to explore more of the Scribble documentation for what putting tags with section titles does. While searching for my e-macs abbrevs and ya-snippet set-up I saw all the old org-mode keyboard shortcuts from years ago. Blogging with Scribble should help me discover all sorts of conveninces for writing Racket documentation too.

Getting ready to use Pages with Scribble and emacs involves all sorts of set up: git, gitlab-pages, emacs, conventions....
