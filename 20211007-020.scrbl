#lang scribble/manual
@(require racket/include scriblib/footnote)
@(include "scriblog-styles-include.rkt")
@(define-footnote a-fn generate-footnotes)
@(require scriblib/autobib)
@(include "bibliography/sources.rkt")
@(define-cite ~cite citet generate-bibliography)

@title{2021/10/07 Camus and Ogata}

I read Camus's @italic{The Rebel: An Essay on Man in Revolt} a few times and tend to connect a lot of things to his writings. There should be ways to connect I.A. Richards's work with Camus's thought. Camus also explained why Bertrand Russel wrote @italic{Conquest of Happiness}.@a-fn{I think it is in @italic{Myth of Sisyphus} where Camus writes that after encountering the Absurd one is tempted to write a 'manual of happiness.'} But since I live in South Kyushu and have had opportunites to learn from and visit Minamata, I'd like to think of Masato Ogata@~cite[OM-CW] along with Albert Camus.

And for some context around the paragraph that comes to mind when I see the @italic{Original Vow(Wish)本願} stone statues in Minamata. Camus is visiting Italy:

@nested[#:style 'inset]{
The Giotto in the Santa Croce. The inner smile of Saint Francis, lover of nature and of life. He justifies those who have a taste for happiness... @~cite[(in-bib AC-NBs ", 53")]}

Saint Francis, seems like he is on the side of life: one of the good saints. And there is beautiful sentence by Camus somewhere about living well, maybe  without eternal hope but, with enjoyment of the beauty in human faces.

@nested[#:style 'inset]{
One needs a certain time to realize that the faces in the Italian primitives are those one meets daily in the street. This is because we have lost the habit of seeing what is really important in a face. We no longer look at our contemporaries, and select on what is useful to our aims (in every sense of the word). The Italian primitives do not distort, the "bring to life."

@; page 53 Santissima An 54 nunziata
In the Cloister of the Dead, at the Santissima Annunziata, the sky was gray and full of clouds, the architecture severe, but there is nothing that speaks of death. There are ledger stones and ex-votos; one man was a loving father and a tender husband, another the best of husbands and a skillful merchant; a young lady, model of all virtues, spoke French and English "si come il nativo." (They all created duties for themselves, and today children play leapfrog on the tombs that seek to perpetuate their virtue.)... Almost all of them, according to the inscriptions, had grown resigned, doubtless because they accepted their other duties. I shall not grow resigned. With all my silence, I shall protest to the very end. There is no reason to say: "It had to be." It is my revolt which is right, and I must follow this joy which is like a pilgrim on earth, follow it step by step.

... If I had to write a book on morality, it would have a hundred pages and ninety-nine would be blank. On the last page I should write: "I recognize only one duty, and that is to love." And as far as everything else is concerned, I say @italic{no}. I say @italic{no} with all my strength. The ledger stones tell me taht this is useless, that life is "col sol levante, col sol cadente." But I cannot see what my revolt loses by being useless, and I can feel what it gains.

@;page 54 ...at me with 55 curiousity
I thought about all of this, sitting on the ground with my back against a column, while the children laughed and played. A priest smiled at me. Women lookd at me with curiousity. In the church the organ was playing softly, and the warm color of its harmonies could be heard from time to time through the children's shouts. Death! If I carry on like this, I shal certainly die happy. I shall have eaten up all my hope.@~cite[(in-bib AC-NBs ", 53-55")]}

Masato Ogata's statues of Ebisu and others, his struggles without hope against "modernity" or industrialized civilizations, make me think of a paragraph written in the context of the "Cloister of San Marco, The sun among the flowers":

@nested[#:style 'inset]{
Siennese and Florentine primitives Their insistence on making monuments smaller than men is a result not of an ignorance of the laws of perspective but of a persistence in giving importance to the men and saints whom they depict. Us this in making theatrical décors.@~cite[(in-bib AC-NBs ", 55")]}

Masato Ogata had a conversation with Akira Kurihara. They talked by the sea above the burial site of barrels and barrels of dead fish and their living cousins. The fish were buried with the toxic sludge that built up in the Shiranui Sea over the years the nearby Chisso factory supplied chemicals for military and consumer products while pouring organic mercury into the waters.

@nested[#:style 'inset]{
Landfill was used to cover the area where pollution was most severe. Fish from the area, which were contaminated with high-concentration methyl-mercury, were caught and stuffed in 2,500 oil drums and buried underneath the landfill as ‘polluted fish’. For Ogata, this landfill symbolizes ‘the depth of human sin’. On the field of the reclaimed land, members of the Association have enshrined small stone statues of Buddha and other deities, including ‘Totoro,’ as a special Minamata deity for deceased children and other young lives lost. @~cite[SY-LW]}

Determined that the landfill
@a-fn{For an idea of the setting for the conversation, Shoko Yoneyama explains a bit about the landfill, @url{https://apjjf.org/2012/10/42/Shoko-YONEYAMA/3845/article.html} The article also has great quotes from @italic{Rowing the Eternal Sea: The Story of a Minamata Fisherman}@~cite[OM-RES].}
shall not serve as a cover-up, hiding the damage and avoiding the recognition of all the victims of industrial pollution, Masato Ogata and others have taken to placing small stone statues of Ebisu, Jizo, Mother Mary, and Tottoro. In his conversation with Akira Kurihara, Ogata says somethng that makes me think of Camus's "Florentine primitives" who insisted on making monuments smaller than men in order to persist in giving importance to men and saints.

@@nested[#:style 'inset]{
@bold{栗原} たまたまここへ来た人---私たちもですが---が恵比寿さんのところへ来て手を合わせるときに、 普通は石仏に向って手を合わせますけど、 ここでは恵比寿さまと並んで、 海のほうえ向って手を合わせるんですね。 それはたぶん仏さんが、 人とほかの命とを結ぶ何かになっているからでしょうね。仏さんの形を彫られたのは、 ある意味では救いということを込められたんでしょう。

@bold{緒方} ええ、普通は観音さんんいしても何か記念碑にしても、 どでかいのをつくるでしょう。 福岡あたりの高速道路横には何十メートルもあるような広告塔みたいなものもあるばってんが(笑)。 おれたちは「どんなにでかくとも人間を越えるようなものはつくらんようにしょう」 と決めたんです。 むしろできるだけ小さいほうがいい、 と。 ニューヨークにある自由の女神とか、 それはどれで歴史的な経過もあろうばってんが、 あぎゃんどでかいものをつくるというのは、 あんまりいいと思わんですけどねえ。}

Ogata's feeling for scale is the same as that attributed to the Florentine primitives by Albert Camus. Kurihara's words bring to mind Gary Snyder's Turtle Island and it's Japanese transaltion by Nanao Sakaki.

@elem[#:style pagecodelink-style]{This page was generated by @italic{scribble} code.@a-fn{See this page's @italic{scribble} code: @url{https://gitlab.com/bzmall2/scriblog/-/blob/main/20211007-020.scrbl}}}

@generate-bibliography[#:tag "20211007-020-bibliography"]
@generate-footnotes[]
@index-section[#:tag "20211007-020-glossary"]
