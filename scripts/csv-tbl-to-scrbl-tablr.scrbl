#lang at-exp racket
@(require scribble/manual racket/file csv-reading)
@(provide csvf->tblr)

@(define (csvf->tblr fn)
  (define lst (csv->list (file->string fn)))
  @tabular[#:style 'boxed #:sep @hspace[0]]{
  @lst})
@;(csvf->tblr "../data/PStylists-simple-word-choice.csv")
  
