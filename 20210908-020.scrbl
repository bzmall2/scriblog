#lang scribble/manual

@title{2021/09/08 Second Post}

I was tempted to use FROG (FROzen Blog) a few times but then I read that the creator/maintainer gave up on it. And I just saw that Alexis King converted github Frog posts to Scribble. I can't spare the time and attention span to learn how the "workflow" and CI etc. stuff. Hopefully a few simple conventions, and may a litte extra by-hand additions will let me avoid technical configurations that are beyond me for now.

Thinking of naming conventions just now has me dabbling with racket/date and gregor. And I want to require examples and set up an evaluator again to retain a procedure made with gregor.

